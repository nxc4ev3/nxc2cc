# this one is important
SET(CMAKE_SYSTEM_NAME Windows)
#this one not so much
SET(CMAKE_SYSTEM_VERSION 5.1)

# specify the cross compiler
SET(CMAKE_C_COMPILER    /usr/bin/i686-w64-mingw32-gcc-win32 CACHE FILEPATH "C compiler" FORCE)
SET(CMAKE_CXX_COMPILER  /usr/bin/i686-w64-mingw32-g++-win32 CACHE FILEPATH "C++ compiler" FORCE)

SET(CMAKE_RC_COMPILER   /usr/bin/i686-w64-mingw32-windres CACHE FILEPATH "Resource compiler" FORCE)
SET(CMAKE_LINKER        /usr/bin/i686-w64-mingw32-ld      CACHE FILEPATH "Linker" FORCE)
SET(CMAKE_STRIP         /usr/bin/i686-w64-mingw32-strip   CACHE FILEPATH "strip" FORCE)
SET(CMAKE_AR            /usr/bin/i686-w64-mingw32-ar      CACHE FILEPATH "ar" FORCE)
SET(CMAKE_NM            /usr/bin/i686-w64-mingw32-nm      CACHE FILEPATH "nm" FORCE)
SET(CMAKE_OBJCOPY       /usr/bin/i686-w64-mingw32-objcopy CACHE FILEPATH "objcopy" FORCE)
SET(CMAKE_OBJDUMP       /usr/bin/i686-w64-mingw32-objdump CACHE FILEPATH "objdump" FORCE)
SET(CMAKE_RANLIB        /usr/bin/i686-w64-mingw32-ranlib  CACHE FILEPATH "ranlib" FORCE)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
