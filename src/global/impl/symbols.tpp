/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Implementation of symbol table template functions.
 */



#ifndef CUSTOM_ID_SYMBOLS_TPP_H
#define CUSTOM_ID_SYMBOLS_TPP_H

#include <global/include/symbols.hpp>
#include <global/include/macros.hpp>
#include <global/include/nxcexcept.hpp>

template<typename T, nxc::sym_class whichID>
T &nxc::symbol_table::get(ID id) {
    symbol_type &e = at(id);
    if (e.which() != static_cast<int>(whichID))
        throw bad_symclass_exception(id, static_cast<int>(whichID), "nxc::symbol_table::get");
    return boost::get<T>(e);
}

// search specified type inside specified scope
template<typename Datatype, nxc::sym_class whichID>
bool nxc::symbol_table::search_scope(ID scope,
                                     std::string const &name,
                                     Datatype *&found) {
    // for each symbol
    auto it = this->begin();
    auto end = this->end();
    for (; it != end; ++it) {
        symbol_type &sym = it->second;
        // refuse other symbol types
        if (sym.which() != static_cast<int>(whichID)) {
            continue;
        }
        // obtain info
        Datatype &info = boost::get<Datatype>(sym);
        // compare parent scopes and names
        if (info.getParentID() == scope && info.getName() == name) {
            // assign and return OK
            found = &info;
            return true;
        }
    }
    found = nullptr;
    // return KO
    return false;
}


template<typename T, nxc::sym_class whichID>
bool nxc::symbol_table::find_everywhere(std::string const &name, T *&ref) {
    auto it = this->begin();
    auto end = this->end();
    for (; it != end; ++it) {
        symbol_type &sym = it->second;
        if (sym.which() != static_cast<int>(whichID))
            continue;
        T &data = boost::get<T>(sym);
        if (data.getName() == name) {
            ref = &data;
            return true;
        }
    }
    return false;
}

template<typename Datatype, typename... Args>
Datatype &nxc::symbol_table::allocate(Args... args) {
    ID id = m_id_counter++;                     // update ID counter
    Datatype data(id, args...);                 // construct datatype
    m_storage.emplace_front(std::move(data));   // construct envelope inside container

    symbol_type &sRef = *m_storage.begin();     // get envelope reference
    Datatype &ref = boost::get<Datatype>(sRef); // get datatype reference
    m_map.emplace(id, sRef);                    // create id-envelope mapping
    return ref;                                 // return datatype reference
}

template<typename lookup_scopes_fn, typename... Args>
bool nxc::symbol_table::lookup_scopes(ID start,
                                      lookup_scopes_fn F,
                                      Args&&... info) {
    nxc::ID scopeid = start;
    do {
        nxc::scope_data &scope = get_scope(scopeid);

        if (F(scope, *this, info...))
            return true;

        scopeid = scope.getParentID();
    } while (scopeid != SCOPE_UNKNOWN && scopeid != SCOPE_NONE);
    return false;
}

namespace nxc {
    class parent_visit : public boost::static_visitor<ID> {
    public:
        parent_visit() {}

        template<typename T>
        ID operator()(T &&t) const {
            return t.getParentID();
        }
    };
}

template<typename lookup_symbols_fn, typename... Args>
bool nxc::symbol_table::lookup_symbols(ID start,
                                       sym_mask allowed,
                                       lookup_symbols_fn F,
                                       Args&&... info) {
    nxc::ID scopeid = start;
    do {
        scope_data &scopedata = get_scope(scopeid);

        auto it = this->begin();
        auto end = this->end();
        for (; it != end; ++it) {
            symbol_type &sym = it->second;
            // check 1: symbol class
            if (!matches(sym.which(), allowed)) { // skip wrong symbol types
                continue;
            }
            // check 2: symbol parent
            parent_visit visitor;
            ID parentid = boost::apply_visitor(visitor, sym);
            if (parentid != scopeid) { // skip non-child symbols
                continue;
            }
            // probe
            if (F(sym, scopedata, *this, info...))
                return true;
        }

        scopeid = find_parent_scope(scopeid);
    } while (scopeid != SCOPE_UNKNOWN && scopeid != SCOPE_NONE);
    return false;
}


#endif //CUSTOM_ID_SYMBOLS_TPP_H
