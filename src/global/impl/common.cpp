/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Aggregate implementation file for common infrastructure.
 */


#include <boost/none.hpp>
#include <global/include/macros.hpp>
#include <global/include/nxcexcept.hpp>
#include <global/include/symbols.hpp>
#include <global/include/typesystem.hpp>


class ut_visitor : public boost::static_visitor<nxc::type> {
public:
    ut_visitor(nxc::symbol_table &tbl) : tbl(tbl) {}

    template<typename Val>
    nxc::type operator()(Val &v) const {
        return nxc::untypedefize(v, const_cast<nxc::symbol_table &>(tbl));
    }

private:
    nxc::symbol_table &tbl;
};

nxc::type nxc::untypedefize(type web, symbol_table &tbl) {
    ut_visitor visitor(tbl);
    nxc::type result = boost::apply_visitor(visitor, web.getInner());
    web.setInner(result.getInner());
    return web;
}

nxc::type nxc::untypedefize(nxc::arrays web, nxc::symbol_table &tbl) {
    web.base(untypedefize(web.base().get(), tbl));
    return nxc::type(web);
}

nxc::type nxc::untypedefize(nxc::basic_type web, nxc::symbol_table &tbl) {
    return nxc::type(web);
}

nxc::type nxc::untypedefize(nxc::usertype web, nxc::symbol_table &tbl) {
    switch (whichtype(web, tbl)) {
        case usertype_kind::struct_kind:
        case usertype_kind::enum_kind:
            return nxc::type(web);
        case usertype_kind::typedef_kind: {
            typedef_data &data = tbl.get_typedef(web);
            return untypedefize(data.getType(), tbl);
        }
    }
    throw std::runtime_error("internal wtf error 2");
}

nxc::type nxc::untypedefize(nxc::void_type web, nxc::symbol_table &tbl) {
    return nxc::type(web);
}

nxc::type nxc::untypedefize(nxc::variant_type web, nxc::symbol_table &tbl) {
    return nxc::type(web);
}

bool nxc::usertype::operator==(const nxc::usertype &other) const {
    return id == other.id;
}

bool nxc::usertype::operator!=(const nxc::usertype &other) const {
    return id != other.id;
}

nxc::ID nxc::usertype::getID() const {
    return id;
}

nxc::arrays::base_type &nxc::arrays::base() {
    return subtype;
}

const nxc::arrays::base_type &nxc::arrays::base() const {
    return subtype;
}

void nxc::arrays::base(const nxc::arrays::base_type &base) {
    this->subtype = base;
}

nxc::arrays::definition_type &nxc::arrays::sizes() {
    return size_arr;
}

const nxc::arrays::definition_type &nxc::arrays::sizes() const {
    return size_arr;
}

void nxc::arrays::sizes(const nxc::arrays::definition_type &sizes) {
    this->size_arr = sizes;
}

nxc::arrays::size_type nxc::arrays::operator[](size_t pos) {
    return size_arr[pos];
}

nxc::type_variants nxc::type::which() const {
    return (type_variants) real_type.which();
}

void nxc::type::setInner(basic_type const &t) {
    this->real_type = t;
}

void nxc::type::setInner(arrays const &t) {
    this->real_type = t;
}

void nxc::type::setInner(usertype const &t) {
    this->real_type = t;
}

void nxc::type::setInner(type_variant const &t) {
    this->real_type = t;
}


void nxc::type::setInner(const nxc::variant_type &t) {
    this->real_type = t;
}

void nxc::type::setInner(const nxc::void_type &t) {
    this->real_type = t;
}

bool nxc::type::isReference() const {
    return is_ref;
}

bool nxc::type::isConst() const {
    return is_const;
}

void nxc::type::setReference(bool ref) {
    is_ref = ref;
}

void nxc::type::setConst(bool const_) {
    is_const = const_;
}

bool nxc::type::constFusion(type const &a, type const &b) {
    bool const_ = a.isConst() || b.isConst();
    setConst(const_);
    return const_;
}

const nxc::type::type_variant &nxc::type::getInner() const {
    return this->real_type;
}

nxc::type::type_variant &nxc::type::getInner() {
    return this->real_type;
}

#ifdef TYPE_DEBUG
void charAppend(char **working, const char *msg) {
    size_t msglen = strlen(msg);
    memcpy(*working, msg, msglen);
    *working += msglen;
}

nxc::arrays::operator const char*() {
    if (*str != '\0')
        return str;

    char *working = str;
    charAppend(&working, "array ");
    for (size_type size : size_arr) {
        charAppend(&working, "[");
        if (size == SIZE_UNKNOWN)
            charAppend(&working, "?");
        else
            working += sprintf(working, "%ld", size);
        charAppend(&working, "]");
    }
    charAppend(&working, " of ");
    charAppend(&working, (const char*)subtype.get());
    return str;
}

nxc::usertype::operator const char*() {
    if (*str != '\0')
        return str;
    char *working = str;
    charAppend(&working, "user type (ID ");
    working += sprintf(working, "%lu", id);
    charAppend(&working, ")");
    return str;
}

nxc::type::operator const char*() {
    if (*str != '\0')
        return str;
    
    char *working = str;
    
    if(this->is_ref) {
        charAppend(&working, "reference to ");
    }
    if (this->is_const) {
        charAppend(&working, "const ");
    }

    switch (which()) {
        case type_variants::basic:
            switch (get<basic_type>()) {
                case basic_type::bool_type:
                    charAppend(&working, "bool");
                    break;
                case basic_type::byte_type:
                    charAppend(&working, "byte");
                    break;
                case basic_type::schar_type:
                    charAppend(&working, "signed char");
                    break;
                case basic_type::uchar_type:
                    charAppend(&working, "unsigned char");
                    break;
                case basic_type::sint_type:
                    charAppend(&working, "signed int");
                    break;
                case basic_type::uint_type:
                    charAppend(&working, "unsigned int");
                    break;
                case basic_type::sshort_type:
                    charAppend(&working, "signed short");
                    break;
                case basic_type::ushort_type:
                    charAppend(&working, "unsigned short");
                    break;
                case basic_type::slong_type:
                    charAppend(&working, "signed long");
                    break;
                case basic_type::ulong_type:
                    charAppend(&working, "unsigned long");
                    break;
                case basic_type::float_type:
                    charAppend(&working, "float");
                    break;
                case basic_type::mutex_type:
                    charAppend(&working, "mutex");
                    break;
                case basic_type::string_type:
                    charAppend(&working, "string");
                    break;
            }
            break;
        case type_variants::array:
            charAppend(&working, (const char*) get<arrays>());
            break;
        case type_variants::user:
            charAppend(&working, (const char*) get<usertype>());
            break;
    }
    return str;
}
nxc::arrays::operator std::string() {
    return std::string(this->operator const char*());
}
nxc::usertype::operator std::string() {
    return std::string(this->operator const char*());
}
nxc::type::operator std::string() {
    return std::string(this->operator const char*());
}
#else

const char *copy(const std::string &str) {
    size_t len = str.size() + 1;

    char *copied = new char[len];
    copied[len - 1] = '\0';

    str.copy(copied, len - 1, 0);
    return copied;
}
nxc::arrays::operator const char*() {
    return copy(this->operator std::string());
}
nxc::usertype::operator const char*() {
    return copy(this->operator std::string());
}
nxc::type::operator const char*() {
    return copy(this->operator std::string());
}
nxc::arrays::operator std::string(){
    std::stringstream str;

    str << "array ";
    for (size_type size : size_arr) {
        str << "[";
        if (size == SIZE_UNKNOWN)
            str << "?";
        else
            str << size;
        str << "]";
    }
    str << " of ";
    str << subtype.get().operator std::string();
    return str.str();
}

nxc::usertype::operator std::string() {
    std::stringstream str;
    str << "user type (ID ";
    str << id;
    str << ")";
    return str.str();
}

nxc::type::operator std::string() {
    std::stringstream str;

    if(this->is_ref) {
        str << "reference to ";
    }
    if (this->is_const) {
        str << "const ";
    }

    switch (which()) {
        case type_variants::basic:
            switch (get<basic_type>()) {
                case basic_type::bool_type:
                    str << "bool";
                    break;
                case basic_type::byte_type:
                    str << "byte";
                    break;
                case basic_type::schar_type:
                    str << "signed char";
                    break;
                case basic_type::uchar_type:
                    str << "unsigned char";
                    break;
                case basic_type::sint_type:
                    str << "signed int";
                    break;
                case basic_type::uint_type:
                    str << "unsigned int";
                    break;
                case basic_type::sshort_type:
                    str << "signed short";
                    break;
                case basic_type::ushort_type:
                    str << "unsigned short";
                    break;
                case basic_type::slong_type:
                    str << "signed long";
                    break;
                case basic_type::ulong_type:
                    str << "unsigned long";
                    break;
                case basic_type::float_type:
                    str << "float";
                    break;
                case basic_type::mutex_type:
                    str << "mutex";
                    break;
                case basic_type::string_type:
                    str << "string";
                    break;
            }
            break;
        case type_variants::array:
            str << (std::string) get<arrays>();
            break;
        case type_variants::user:
            str << (std::string) get<usertype>();
            break;
        case type_variants::variant_t:
            str << "variant";
            break;
        case type_variants::void_t:
            str << "void";
            break;
    }
    return str.str();
}


#endif

bool nxc::var_data::isStatic() const {
    return this->is_static;
}

void nxc::var_data::setStatic(bool static_) {
    this->is_static = static_;
}

bool nxc::var_data::isArgument() const {
    return this->is_arg;
}

void nxc::var_data::setArgument(bool arg) {
    this->is_arg = arg;
}

bool nxc::var_data::hasValue() const {
    return (bool) this->const_int_value;
}

int64_t nxc::var_data::getValue() const {
    return this->const_int_value.get();
}

void nxc::var_data::resetValue() {
    this->const_int_value = boost::none;
}

void nxc::var_data::setValue(int64_t val) {
    this->const_int_value = val;
}


nxc::ID nxc::sym_id::getID() const {
    return this->this_id;
}

void nxc::sym_id::setID(nxc::ID id) {
    this->this_id = id;
}

nxc::ID nxc::sym_parent_id::getParentID() const {
    return this->parent_id;
}

void nxc::sym_parent_id::setParentID(nxc::ID id) {
    this->parent_id = id;
}

void nxc::sym_parent_id::setParentID(const nxc::scope_data &scope) {
    this->parent_id = scope.getID();
}


nxc::scope_data::var_list_type &nxc::scope_data::vars() {
    return this->variables;
}

const nxc::scope_data::var_list_type &nxc::scope_data::vars() const {
    return this->variables;
}

void nxc::scope_data::vars(const nxc::scope_data::var_list_type &list) {
    this->variables = list;
}

nxc::ID nxc::task_data::getBodyID() const {
    return this->body_scope;
}

void nxc::task_data::setBodyID(nxc::ID id) {
    this->body_scope = id;
}

std::string const &nxc::sym_name::getName() const {
    return this->name;
}

nxc::ID nxc::func_data::getBodyID() const {
    return this->body_scope;
}

void nxc::func_data::setBodyID(nxc::ID id) {
    this->body_scope = id;
}

nxc::func_data::return_type &nxc::func_data::getReturnType() {
    return this->retval;
}

const nxc::func_data::return_type &nxc::func_data::getReturnType() const {
    return this->retval;
}

void nxc::func_data::setReturnType(const nxc::func_data::return_type &ref) {
    this->retval = ref;
}

nxc::func_data::arg_list_type &nxc::func_data::getArguments() {
    return this->args;
}

const nxc::func_data::arg_list_type &nxc::func_data::getArguments() const {
    return this->args;
}

void nxc::func_data::setArguments(const nxc::func_data::arg_list_type &ref) {
    this->args = ref;
}

nxc::func_data::flag_list_type &nxc::func_data::getFlags() {
    return this->flags;
}

const nxc::func_data::flag_list_type &nxc::func_data::getFlags() const {
    return this->flags;
}

void nxc::func_data::setFlags(const nxc::func_data::flag_list_type &ref) {
    this->flags = ref;
}

bool nxc::func_data::wasIntroduced() const {
    return introduced;
}

void nxc::func_data::setIntroduced(bool introduced) {
    this->introduced = introduced;
}

nxc::enum_data::member_list_type &nxc::enum_data::getMembers() {
    return this->constants;
}

const nxc::enum_data::member_list_type &nxc::enum_data::getMembers() const {
    return this->constants;
}

void nxc::enum_data::setMembers(const nxc::enum_data::member_list_type &type) {
    this->constants = type;
}

bool nxc::enum_data::isComplete() const {
    return this->complete;
}

void nxc::enum_data::setComplete(bool complete) {
    this->complete = complete;
}

bool nxc::enum_data::wasPrinted() const {
    return this->printed;
}

void nxc::enum_data::setPrinted(bool printed) {
    this->printed = printed;
}

nxc::struct_data::member_list_type &nxc::struct_data::members() {
    return this->memb_arr;
}

const nxc::struct_data::member_list_type &nxc::struct_data::members() const {
    return this->memb_arr;
}

void nxc::struct_data::members(const nxc::struct_data::member_list_type &type) {
    this->memb_arr = type;
}

nxc::ID nxc::struct_data::getScopeID() const {
    return this->scope;
}

void nxc::struct_data::setScopeID(nxc::ID id) {
    this->scope = id;
}

void nxc::struct_data::setScopeID(nxc::scope_data *scope) {
    this->scope = scope->getID();
}

void nxc::struct_data::setScopeID(nxc::scope_data &scope) {
    this->scope = scope.getID();
}

bool nxc::struct_data::wasPrinted() const {
    return this->printed;
}

void nxc::struct_data::setPrinted(bool printed) {
    this->printed = printed;
}

nxc::type &nxc::sym_type::getType() {
    return this->type;
}

const nxc::type &nxc::sym_type::getType() const {
    return this->type;
}

void nxc::sym_type::setType(const nxc::type &type) {
    this->type = type;
}

nxc::ID nxc::symbol_table::find_parent_scope(nxc::ID scope) {
    nxc::scope_data &data = get_scope(scope);
    return data.getParentID();
}

nxc::var_data &nxc::symbol_table::new_var() {
    return allocate<var_data>();
}

nxc::scope_data &nxc::symbol_table::new_scope(nxc::ID parent) {
    return allocate<scope_data>(parent);
}

nxc::scope_data &nxc::symbol_table::new_scope(nxc::scope_data &parent) {
    return allocate<scope_data>(parent.getID());
}


nxc::task_data &nxc::symbol_table::new_task() {
    return allocate<task_data>();
}

nxc::func_data &nxc::symbol_table::new_func() {
    return allocate<func_data>();
}

nxc::typedef_data &nxc::symbol_table::new_typedef() {
    return allocate<typedef_data>();
}

nxc::enum_data &nxc::symbol_table::new_enum() {
    return allocate<enum_data>();
}

nxc::struct_data &nxc::symbol_table::new_struct() {
    return allocate<struct_data>();
}

nxc::var_data &nxc::symbol_table::get_var(nxc::ID id) {
    return get<var_data, sym_class::VARIABLE>(id);
}

nxc::scope_data &nxc::symbol_table::get_scope(nxc::ID id) {
    return get<scope_data, sym_class::SCOPE>(id);
}

nxc::task_data &nxc::symbol_table::get_task(nxc::ID id) {
    return get<task_data, sym_class::TASK>(id);
}

nxc::func_data &nxc::symbol_table::get_func(nxc::ID id) {
    return get<func_data, sym_class::FUNCTION>(id);
}

nxc::typedef_data &nxc::symbol_table::get_typedef(nxc::ID id) {
    return get<typedef_data, sym_class::TYPEDEF>(id);
}

nxc::enum_data &nxc::symbol_table::get_enum(nxc::ID id) {
    return get<enum_data, sym_class::ENUM>(id);
}

nxc::struct_data &nxc::symbol_table::get_struct(nxc::ID id) {
    return get<struct_data, sym_class::STRUCT>(id);
}

nxc::sym_class nxc::symbol_table::which(nxc::ID id) const {
    return (sym_class) at(id).which();
}


nxc::scope_data &nxc::symbol_table::global_scope() {
    return m_global_ref;
}

const nxc::scope_data &nxc::symbol_table::global_scope() const {
    return m_global_ref;
}

nxc::symbol_table::symbol_type &nxc::symbol_table::get_raw(nxc::ID id) {
    return at(id);
}

bool nxc::arrays::operator==(const nxc::arrays &other) const {
    return other.size_arr == this->size_arr
           && other.subtype.get() == this->subtype.get();
}

bool nxc::arrays::operator!=(const nxc::arrays &other) const {
    return !(*this == other);
}


bool nxc::symbol_table::detect_usertype_offender(ID scope, std::string const &name) {
    auto it = this->begin();
    auto end = this->end();
    for (; it != end; ++it) {
        auto &sym = it->second;
        std::string localname;
        sym_class which = static_cast<sym_class>(sym.which());
        switch (which) {
            default:
            case sym_class::VARIABLE:
            case sym_class::SCOPE:
            case sym_class::TASK:
            case sym_class::FUNCTION:
                continue;
            case sym_class::TYPEDEF: {
                typedef_data &data = boost::get<typedef_data>(sym);
                if (data.getParentID() != scope)
                    continue;
                localname = data.getName();
                break;
            }
            case sym_class::ENUM: {
                enum_data &data = boost::get<enum_data>(sym);
                if (data.getParentID() != scope)
                    continue;
                localname = data.getName();
                break;
            }
            case sym_class::STRUCT: {
                struct_data &data = boost::get<struct_data>(sym);
                if (data.getParentID() != scope)
                    continue;
                localname = data.getName();
                break;
            }
        }
        if (localname == name) {
            return true;
        }
    }
    return false;
}

bool nxc::symbol_table::detect_var_offender(ID scope, std::string const &name) {
    auto it = this->begin();
    auto end = this->end();
    for (; it != end; ++it) {
        auto &sym = it->second;
        sym_class which = static_cast<sym_class>(sym.which());
        switch (which) {
            default:
            case sym_class::SCOPE:
            case sym_class::TASK:
            case sym_class::FUNCTION:
            case sym_class::STRUCT:
            case sym_class::TYPEDEF:
                continue;
            case sym_class::ENUM: {
                enum_data const &enumik = boost::get<enum_data>(sym);
                if (enumik.getParentID() != scope)
                    continue;
                for (enum_data::member_type const &memb : enumik.getMembers()) {
                    if (memb.first == name) {
                        return true;
                    }
                }
                break;
            }
            case sym_class::VARIABLE: {
                var_data const &var = boost::get<var_data>(sym);
                if (var.getParentID() != scope)
                    continue;
                if (var.getName() == name) {
                    return true;
                }
                break;
            }
        }
    }
    return false;
}
