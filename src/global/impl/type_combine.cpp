/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Type combining logic (casting/type compatibility).
 */


#include <global/include/type_combine.hpp>


void throwTypeError(std::string &&what, nxc::type a, nxc::type b) {
    std::stringstream msg;
    msg << what << ": ";
    msg << a.operator std::string();
    msg << " X ";
    msg << a.operator std::string() << ".";
    throw nxc::semantic_error(-1, -1, "", msg.str().c_str());
}

void nxc::type_combine::combine(nxc::type const &a, nxc::type const &b, nxc::type &result) {
    nxc::type rawA = untypedefize(a, tbl), rawB = untypedefize(b, tbl);

    type_variants whichA = rawA.which();
    type_variants whichB = rawB.which();

    if (whichA == type_variants::variant_t){
        result = b;
        return;
    }
    if (whichB == type_variants::variant_t) {
        result = a;
        return;
    }
    if (whichA == type_variants::void_t || whichB == type_variants::void_t) {
        if (whichA == whichB) {
            result = nxc::type(void_type{});
            return;
        } else {
            throwTypeError("Cannot combine non-void with void", rawA, rawB);
        }
    }

    switch(whichA) {
        case type_variants::basic: {
            switch (b.which()) {
                case type_variants::basic:
                    return combine_basic(rawA, rawB, result);
                case type_variants::array:
                    throwTypeError("Combining array with a basic type", rawA, rawB);
                case type_variants::user:
                    return combine(rawB, rawA, result); // flip
                default:
                    break;
            }
        }
        case type_variants::array:{
            if (rawB.which() != type_variants::array) {
                throwTypeError("Combining array with a non-array", rawA, rawB);
            }
            return combine_array(rawA, rawB, result);
        }
        case type_variants::user:{
            usertype const &userA = rawA.get<usertype>();
            usertype_kind kindA = whichtype(userA, tbl);

            if (kindA == usertype_kind::struct_kind) {
                return combine_struct(rawA, rawB, result);
            } else /*if (kindA == usertype_kind::enum_kind)*/ {
                nxc::type foo(enum_datatype);
                return combine(rawB, foo, result); // flip and replace
            }
        }
        default:
            break;
    }
}

void nxc::type_combine::combine_struct(nxc::type const &struct_, nxc::type const &other, nxc::type &result) {
    usertype const &userA = struct_.get<usertype>(); // struct_ must be struct

    if (other.which() != type_variants::user) {
        throwTypeError("Combining struct type with basic type", struct_, other);
    }
    usertype const &userB = other.get<usertype>();
    if (whichtype(userB, tbl) != usertype_kind::struct_kind) {
        throwTypeError("Combining struct type with enum type", struct_, other);
    }
    if (userA.getID() == userB.getID()) {
        result.setInner(userA);
        result.constFusion(struct_, other);
    } else {
        throwTypeError("Structs are different types", struct_, other);
    }
}

void nxc::type_combine::combine_array(nxc::type const &a, nxc::type const &b, nxc::type &result) {
    nxc::type out_sub;
    nxc::arrays out;
    nxc::arrays const &arr1 = a.get<nxc::arrays>();
    nxc::arrays const &arr2 = b.get<nxc::arrays>();
    nxc::type const &sub1 = arr1.base().get();
    nxc::type const &sub2 = arr2.base().get();
    if (arr1.sizes().size() != arr2.sizes().size()) {
        throwTypeError("Combining arrays with different dimension count", a, b);
    }
    combine(sub1, sub2, out_sub);
    out.base(out_sub);
    result.constFusion(a, b);
    for (size_t i = 0, len = arr1.sizes().size(); i < len; i++) {
        int64_t size1 = arr1.sizes()[i], size2 = arr2.sizes()[i];

        if (size1 == SIZE_UNKNOWN || size2 == SIZE_UNKNOWN) {
            if (size1 == SIZE_UNKNOWN && size2 == SIZE_UNKNOWN) {
                out.sizes()[i] = SIZE_UNKNOWN;
            }
            out.sizes()[i] = (arr1.sizes()[i] == SIZE_UNKNOWN) ? (arr2.sizes()[i]) : (arr1.sizes()[i]);
        }
        if (size1 == size2) {
            out.sizes()[i] = size1;
        } else {
            throwTypeError("Combining arrays with different dimension values", a, b);
        }
    }
    result.setInner(out);
}

void nxc::type_combine::combine_basic(nxc::type const &a, nxc::type const &b, nxc::type &result) {
    nxc::basic_type tA = a.get<basic_type>();
    nxc::basic_type tB = b.get<basic_type>();
    if (tA == tB) {
        result = a;
        return;
    }
    type_cat scoreA = score(tA), scoreB = score(tB);
    sign_cat signA = sign(tA), signB = sign(tB);
    // eliminate string and mutex (handled on tA==tB)
    if (scoreA == type_cat::score_invalid || scoreB == type_cat::score_invalid) {
        throwTypeError("Combining string/mutex with integer type", a, b);
    }
    type_cat score = (type_cat) std::max((int) scoreA, (int) scoreB);
    sign_cat sign = (sign_cat) std::max((int) signA, (int) signB);
    result.setInner(synthesis(score, sign));
    result.constFusion(a, b);
}

nxc::type_cat nxc::type_combine::score(nxc::basic_type t) {
    switch (t) {
        case basic_type::bool_type:
            return type_cat::score_bool;
        case basic_type::byte_type:
        case basic_type::schar_type:
        case basic_type::uchar_type:
            return type_cat::score_byte;
        case basic_type::sint_type:
        case basic_type::uint_type:
        case basic_type::sshort_type:
        case basic_type::ushort_type:
            return type_cat::score_short;
        case basic_type::slong_type:
        case basic_type::ulong_type:
            return type_cat::score_long;
        case basic_type::float_type:
            return type_cat::score_float;
        case basic_type::mutex_type:
        case basic_type::string_type:
            return type_cat::score_invalid;
    }
}

nxc::sign_cat nxc::type_combine::sign(nxc::basic_type t) {
    switch (t) {
        case basic_type::bool_type:
        case basic_type::byte_type:
        case basic_type::uchar_type:
        case basic_type::uint_type:
        case basic_type::ushort_type:
        case basic_type::ulong_type:
            return sign_cat::sign_unsigned;

        case basic_type::schar_type:
        case basic_type::sint_type:
        case basic_type::sshort_type:
        case basic_type::slong_type:
        case basic_type::float_type:
            return sign_cat::sign_signed;

        case basic_type::mutex_type:
        case basic_type::string_type:
            return sign_cat::sign_invalid;
    }
}

nxc::basic_type nxc::type_combine::synthesis(type_cat basis, sign_cat sign) {
    switch (basis) {
        case type_cat::score_bool:
            return nxc::basic_type::bool_type;

        case type_cat::score_byte:
            return (sign == sign_cat::sign_signed ? nxc::basic_type::schar_type : nxc::basic_type::uchar_type);

        case type_cat::score_short:
            return (sign == sign_cat::sign_signed ? nxc::basic_type::sshort_type : nxc::basic_type::ushort_type);

        case type_cat::score_long:
            return (sign == sign_cat::sign_signed ? nxc::basic_type::slong_type : nxc::basic_type::ulong_type);

        case type_cat::score_float:
            return nxc::basic_type::float_type;

        case type_cat::score_invalid:
            return basic_type::sint_type;
    }
}


nxc::compat_score nxc::type_combine::compare(nxc::type tAct, nxc::type tReq) {
    tAct = untypedefize(tAct, tbl);
    tReq = untypedefize(tReq, tbl);
    unenumize(tAct);
    unenumize(tReq);
    if (tAct.which() == type_variants::variant_t || tReq.which() == type_variants::variant_t) {
        return nxc::compat_score::exact;
    }
    if (tAct.which() == type_variants::void_t || tReq.which() == type_variants::void_t) {
        return tAct.which() == tReq.which() ? nxc::compat_score::exact : nxc::compat_score::incompatible;
    }
    if (tAct.which() != tReq.which()) // array == array && struct == struct && basic == basic
        return compat_score::incompatible;
    switch (tAct.which()) {
        case type_variants::basic: {
            basic_type t1 = tAct.get<basic_type>();
            basic_type t2 = tReq.get<basic_type>();
            return compare_basic(t1, t2);
        }
        case type_variants::array: {
            arrays &t1 = tAct.get<arrays>();
            arrays &t2 = tReq.get<arrays>();
            return compare_arrays(t1, t2);
        }
        case type_variants::user: {
            ID i1 = tAct.get<usertype>().getID();
            ID i2 = tReq.get<usertype>().getID();
            return i1 == i2 ? compat_score::exact : compat_score::incompatible;
        }
        default:
            return nxc::compat_score::incompatible;
    }
}

void nxc::type_combine::unenumize(nxc::type &t) {
    if (t.which() == type_variants::user) {
        usertype &user = t.get<usertype>();
        usertype_kind kind = whichtype(user, tbl);
        if (kind == usertype_kind::enum_kind) {
            t.setInner(enum_datatype);
        }
    }
}

nxc::compat_score nxc::type_combine::compare_basic(nxc::basic_type tAct, nxc::basic_type tReq) {
    if (tAct == basic_type::string_type) {
        return tReq == basic_type::string_type ? compat_score::exact : compat_score::incompatible;
    } else if (tAct == basic_type::mutex_type) {
        return tReq == basic_type::mutex_type ? compat_score::exact : compat_score::incompatible;
    }
    type_cat catAct = score(tAct);
    type_cat catReq = score(tReq);
    if (catAct == catReq) {
        sign_cat signAct = sign(tAct);
        sign_cat signReq = sign(tReq);
        return signAct == signReq ? compat_score::exact : compat_score::signcast;
    } else {
        return compat_score::numcast;
    }
}


nxc::compat_score nxc::type_combine::compare_arrays(nxc::arrays &tAct, nxc::arrays &tReq) {
    compat_score score = compare(tAct.base().get(), tReq.base().get());
    if (score != compat_score::exact && score != compat_score::signcast)
        return compat_score::incompatible;
    if (tAct.sizes().size() != tReq.sizes().size())
        return compat_score::incompatible;
    for (size_t i = 0, len = tAct.sizes().size(); i < len; i++) {
        arrays::size_type sAct = tAct.sizes()[i];
        arrays::size_type sReq = tReq.sizes()[i];
        if (sAct == sReq)
            continue; // good
        if (sAct != SIZE_UNKNOWN && sReq != SIZE_UNKNOWN && sAct != sReq)
            return compat_score::incompatible;
        if (sAct == SIZE_UNKNOWN) {
            // sReq != SIZE_UNKNOWN
            // upcast
            score = compat_score::numcast;
        }
    }

    return score;
}
