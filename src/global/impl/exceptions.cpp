/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Common exceptions.
 * - syntax error
 *   - unexpected token
 *   - malformed declaration
 *   - unspecified parser failure
 * - semantic error
 * - serialization error
 */


#include <global/include/nxcexcept.hpp>
#include <lexer/token2_ids.hpp>
#include <lexer/token2_util.hpp>
#include <interface/lexer.hpp>


nxc::syntax_error::syntax_error(int row,
                                int col,
                                const std::string &file,
                                const std::string &tokValue,
                                uint32_t tokType,
                                const char *message)
        : syntax_error(row, col, file, tokValue, tokType, message, "syntax error") {}

nxc::syntax_error::syntax_error(int row,
                                int col,
                                const std::string &file,
                                const std::string &tokValue,
                                uint32_t tokType,
                                const char *message,
                                const char *what)
        : row(row),
          col(col),
          file(file),
          value(tokValue),
          type(tokType),
          message(message) {
    auto tokNameBad = boost::wave::get_token_name((boost::wave::token2_id) tokType);
    std::string tokName(tokNameBad.begin(), tokNameBad.end());
    std::stringstream str;
    str << createLocation(file, row, col) << ": ";

    str << what << ": " << message << ": ";
    str << "found token " << "[" << tokName << "] '" << tokValue << "'";
    summary = str.str();
}

nxc::unexpected_token::unexpected_token(int row,
                                        int col,
                                        const std::string &file,
                                        const std::string &tokValue,
                                        uint32_t tokType,
                                        uint32_t expected,
                                        const char *message)
        : syntax_error(row,
                       col,
                       file,
                       tokValue,
                       tokType,
                       message) {
    auto tokNameBad = boost::wave::get_token_name((boost::wave::token2_id) tokType);
    auto tokNameExBad = boost::wave::get_token_name((boost::wave::token2_id) expected);
    std::string tokName(tokNameBad.begin(), tokNameBad.end());
    std::string tokNameEx(tokNameExBad.begin(), tokNameExBad.end());
    std::stringstream str;
    str << createLocation(file, row, col) << ": ";

    str << "Unexpected token: " << message << " - ";
    str << "expected [" << tokNameEx << "], ";
    str << "found " << "[" << tokName << "] '" << tokValue << "'";
    summary = str.str();
}

nxc::malformed_declaration::malformed_declaration(int row,
                                                  int col,
                                                  const std::string &file,
                                                  const std::string &tokValue,
                                                  uint32_t tokType,
                                                  const char *message)
        : syntax_error(row, col, file, tokValue, tokType, message, "malfolmed declaration") {}

nxc::parsing_failed::parsing_failed(int row,
                                    int col,
                                    const std::string &file,
                                    const std::string &tokValue,
                                    uint32_t tokType,
                                    const char *message)
        : syntax_error(row, col, file, tokValue, tokType, message, "parsing failed") {}

nxc::serialize_error::serialize_error(int row,
                                      int col,
                                      const std::string &file,
                                      const char *message)
        : serialize_error(row, col, file, message, "Serialization error") {}

nxc::serialize_error::serialize_error(int row, int col, const std::string &file, const char *message,
                                      const char *what)
        : row(row), col(col), file(file), message(message) {
    std::stringstream str;
    str << createLocation(file, row, col) << ": " << what << ": " << message;
    summary = str.str();
}

nxc::semantic_error::semantic_error(int row,
                                    int col,
                                    const std::string &file,
                                    const char *message)
        : semantic_error(createLocation(file, row, col), message) {}

nxc::semantic_error::semantic_error(int row, int col, const std::string &file, const char *message,
                                    const char *what)
        : semantic_error(createLocation(file, row, col), message, what) {}

nxc::semantic_error::semantic_error(const nxc::my_pos &pos, const char *message)
        : semantic_error(pos, message, "Semantic error") {}

nxc::semantic_error::semantic_error(const nxc::my_pos &pos, const char *message, const char *what)
        : row(pos.row), col(pos.col), file(pos.file), message(message) {
    std::stringstream str;
    str << pos << ": " << what << ": " << message;
    summary = str.str();
}
