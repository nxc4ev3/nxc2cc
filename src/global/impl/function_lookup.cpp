/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Overloaded function chooser logic – compatibility score calculation.
 */


#include <climits>
#include "global/include/function_lookup.hpp"

nxc::func_data *nxc::function_lookup::find(const std::string &name, const std::vector<nxc::type> &args) {
    std::vector<std::pair<func_data *, int>> scores;

    for (size_t i = 0, len = syms.size(); i < len; ++i) {
        if (syms.which(i) != sym_class::FUNCTION)
            continue;
        func_data &data = syms.get_func(i);

        int score = 0;
        bool compatible = calcScore(data, name, args, score);
        if (!compatible)
            continue;

        std::pair<func_data *, int> pair{
                &data,
                score
        };
        scores.push_back(std::move(pair));
    }

    return getBest(scores, name);
}

bool nxc::function_lookup::calcScore(const func_data &reqFn,
                                     const std::string &actName,
                                     const std::vector<nxc::type> &actArgs,
                                     int &sum) {

    if (reqFn.getName() != actName)
        return false;

    const func_data::arg_list_type &reqArgs = reqFn.getArguments();

    if (reqArgs.size() != actArgs.size())
        return false;

    sum = 0;
    auto it = reqArgs.begin();
    auto end = reqArgs.end();
    for (size_t arg = 0; it != end; ++arg, ++it) {
        nxc::type const &req = it->first;
        nxc::type const &act = actArgs[arg];
        compat_score score = combiner.compare(act, req);
        switch (score) {
            case compat_score::numcast: // decrease score
                sum--;
                break;
            case compat_score::signcast: // let the score be as it is
            case compat_score::exact:
                break;
            case compat_score::incompatible: // skip this function
                return false;
        }
    }
    return true;
}

nxc::func_data *nxc::function_lookup::getBest(const std::vector<std::pair<func_data *, int>> &scores,
                                              const std::string &name) {
    // get max score + max function
    int max = INT_MIN;
    bool dupl = false;
    func_data *ptr = nullptr;
    for (auto &pair : scores) {
        if (max == pair.second) {
            dupl = true;
        } else if (max < pair.second) {
            max = pair.second;
            dupl = false;
            ptr = pair.first;
        }
    }
    if (dupl) {
        std::string msg("Ambiguous function call: ");
        msg.append(name);
        throw semantic_error(-1, -1, "", msg.c_str());
    }
    return ptr;
}
