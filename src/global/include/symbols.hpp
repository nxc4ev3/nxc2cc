/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Symbol table infrastructure.
 */


#ifndef CUSTOM_ID_SYMBOLS_HPP
#define CUSTOM_ID_SYMBOLS_HPP

#include <global/include/typesystem.hpp>
#include <global/include/nxcexcept.hpp>
#include <parser2/include/ast/enums.hpp>
#include <boost/variant/get.hpp>
#include <boost/variant/variant.hpp>
#include <boost/optional/optional.hpp>
#include <unordered_map>
#include <forward_list>
#include <list>
#include <set>

//////////////////
// VALUE MACROS //
//////////////////

#define SCOPE_GLOBAL   0
#define SCOPE_UNKNOWN -1
#define SCOPE_NONE    -2

#define ID_UNKNOWN    -1

//////////////////
// ENUMERATIONS //
//////////////////

namespace nxc {
    enum class sym_class {
        VARIABLE = 0,
        SCOPE = 1,
        TASK = 2,
        FUNCTION = 3,
        TYPEDEF = 4,
        ENUM = 5,
        STRUCT = 6,
        ERROR = -1
    };

    enum class sym_mask {
        NONE = 0x00,
        VARIABLE = 0x01,
        SCOPE = 0x02,
        TASK = 0x04,
        FUNCTION = 0x08,
        TYPEDEF = 0x10,
        ENUM = 0x20,
        STRUCT = 0x40,
    };

////////////////////
// ENUM FUNCTIONS //
////////////////////

    inline nxc::sym_mask map_symclass(nxc::sym_class sym) {
        switch (sym) {
            case nxc::sym_class::VARIABLE:
                return nxc::sym_mask::VARIABLE;
            case nxc::sym_class::SCOPE:
                return nxc::sym_mask::SCOPE;
            case nxc::sym_class::TASK:
                return nxc::sym_mask::TASK;
            case nxc::sym_class::FUNCTION:
                return nxc::sym_mask::FUNCTION;
            case nxc::sym_class::TYPEDEF:
                return nxc::sym_mask::TYPEDEF;
            case nxc::sym_class::ENUM:
                return nxc::sym_mask::ENUM;
            case nxc::sym_class::STRUCT:
                return nxc::sym_mask::STRUCT;
            case nxc::sym_class::ERROR:
                return nxc::sym_mask::NONE;
        }
    }

    inline constexpr nxc::sym_mask operator&(nxc::sym_mask __x, nxc::sym_mask __y) {
        return static_cast<nxc::sym_mask>(static_cast<int>(__x) & static_cast<int>(__y));
    }

    inline constexpr nxc::sym_mask operator|(nxc::sym_mask __x, nxc::sym_mask __y) {
        return static_cast<nxc::sym_mask>(static_cast<int>(__x) | static_cast<int>(__y));
    }

    inline constexpr nxc::sym_mask operator^(nxc::sym_mask __x, nxc::sym_mask __y) {
        return static_cast<nxc::sym_mask>(static_cast<int>(__x) ^ static_cast<int>(__y));
    }

    inline constexpr nxc::sym_mask operator~(nxc::sym_mask __x) {
        return static_cast<nxc::sym_mask>(~static_cast<int>(__x));
    }

    inline nxc::sym_mask &operator&=(nxc::sym_mask &__x, nxc::sym_mask __y) {
        __x = __x & __y;
        return __x;
    }

    inline nxc::sym_mask &operator|=(nxc::sym_mask &__x, nxc::sym_mask __y) {
        __x = __x | __y;
        return __x;
    }

    inline nxc::sym_mask &operator^=(nxc::sym_mask &__x, nxc::sym_mask __y) {
        __x = __x ^ __y;
        return __x;
    }


    inline bool matches(nxc::sym_class sym, nxc::sym_mask mask) {
        return (mask & map_symclass(sym)) != nxc::sym_mask::NONE;
    }

    inline bool matches(int sym, nxc::sym_mask mask) {
        return (mask & map_symclass(static_cast<nxc::sym_class>(sym))) != nxc::sym_mask::NONE;
    }

    template<typename Common, typename T1, typename T2>
    inline bool compare(T1 sym1, T2 sym2) {
        return static_cast<Common>(sym1) == static_cast<Common>(sym2);
    }

    template<typename T>
    inline bool enum_is(int val1, T val2) {
        return compare<int>(val1, val2);
    }

    template<typename T>
    inline bool enum_is(T val1, int val2) {
        return compare<int>(val1, val2);
    }

}

/////////////
// CLASSES //
/////////////

namespace nxc {

    /////////////////////////
    // COMMON SUPERCLASSES //
    /////////////////////////

    ////////////////
    // SYMBOL NAME
    //
    class sym_name {
    public:
        sym_name() {}

        sym_name(std::string const &name) : name(name) {}

        sym_name(std::string &&name) : name(std::move(name)) {}

        std::string const &getName() const;

        template<typename T>
        void setName(T &&name) { this->name = std::forward<T>(name); }

    private:
        std::string name;
    };

    //////////////
    // SYMBOL ID
    //
    class sym_id {
    public:
        sym_id() : this_id(ID_UNKNOWN) {}

        explicit sym_id(ID id) : this_id(id) {}

        ID getID() const;

        void setID(ID id);

        operator ID() const { return this->getID(); }

    private:
        ID this_id;
    };

    /////////////////////////////////
    // SYMBOL ID + SYMBOL PARENT ID
    //

    class scope_data;

    class sym_parent_id : public sym_id {
    public:
        sym_parent_id() : sym_id() {}

        explicit sym_parent_id(ID id) : sym_id(id) {}

        explicit sym_parent_id(ID id, ID parent) : sym_id(id), parent_id(parent) {}

        ID getParentID() const;

        void setParentID(ID id);

        void setParentID(scope_data const &scope);

    private:
        ID parent_id = SCOPE_UNKNOWN;
    };

    ////////////////
    // SYMBOL TYPE
    //
    class sym_type {
    public:
        sym_type() {}

        sym_type(nxc::type const &type) : type(type) {}

        sym_type(nxc::type &&type) : type(std::move(type)) {}

        nxc::type &getType();

        nxc::type const &getType() const;

        void setType(nxc::type const &type);

    private:
        nxc::type type;
    };

    /////////////////////
    // MAIN STRUCTURES //
    /////////////////////

    //////////////////
    // VARIABLE DATA
    //
    class var_data : public sym_parent_id, public sym_name, public sym_type {
    public:
        var_data() : sym_parent_id() {}

        var_data(ID id) : sym_parent_id(id) {}

        bool isStatic() const;

        void setStatic(bool static_);

        bool isArgument() const;

        void setArgument(bool arg);

        bool hasValue() const;

        immediate_type getValue() const;

        void resetValue();

        void setValue(immediate_type val);

    private:
        bool is_static = false;
        bool is_arg = false;
        boost::optional<immediate_type> const_int_value;
    };

    ///////////////
    // SCOPE DATA
    //
    class scope_data : public sym_parent_id {
    public:
        typedef ID var_type;
        typedef std::vector<var_type> var_list_type;

        scope_data() : sym_parent_id() {}

        scope_data(ID id) : sym_parent_id(id) {}

        scope_data(ID id, ID parent) : sym_parent_id(id, parent) {}

        var_list_type &vars();

        var_list_type const &vars() const;

        void vars(var_list_type const &list);

    private:
        var_list_type variables;
    };

    //////////////
    // TASK DATA
    //
    class task_data : public sym_id, public sym_name {
    public:
        task_data() : sym_id() {}

        task_data(ID id) : sym_id(id) {}

        ID getBodyID() const;

        void setBodyID(ID id);

        ID getParentID() const { return SCOPE_GLOBAL; }

    private:
        ID body_scope = SCOPE_UNKNOWN;
    };


    //////////////////
    // FUNCTION DATA
    //
    class func_data : public sym_id, public sym_name {
    public:
        typedef std::pair<type, std::string> arg_type;
        typedef std::list<arg_type> arg_list_type;
        typedef boost::optional<type> return_type;
        typedef FunctionModifierMask flag_list_type;

        func_data() : sym_id() {}

        func_data(ID id) : sym_id(id) {}

        ID getBodyID() const;

        void setBodyID(ID id);

        return_type &getReturnType();

        return_type const &getReturnType() const;

        void setReturnType(return_type const &ref);

        arg_list_type &getArguments();

        arg_list_type const &getArguments() const;

        void setArguments(arg_list_type const &ref);

        flag_list_type &getFlags();

        flag_list_type const &getFlags() const;

        void setFlags(flag_list_type const &ref);

        bool wasIntroduced() const;

        void setIntroduced(bool introduced);

        ID getParentID() const { return SCOPE_GLOBAL; }

    private:
        return_type retval;
        arg_list_type args;
        flag_list_type flags;
        ID body_scope = SCOPE_UNKNOWN;
        bool introduced = false;
    };


    /////////////////
    // TYPEDEF DATA
    //
    class typedef_data : public sym_parent_id, public sym_name, public sym_type {
    public:
        typedef_data() : sym_parent_id() {}

        typedef_data(ID id) : sym_parent_id(id) {}

    };


    //////////////
    // ENUM DATA
    //
    class enum_data : public sym_parent_id, public sym_name {
    public:
        typedef std::pair<std::string, immediate_type> member_type;
        typedef std::list<member_type> member_list_type;

        enum_data() : sym_parent_id() {}

        enum_data(ID id) : sym_parent_id(id) {}

        member_list_type &getMembers();

        member_list_type const &getMembers() const;

        void setMembers(member_list_type const &type);

        bool isComplete() const;

        void setComplete(bool complete);

        bool wasPrinted() const;

        void setPrinted(bool printed);

    private:
        member_list_type constants;
        bool complete = false;
        bool printed = false;
    };


    ////////////////
    // STRUCT DATA
    //
    struct struct_data : public sym_parent_id, public sym_name {
    public:
        typedef ID member_type;
        typedef std::vector<member_type> member_list_type;

        struct_data() : sym_parent_id() {}

        struct_data(ID id) : sym_parent_id(id) {}

        member_list_type &members();

        member_list_type const &members() const;

        void members(member_list_type const &type);

        ID getScopeID() const;

        void setScopeID(ID id);

        void setScopeID(scope_data *scope);

        void setScopeID(scope_data &scope);

        bool wasPrinted() const;

        void setPrinted(bool printed);

    private:
        ID scope = SCOPE_UNKNOWN;
        member_list_type memb_arr;
        bool printed = false;
    };

    //////////////////
    // SYMBOL TABLE //
    //////////////////

    class symbol_table {
    public:
        typedef boost::variant<
                var_data,
                scope_data,
                task_data,
                func_data,
                typedef_data,
                enum_data,
                struct_data> symbol_type;
        typedef std::forward_list<symbol_type> storage_type;
        typedef std::unordered_map<ID, symbol_type &> mapping_type;
        typedef mapping_type::iterator iterator_type;
        typedef mapping_type::const_iterator const_iterator_type;


        symbol_table() : m_id_counter(SCOPE_GLOBAL),
                         m_global_ref(new_scope(SCOPE_NONE)) {}

        var_data &new_var();

        scope_data &new_scope(ID parent = SCOPE_UNKNOWN);

        scope_data &new_scope(scope_data &parent);

        task_data &new_task();

        func_data &new_func();

        typedef_data &new_typedef();

        enum_data &new_enum();

        struct_data &new_struct();


        var_data &get_var(ID id);

        scope_data &get_scope(ID id);

        task_data &get_task(ID id);

        func_data &get_func(ID id);

        typedef_data &get_typedef(ID id);

        enum_data &get_enum(ID id);

        struct_data &get_struct(ID id);

        inline iterator_type begin() { return m_map.begin(); }

        inline const_iterator_type begin() const { return m_map.cbegin(); }

        inline iterator_type end() { return m_map.end(); }

        inline const_iterator_type end() const { return m_map.cend(); }

        sym_class which(ID id) const;

        size_t size() const { return m_map.size(); }

        symbol_type &get_raw(ID id);

        template<typename T, sym_class which>
        T &get(ID id);

        scope_data &global_scope();

        scope_data const &global_scope() const;

        ID find_parent_scope(ID scope);

        // search specified type inside specified scope
        template<typename Datatype, sym_class which>
        bool search_scope(ID scope,
                          std::string const &name,
                          Datatype *&found);


        template<typename T, sym_class which>
        bool find_everywhere(std::string const &name, T *&ref);

        template<typename Datatype, typename... Args>
        Datatype &allocate(Args... args);

        template<typename lookup_scopes_fn, typename... Args>
        bool lookup_scopes(ID start,
                           lookup_scopes_fn predicate,
                           Args &&... info);

        template<typename lookup_symbols_fn, typename... Args>
        bool lookup_symbols(ID start,
                            sym_mask allowed,
                            lookup_symbols_fn predicate,
                            Args &&... info);

        bool detect_usertype_offender(ID scope, std::string const &name);

        bool detect_var_offender(ID scope, std::string const &name);

    private:
        inline symbol_type &at(ID i) { return m_map.at(i); }

        inline const symbol_type &at(ID i) const {
            validate(i);
            return m_map.at(i);
        }

        inline void validate(ID i) const {
            if (0 > i || i >= size())
                throw invalid_id_exception(i, "validate");
        };

        ID m_id_counter = 0;
        storage_type m_storage;
        mapping_type m_map;
        scope_data &m_global_ref;
    };
}


//////////////////////
// GLOBAL FUNCTIONS //
//////////////////////

namespace nxc {

    inline usertype_kind whichtype(usertype const &type, symbol_table const &table) {
        return static_cast<usertype_kind> (table.which(type.getID()));
    }

    type untypedefize(type web, symbol_table &tbl);
    type untypedefize(arrays web, symbol_table &tbl);
    type untypedefize(basic_type web, symbol_table &tbl);
    type untypedefize(usertype web, symbol_table &tbl);
    type untypedefize(void_type web, symbol_table &tbl);
    type untypedefize(variant_type web, symbol_table &tbl);
}

#include <global/impl/symbols.tpp>

#endif //CUSTOM_ID_SYMBOLS_HPP
