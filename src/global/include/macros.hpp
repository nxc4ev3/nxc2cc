/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Common headers & macros.
 */


#ifndef CUSTOM_ID_MACROS_HPP
#define CUSTOM_ID_MACROS_HPP


////////////////////
// COMMON HEADERS //
////////////////////

#include <stdio.h>
#include <cstdint>


///////////////////
// COMMON MACROS //
///////////////////

#ifndef ID_UNKNOWN
#define ID_UNKNOWN -1
#endif


/////////////////////
// COMMON TYPEDEFS //
/////////////////////

namespace nxc {
    typedef ssize_t ID;
    typedef int64_t immediate_type;
}

#endif //CUSTOM_ID_MACROS_HPP
