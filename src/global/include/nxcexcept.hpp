/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Common exceptions.
 * - internal error
 *   - invalid symbol ID query
 *   - invalid symbol cast
 * - syntax error
 *   - unexpected token
 *   - malformed declaration
 *   - unspecified parser failure
 * - semantic error
 * - serialization error
 */


#ifndef CUSTOM_ID_NXCEXCEPT_HPP
#define CUSTOM_ID_NXCEXCEPT_HPP

#include <exception>
#include <string>
#include <sstream>
#include <global/include/macros.hpp>


namespace nxc {
    struct my_pos;

    class invalid_id_exception : public std::exception {
    public:
        invalid_id_exception() : id(-1) {
            std::ostringstream stream;
            stream << "Invalid symbol identifier query.";
            str = stream.str();
        }

        explicit invalid_id_exception(ID id) : id(id) {
            std::ostringstream stream;
            stream << "Invalid symbol identifier query: " << id << ".";
            str = stream.str();
        }

        explicit invalid_id_exception(ID id, const char *component) : id(id) {
            std::ostringstream stream;
            stream << "[" << component << "] Invalid symbol identifier query: " << id << ".";
            str = stream.str();
        }

        explicit invalid_id_exception(ID id, const std::string &component) : id(id) {
            std::ostringstream stream;
            stream << "[" << component << "] Invalid symbol identifier query: " << id << ".";
            str = stream.str();
        }

        virtual ~invalid_id_exception() {}

        virtual const char *what() const throw() {
            return str.c_str();
        }

        inline ID getID() const { return id; }


    private:
        ID id;
        std::string str;
    };

    class bad_symclass_exception : public std::exception {
    public:
        bad_symclass_exception() : id(ID_UNKNOWN), type(ID_UNKNOWN) {
            std::ostringstream stream;
            stream << "Wrong symbol type query.";
            str = stream.str();
        }

        explicit
        bad_symclass_exception(ID id, int type) : id(id), type(type) {
            std::ostringstream stream;
            stream << "Wrong symbol type query: " << id << " is not " << type << ".";
            str = stream.str();
        }

        explicit
        bad_symclass_exception(ID id, int type, const char *component) : id(id), type(type) {
            std::ostringstream stream;
            stream << "[" << component << "] Wrong symbol type query: " << id << " is not " << type << ".";
            str = stream.str();
        }

        explicit
        bad_symclass_exception(ID id, int type, const std::string &component) : id(id), type(type) {
            std::ostringstream stream;
            stream << "[" << component << "] Wrong symbol type query: " << id << " is not " << type << ".";
            str = stream.str();
        }

        virtual ~bad_symclass_exception() {}

        virtual const char *what() const throw() {
            return str.c_str();
        }

        inline ID getID() const { return id; }

    private:
        ID id;
        int type;
        std::string str;
    };



    class semantic_error : public std::exception {
    public:
        semantic_error() : message("") { summary = "semantic error"; }

        semantic_error(const my_pos &pos,
                       const char *message);
        semantic_error(int row, int col, const std::string &file,
                        const char *message);
        semantic_error(const my_pos &pos,
                       const char *message, const char *what);
        semantic_error(int row, int col, const std::string &file,
                        const char *message, const char *what);

        inline ssize_t getRow() const { return row; }

        inline ssize_t getColumn() const { return col; }

        inline const std::string &getFile() const { return file; }

        inline const char *getMessage() const { return message; }


        inline virtual const char *what() const noexcept { return summary.c_str(); }

    protected:
        ssize_t row, col;
        std::string file;
        const char *message;

        std::string summary;
    };

    class serialize_error : public std::exception {
    public:
        serialize_error() : message("") { summary = "serialize error"; }

        serialize_error(int row, int col, const std::string &file,
                     const char *message);

        serialize_error(int row, int col, const std::string &file,
                     const char *message, const char *what);

        inline ssize_t getRow() const { return row; }

        inline ssize_t getColumn() const { return col; }

        inline const std::string &getFile() const { return file; }

        inline const char *getMessage() const { return message; }


        inline virtual const char *what() const noexcept { return summary.c_str(); }

    protected:
        ssize_t row, col;
        std::string file;
        const char *message;

        std::string summary;
    };
    class syntax_error : public std::exception {
    public:
        syntax_error() : message("") { summary = "syntax error"; }

        syntax_error(int row, int col, const std::string &file,
                     const std::string &tokValue, uint32_t tokType,
                     const char *message);

        syntax_error(int row, int col, const std::string &file,
                     const std::string &tokValue, uint32_t tokType,
                     const char *message, const char *what);

        inline ssize_t getRow() const { return row; }

        inline ssize_t getColumn() const { return col; }

        inline const std::string &getFile() const { return file; }

        inline const char *getMessage() const { return message; }

        inline const std::string &getTokenValue() const { return value; }

        inline uint32_t getTokenType() const { return type; }

        inline virtual const char *what() const noexcept { return summary.c_str(); }

    protected:
        ssize_t row, col;
        std::string file;
        const char *message;

        std::string value;
        uint32_t type;

        std::string summary;
    };

    class unexpected_token : public syntax_error {
    public:
        unexpected_token() : syntax_error() {}

        unexpected_token(int row, int col, const std::string &file,
                         const std::string &tokValue, uint32_t tokType, uint32_t expected,
                         const char *message);

        inline uint32_t getExpectedType() const { return expected; }

    private:
        uint32_t expected;
    };

    class parsing_failed : public syntax_error {
    public:
        parsing_failed() : syntax_error() {}

        parsing_failed(int row, int col, const std::string &file,
                       const std::string &tokValue, uint32_t tokType,
                       const char *message);
    };

    class malformed_declaration : public syntax_error {
    public:
        malformed_declaration() : syntax_error() {}

        malformed_declaration(int row, int col, const std::string &file,
                              const std::string &tokValue, uint32_t tokType,
                              const char *message);
    };
}
#endif //CUSTOM_ID_NXCEXCEPT_HPP
