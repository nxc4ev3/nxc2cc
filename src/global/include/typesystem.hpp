/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * NXC type infrastructure.
 */

#ifndef CUSTOM_ID_TYPESYSTEM_HPP
#define CUSTOM_ID_TYPESYSTEM_HPP

#include <cstdint>
#include <vector>
#include <boost/variant.hpp>
#include <global/include/macros.hpp>

#undef TYPE_DEBUG


//////////////////
// VALUE MACROS //
//////////////////

#define SIZE_UNKNOWN  -1


//////////////////
// ENUMERATIONS //
//////////////////

namespace nxc {
    enum class usertype_kind {
        struct_kind = 6, enum_kind = 5, typedef_kind = 4
    };
    enum class basic_type {
        bool_type,

        byte_type,

        schar_type,
        uchar_type,

        sint_type,
        uint_type,

        sshort_type,
        ushort_type,

        slong_type,
        ulong_type,

        float_type,

        mutex_type,

        string_type
    };

    enum class type_variants {
        basic,
        array,
        user,
        variant_t,
        void_t
    };

    const basic_type enum_datatype = basic_type::sint_type;
    const basic_type char_datatype = basic_type::schar_type;
}

/////////////
// CLASSES //
/////////////

namespace nxc {
    struct type;

    class variant_type {
    public:
        bool operator==(variant_type const &other) const { return true; }
        bool operator!=(variant_type const &other) const { return false; }

    };
    class void_type {
    public:
        bool operator==(void_type const &other) const { return true; }
        bool operator!=(void_type const &other) const { return false; }
    };

    class usertype {
    public:
        explicit usertype(ID id) : id(id) {}

        ID getID() const;

        bool operator==(usertype const &other) const;

        bool operator!=(usertype const &other) const;

        usertype &operator=(const usertype& other) { this->id = other.id; return *this; }

        operator ID() const { return id; }

#ifdef TYPE_DEBUG
        char str[256] = {'\0'};
#endif
        explicit operator const char*();
        explicit operator std::string();
    private:
        ID id;
    };


    class arrays {
    public:
        typedef int64_t size_type;
        typedef boost::recursive_wrapper<type> base_type;
        typedef std::vector<size_type> definition_type;

        arrays() {}

        explicit arrays(base_type &base) : subtype(base) {}

        explicit arrays(base_type &base, definition_type sizes) : subtype(base), size_arr(sizes) {}

        base_type &base();

        base_type const &base() const;

        void base(base_type const &base);

        definition_type &sizes();

        definition_type const &sizes() const;

        void sizes(definition_type const &sizes);

        size_type operator[](size_t pos);

        bool operator==(const arrays &other) const;

        bool operator!=(const arrays &other) const;

#ifdef TYPE_DEBUG
        char str[256] = {'\0'};
#endif
        explicit operator const char*();
        explicit operator std::string();
    private:
        definition_type size_arr;
        base_type subtype;
    };


    struct type {
        typedef boost::variant<basic_type, arrays, usertype, variant_type, void_type> type_variant;

        type() {}

        type(type const &t) = default;

        type(type &&t) = default;

        explicit type(basic_type t) : real_type(t) {}
        explicit type(arrays const &t) : real_type(t) {}
        explicit type(usertype const &t) : real_type(t) {}
        explicit type(variant_type const &t) : real_type(t) {}
        explicit type(void_type const &t) : real_type(t) {}
        explicit type(basic_type t, bool const_) : real_type(t), is_const(const_) {}
        explicit type(arrays const &t, bool const_) : real_type(t), is_const(const_) {}
        explicit type(usertype const &t, bool const_) : real_type(t), is_const(const_) {}
        explicit type(variant_type const &t, bool const_) : real_type(t), is_const(const_) {}
        explicit type(void_type const &t, bool const_) : real_type(t), is_const(const_) {}

        type &operator=(type const &t) = default;

        type &operator=(type &&t) = default;

        type_variants which() const;

        void setInner(basic_type const &t);

        void setInner(arrays const &t);

        void setInner(usertype const &t);

        void setInner(variant_type const &t);

        void setInner(void_type const &t);

        void setInner(type_variant const &t);

        type_variant &getInner();

        type_variant const &getInner() const;

        bool isReference() const;

        bool isConst() const;

        void setReference(bool ref);

        void setConst(bool const_);

        bool constFusion(type const &a, type const &b);

        template<typename T>
        inline T &get() { return boost::get<T>(real_type); }

        template<typename T>
        inline T const &get() const { return boost::get<T>(real_type); }

        inline bool operator==(const type &other) const {
            return other.real_type == this->real_type
                && other.is_const  == this->is_const
                && other.is_ref    == this->is_ref;
        }
        inline bool operator!=(const type &other) const {
            return !(*this == other);
        }
#ifdef TYPE_DEBUG
        char str[256] = {'\0'};
#endif
        explicit operator const char*();
        explicit operator std::string();
        const char* toString() { return (const char*)*this; }

    private:
        type_variant real_type = basic_type::sint_type;
        bool is_const = false;
        bool is_ref = false;
    };


}

#endif //CUSTOM_ID_TYPESYSTEM_HPP
