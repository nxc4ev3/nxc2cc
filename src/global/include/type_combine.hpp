/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Type combining logic (casting/type compatibility).
 */

#ifndef CUSTOM_ID_TYPE_COMBINE_H
#define CUSTOM_ID_TYPE_COMBINE_H

#include <global/include/typesystem.hpp>
#include <global/include/nxcexcept.hpp>
#include <global/include/symbols.hpp>
#include <global/include/macros.hpp>

namespace nxc {

    enum class type_cat {
        score_bool = 0,
        score_byte = 1,
        score_short = 2,
        score_long = 3,
        score_float = 4,
        score_invalid = -1
    };

    enum class sign_cat {
        sign_signed = 0,
        sign_unsigned = 1,
        sign_invalid = -1
    };
    enum class compat_score {
        incompatible = false,
        numcast,
        signcast,
        exact,
    };

    class type_combine {
    public:
        type_combine(symbol_table &tbl) : tbl(tbl) {}

        void combine(nxc::type const &a, nxc::type const &b, nxc::type &result);

        compat_score compare (nxc::type tAct, nxc::type tReq);

    protected:

        void unenumize(nxc::type &t);

        void combine_struct(nxc::type const &struct_, nxc::type const &other, nxc::type &result);

        void combine_array(nxc::type const &a, nxc::type const &b, nxc::type &result);

        void combine_basic(nxc::type const &a, nxc::type const &b, nxc::type &result);

        type_cat score(nxc::basic_type t);

        sign_cat sign(nxc::basic_type t);

        nxc::basic_type synthesis(type_cat basis, sign_cat sign);

        symbol_table &tbl;


        compat_score compare_basic(basic_type tAct, basic_type tReq);

        compat_score compare_arrays(arrays &tAct, arrays &tReq);
    };
}


#endif //CUSTOM_ID_TYPE_COMBINE_H
