/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Overloaded function chooser logic – compatibility score calculation.
 */

#ifndef CUSTOM_ID_FUNCTION_LOOKUP_HPP
#define CUSTOM_ID_FUNCTION_LOOKUP_HPP

#include <global/include/symbols.hpp>
#include <global/include/type_combine.hpp>

namespace nxc {

    class function_lookup {
    public:
        function_lookup(nxc::symbol_table &syms)
                : syms(syms), combiner(syms) {}

        function_lookup(nxc::symbol_table &syms, type_combine &combo)
                : syms(syms), combiner(combo) {}

        func_data *find(const std::string &name,
                        const std::vector<nxc::type> &args);

    protected:
        bool calcScore(const func_data &req,
                       const std::string &actName,
                       const std::vector<nxc::type> &actArgs,
                       int &score);

        func_data *getBest(const std::vector<std::pair<func_data *, int>> &scores,
                           const std::string &name);

    private:
        nxc::symbol_table &syms;
        nxc::type_combine combiner;
    };
}


#endif //CUSTOM_ID_FUNCTION_LOOKUP_HPP
