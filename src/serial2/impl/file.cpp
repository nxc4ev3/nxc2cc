/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * AST to C source serializer - translation-unit, function & task part.
 */


#include <parser2/include/ast.hpp>
#include <serial2/include/private/serializer.hpp>


void nxc::serializer::serialize(nxc::translation_unit &unit) {
    declareTasks(unit);

    for (extdeclaration &decl : unit.declarations) {
        switch (decl.type) {
            case extdeclaration_type::None:
                continue;
            case extdeclaration_type::Function: {
                efuncinfo &info = static_cast<efuncinfo &>(*decl.pointer);
                switch (info.getType(syms)) {
                    case FunctionType::Task:
                        serialize_task(info);
                        break;
                    case FunctionType::Function:
                        serialize_function(info);
                        break;
                }
                break;
            }
            case extdeclaration_type::Declaration: {
                edeclinfo &info = static_cast<edeclinfo &>(*decl.pointer);
                serialize_declaration(info, SCOPE_GLOBAL);
                break;
            }
        }
    }
}


void nxc::serializer::serialize_task(nxc::efuncinfo &info) {
    task_data &data = syms.get_task(info.id);
    const std::string &name = data.getName();
    const std::string &funcname = name_task_func(name);
    indent();
    if (info.code) {
        indent();
        out << "void *" << funcname << "_real(PRECEDES_ARGS)" << std::endl;
        serialize_stmt_compound(*info.code, SCOPE_GLOBAL, true);
        out << std::endl;

        indent();
        out << "void *" << funcname << "(void *arg) {" << std::endl;
        increaseIndent();

        indent(); out << name_task_spawned(name) << " = true;" << std::endl;
        indent(); out << "PRECEDES_INIT;" << std::endl;
        indent(); out << "PRECEDES_CALL(" << funcname << "_real);" << std::endl;
        indent(); out << "PRECEDES_EXEC;" << std::endl;
        indent(); out << "PRECEDES_FREE;" << std::endl;

        decreaseIndent();
        indent(); out << "}" << std::endl;
    } else {
        out << "void *" << funcname << "(void *arg);" << std::endl;
    }
}

void nxc::serializer::serialize_function(nxc::efuncinfo &info) {
    func_data &data = syms.get_func(info.id);
    bool safecall = (data.getFlags() & FunctionModifierMask::Safecall) != FunctionModifierMask::None;
    if (safecall) {
        serialize_function_safecall(info, data);
    } else {
        serialize_function_normal(info, data);
    }
}

void nxc::serializer::serialize_function_safecall(nxc::efuncinfo &info, nxc::func_data &data) {
    serialize_function_head_mutex(data);
    if (info.code) {
        indent();
        serialize_function_head("_real", data);
        out << std::endl;
        serialize_stmt_compound(*info.code, SCOPE_GLOBAL, true);
        out << std::endl;

        std::string mutexname = name_func_mutex(data);
        indent();
        serialize_function_head("", data);
        out << "{" << std::endl;
        increaseIndent();
        indent(); out << "__MUTEX_INIT(" << func_mangle(data) << ");" << std::endl;
        indent(); out << "pthread_mutex_lock(&" << mutexname << ");" << std::endl;
        indent(); out << func_mangle(data) << "_real(";

        auto it = data.getArguments().begin();
        auto end = data.getArguments().end();
        for (; it != end; ++it) {
            out << it->second;
            if (std::next(it) != end) {
                out << ", ";
            }
        }

        out << ");" << std::endl;
        indent(); out << "pthread_mutex_unlock(&" << mutexname << ");" << std::endl;
        decreaseIndent();
        indent(); out << "}" << std::endl;
    } else {
        indent();
        serialize_function_head("", data);
        out << ";" << std::endl;
    }
}

void nxc::serializer::serialize_function_normal(nxc::efuncinfo &info, nxc::func_data &data) {
    serialize_function_head("", data);
    if (info.code) {
        serialize_stmt_compound(*info.code, SCOPE_GLOBAL, true);
    } else {
        out << ";" << std::endl;
    }
}

void nxc::serializer::serialize_function_head_mutex(nxc::func_data &data) {
    if (!data.wasIntroduced()) {
        data.setIntroduced(true);
        indent();
        out << "bool " << name_func_mutex(data) << "_init = false;" << std::endl;
        indent();
        out << "pthread_mutex_t " << name_func_mutex(data) << ";" << std::endl;
    }
}



void nxc::serializer::serialize_function_head(const char *nameappend, nxc::func_data &data) {
    bool inline_ = (data.getFlags() & FunctionModifierMask::Inline) != FunctionModifierMask::None;
    indent();
    if (inline_)
        out << "inline ";

    if (data.getReturnType()) {
        nxc::type &t = data.getReturnType().get();
        serialize_type(t);
    } else {
        out << "void";
    }
    out << " ";
    out << func_mangle(data) << nameappend;
    out << "(";

    auto it = data.getArguments().begin();
    auto end = data.getArguments().end();
    for (; it != end; ++it) {
        serialize_type(it->first);
        out << " ";
        bool ref = it->first.isReference();

        // fixme handle references better way
        if (ref)
            out << "(*";
        out << it->second;
        if (ref)
            out << ")";

        if (std::next(it) != end) {
            out << ", ";
        }
    }
    out << ")";
}


void nxc::serializer::serialize_declaration(nxc::edeclinfo &info, ID parent) {
    std::vector<instance_info> &instances = *info.instances;
    indent();
    switch (info.modifier) {
        case DeclarationModifier::Typedef:
            out << "typedef ";
            break;
        case DeclarationModifier::Static:
            out << "static ";
            break;
        case DeclarationModifier::None:
            break;
    }
    serialize_type(*info.baseType);

    out << " ";

    auto it = instances.begin();
    auto end = instances.end();
    for (; it != end; ++it) {
        instance_info &instance = *it;
        if (syms.which(instance.id) == sym_class::TYPEDEF) {

            typedef_data &var = syms.get_typedef(instance.id);
            serialize_declaration_name(var.getType(), var.getName());

        } else if (syms.which(instance.id) == sym_class::VARIABLE){

            var_data &var = syms.get_var(instance.id);
            serialize_declaration_name(var.getType(), var.getName());
            serialize_declaration_init(instance, parent);
        }

        if (std::next(it) != end) {
            out << ",";
        }
    }
    out << ";" << std::endl;
}

void nxc::serializer::serialize_declaration_name(nxc::type &type, const std::string &name) {
    if (type.which() == type_variants::array) {
        nxc::arrays &arr = type.get<nxc::arrays>();
        std::string specifier = name;
        for (auto pass : arr.sizes()) {
            if (pass == SIZE_UNKNOWN) {
                specifier.insert(0, "(*");
                specifier.append(")");
            } else {
                specifier.append("[");
                specifier.append(std::to_string(pass));
                specifier.append("]");
            }
        }
        out << specifier;
    } else {
        out << name;
    }
}

void nxc::serializer::serialize_declaration_init(instance_info &instance, ID parent) {
    if (instance.value) {
        initializer &init = *instance.value;
        out << " = ";
        serialize_init(init, parent);
    }
}

void nxc::serializer::serialize_init(initializer &init, ID parent) {
    if (init.type == InitializerType::Array) {
        initializer_array &array = static_cast<initializer_array &>(*init.ptr);

        out << "{";
        auto it = array.fields.begin();
        auto end = array.fields.end();
        for (; it != end; ++it) {
            serialize_init(*it, parent);
            if (std::next(it) != end) {
                out << ",";
            }
        }
        out << "}";
    } else if (init.type == InitializerType::Expression) {
        initializer_expr &expr = static_cast<initializer_expr &>(*init.ptr);
        serialize_expr(*expr.expr, parent);

    } else {
        return;
    }
}

void nxc::serializer::declareTasks(nxc::translation_unit &unit) {
    for (extdeclaration &decl : unit.declarations) {
        if (decl.type != extdeclaration_type::Function)
            continue;
        efuncinfo &info = PtrCastRef<efuncinfo>(decl.pointer);
        if (info.getType(syms) != FunctionType::Task)
            continue;
        task_data &data = syms.get_task(info.id);
        indent();
        out << "pthread_t " << name_task_thread(data.getName()) << ";" << std::endl;
        indent();
        out << "void * " << name_task_func(data.getName()) << "(void *arg);" << std::endl;
        indent();
        out << "bool " << name_task_spawned(data.getName()) << " = false;" << std::endl;
        out << std::endl;
    }
}
