/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * AST to C source serializer - statement part.
 */


#include <serial2/include/private/serializer.hpp>

const std::string &getJumpTo(nxc::statement &base) {
    return static_cast<nxc::jump_to &>(*base.stmt).label;
}

const std::string &getThreadName(nxc::statement &base) {
    return static_cast<nxc::thread_name &>(*base.stmt).name;
}

const std::string &getThreadPriority_Name(nxc::statement &base) {
    return static_cast<nxc::thread_priority &>(*base.stmt).name;
}

nxc::ast_node &getThreadPriority_Expr(nxc::statement &base) {
    return *static_cast<nxc::thread_priority &>(*base.stmt).priority;
}

bool hasExpression(nxc::statement &base) {
    if (base.stmt == nullptr)
        return false;

    nxc::expression_statement &box = static_cast<nxc::expression_statement &>(*base.stmt);
    return (bool) box.expression;
}

nxc::ast_node &getExpression(nxc::statement &base) {
    return *static_cast<nxc::expression_statement &>(*base.stmt).expression;
}

nxc::statement &getDefaultStatement(nxc::statement &base) {
    return static_cast<nxc::label_default &>(*base.stmt).child;
}

nxc::ast_node &getCaseValue(nxc::statement &base) {
    return *static_cast<nxc::label_case &>(*base.stmt).value;
}

nxc::statement &getCaseStatement(nxc::statement &base) {
    return static_cast<nxc::label_case &>(*base.stmt).child;
}

const std::string &getLabelName(nxc::statement &base) {
    return static_cast<nxc::label_named &>(*base.stmt).name;
}

nxc::statement &getLabelStatement(nxc::statement &base) {
    return static_cast<nxc::label_named &>(*base.stmt).child;
}

void nxc::serializer::serialize_stmt(nxc::statement &stmt, nxc::ID parent) {
    switch (stmt.type) {
        case StatementType::None:
            break;
        case StatementType::Expression:
            indent();
            if (hasExpression(stmt))
                serialize_expr(getExpression(stmt), parent);
            out << ";";
            break;
        case StatementType::Compound:
            serialize_stmt_compound(stmt, parent);
            break;
        case StatementType::Jump_Continue:
            indent();
            out << "continue;";
            break;
        case StatementType::Jump_Break:
            indent();
            out << "break;";
            break;
        case StatementType::Jump_Goto:
            indent();
            out << "goto " << getJumpTo(stmt) << ";";
            break;
        case StatementType::Jump_Return:
            indent();
            out << "return";
            if (hasExpression(stmt)) {
                out << " ";
                serialize_expr(getExpression(stmt), parent);
            }
            out << ";";
            break;
        case StatementType::Thread_Start:
            indent();
            out << "__THREAD_START(" << getThreadName(stmt) << ");";
            break;
        case StatementType::Thread_Stop:
            indent();
            out << "__THREAD_STOP(" << getThreadName(stmt) << ");";
            break;
        case StatementType::Thread_Priority:
            indent();
            out << "__THREAD_PRIORITY(" << getThreadPriority_Name(stmt) << ", ";
            serialize_expr(getThreadPriority_Expr(stmt), parent);
            out << ");";
            break;
        case StatementType::Label_Identifier:
            indent();
            out << getLabelName(stmt) << ":" << std::endl;
            serialize_stmt(getLabelStatement(stmt), parent);
            break;
        case StatementType::Label_Case:
            decreaseIndent();
            indent();
            increaseIndent();
            out << "case ";
            serialize_expr(getCaseValue(stmt), parent);
            out << ":" << std::endl;
            serialize_stmt(getCaseStatement(stmt), parent);
            break;
        case StatementType::Label_Default:
            decreaseIndent();
            indent();
            increaseIndent();
            out << "default:" << std::endl;
            serialize_stmt(getDefaultStatement(stmt), parent);
            break;
        case StatementType::Select_IfElse:
            serialize_stmt_if(stmt, parent);
            break;
        case StatementType::Select_Switch:
            serialize_stmt_switch(stmt, parent);
            break;
        case StatementType::Iteration_DoWhile:
        case StatementType::Iteration_DoUntil:
            serialize_stmt_do(stmt, parent);
            break;
        case StatementType::Iteration_For:
            serialize_stmt_for(stmt, parent);
            break;
        case StatementType::Iteration_While:
        case StatementType::Iteration_Until:
            serialize_stmt_while(stmt, parent);
            break;
        case StatementType::Iteration_Repeat:
            serialize_stmt_repeat(stmt, parent);
            break;
    }
    out << std::endl;
}

void nxc::serializer::serialize_stmt_pseudoindent(nxc::statement &stmt, nxc::ID parent) {
    bool increase = stmt.type != StatementType::Compound;
    if (increase)
        increaseIndent();
    serialize_stmt(stmt, parent);
    if (increase)
        decreaseIndent();
}

void nxc::serializer::serialize_stmt_if(nxc::statement &stmt, nxc::ID parent) {
    ifelse_statement &info = static_cast<ifelse_statement &>(*stmt.stmt);
    indent();
    out << "if (";
    serialize_expr(*info.condition, parent);
    out << ")" << std::endl;
    serialize_stmt_pseudoindent(info.true_case, parent);
    if (info.false_case) {
        indent();
        out << "else" << std::endl;
        serialize_stmt_pseudoindent(*info.false_case, parent);
    }
}

void nxc::serializer::serialize_stmt_switch(nxc::statement &stmt, nxc::ID parent) {
    switch_statement &info = static_cast<switch_statement &>(*stmt.stmt);
    indent();
    out << "switch (";
    serialize_expr(*info.value, parent);
    out << ")" << std::endl;
    serialize_stmt_pseudoindent(info.body, parent);
}

void nxc::serializer::serialize_stmt_do(nxc::statement &stmt, nxc::ID parent) {
    loop_info &info = static_cast<loop_info &>(*stmt.stmt);
    bool negate = stmt.type == StatementType::Iteration_DoUntil;
    indent();
    out << "do" << std::endl;
    serialize_stmt_pseudoindent(info.body, parent);

    indent();
    out << "while (";
    if (negate)
        out << "!(";
    serialize_expr(*info.condition, parent);
    if (negate)
        out << ")";
    out << ");";
}

void nxc::serializer::serialize_stmt_for(nxc::statement &stmt, nxc::ID parent) {
    for_loop_info &info = static_cast<for_loop_info &>(*stmt.stmt);
    indent();
    out << "for (";
    if (info.initializer) {
        out << std::endl;
        increaseIndent();
        expression_declaration &init = static_cast<expression_declaration &>(*info.initializer);
        if (init.type == ExpressionDeclaration::Declaration) {
            serialize_declaration(*static_cast<in_decl &>(*init.ptr).declaration, parent);
        } else if (init.type == ExpressionDeclaration::Expression) {
            serialize_stmt(static_cast<in_stmt &>(*init.ptr).stmt, parent);
        }
        indent();
        decreaseIndent();
    } else {
        out << ";";
    }
    if (info.condition)
        serialize_expr(*info.condition, parent);
    out << ";";
    if (info.update)
        serialize_expr(*info.update, parent);
    out << ")" << std::endl;
    serialize_stmt_pseudoindent(info.body, parent);
}

void nxc::serializer::serialize_stmt_while(nxc::statement &stmt, nxc::ID parent) {
    loop_info &info = static_cast<loop_info &>(*stmt.stmt);
    bool negate = stmt.type == StatementType::Iteration_Until;

    indent();
    out << "while (";
    if (negate)
        out << "!(";
    serialize_expr(*info.condition, parent);
    if (negate)
        out << ")";
    out << ")";

    serialize_stmt_pseudoindent(info.body, parent);
}

void nxc::serializer::serialize_stmt_repeat(nxc::statement &stmt, nxc::ID parent) {
    loop_info &info = static_cast<loop_info &>(*stmt.stmt);

    static int repeat_counter = 0;
    int i = repeat_counter++;

    indent();
    out << "for (";
    out << "int __rep_" << i << " = 0,";
    out << "__cnt_" << i << " = (";
    serialize_expr(*info.condition, parent);
    out << ");";
    out << "__rep_" << i << "< __cnt_" << i << ";";
    out << "__rep_" << i << "++)" << std::endl;

    serialize_stmt_pseudoindent(info.body, parent);
}

void nxc::serializer::serialize_stmt_compound(nxc::statement &stmt, nxc::ID parent, bool braces) {
    compound_statement &container = PtrCastRef<compound_statement>(stmt.stmt);
    if (braces) {
        indent();
        out << "{" << std::endl;
        increaseIndent();
    }
    for (auto &line : container.statements) {

        if (line.type == ExpressionDeclaration::Declaration) {
            in_decl &declline = PtrCastRef<in_decl>(line.ptr);
            serialize_declaration(*declline.declaration, container.id);

        } else if (line.type == ExpressionDeclaration::Expression) {
            in_stmt &stmtline = PtrCastRef<in_stmt>(line.ptr);
            serialize_stmt(stmtline.stmt, container.id);
        }
    }
    if (braces) {
        decreaseIndent();
        indent();
        out << "}" << std::endl;
    }
}
