/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * AST to C source serializer - type part.
 */


#include <sstream>
#include <global/include/typesystem.hpp>
#include <global/include/symbols.hpp>
#include <global/include/function_lookup.hpp>
#include "serial2/include/private/serializer.hpp"

std::string nxc::serializer::name_task_func(std::string const &name) {
    return std::string("__task_").append(name);
}

std::string nxc::serializer::name_task_thread(std::string const &name) {
    return std::string("__thr_").append(name);
}

std::string nxc::serializer::name_task_spawned(std::string const &name) {
    return std::string("__spawned_").append(name);
}

std::string nxc::serializer::type_mangle(nxc::type &obj) {
    std::stringstream ss;
    if (obj.isConst())
        ss << "c";
    switch (obj.which()) {
        case type_variants::basic: {
            nxc::basic_type t = obj.get<nxc::basic_type>();
            switch (t) {
                case nxc::basic_type::bool_type:
                    ss << "bool";
                    break;
                case nxc::basic_type::uchar_type:
                case nxc::basic_type::byte_type:
                    ss << "ubyte";
                    break;
                case nxc::basic_type::schar_type:
                    ss << "sbyte";
                    break;
                case nxc::basic_type::sshort_type:
                case nxc::basic_type::sint_type:
                    ss << "sint";
                    break;
                case nxc::basic_type::ushort_type:
                case nxc::basic_type::uint_type:
                    ss << "uint";
                    break;
                case nxc::basic_type::slong_type:
                    ss << "slong";
                    break;
                case nxc::basic_type::ulong_type:
                    ss << "ulong";
                    break;
                case nxc::basic_type::float_type:
                    ss << "float";
                    break;
                case nxc::basic_type::mutex_type:
                    ss << "mutex";
                    break;
                case nxc::basic_type::string_type:
                    ss << "string";
                    break;
            }
            break;
        }
        case type_variants::array: {
            nxc::arrays &t = obj.get<nxc::arrays>();
            if (t.sizes().size() > 0) {
                ss << "arr" << t.sizes().size();
            }
            ss << type_mangle(t.base().get());
            break;
        }
        case type_variants::user: {
            nxc::usertype &t = obj.get<nxc::usertype>();
            ss << "type" << t.getID();
            break;
        }
        case type_variants::variant_t:
            ss << "variant";
            break;
        case type_variants::void_t:
            ss << "void";
            break;
    }
    return ss.str();
}

std::string nxc::serializer::func_mangle(nxc::func_data const &data) {
    std::stringstream ss;
    ss << data.getName() << "_";
    for (nxc::func_data::arg_type const &arg : data.getArguments()) {
        ss << "_" << type_mangle(const_cast<nxc::type &>(arg.first));
    }
    return ss.str();
}


std::string nxc::serializer::name_func_mutex(nxc::func_data const &data) {
    return std::string("__mutex_").append(func_mangle(data));
}


void nxc::serializer::serialize_call(const nxc::ast_branch &call, nxc::ID parent) {
    const std::string &name = getFunctionName(call);
    std::vector<type> types = getFunctionArgTypes(call);

    if (natives.serialize(call, types, syms, parent, *this, out))
        return;

    function_lookup lookup(syms);
    func_data *ptr = lookup.find(name, types);
    if (ptr == nullptr) {
        std::string msg("Call to unknown function: ");
        msg.append(name);
        throw serialize_error(-1, -1, "", msg.c_str());
    }

    out << func_mangle(*ptr);
    out << "(";

    auto dT     = ptr->getArguments().begin();
    auto dT_end = ptr->getArguments().end();
    auto sA     = call.children.begin() + 1;
    auto sA_end = call.children.end();
    for (; dT != dT_end && sA != sA_end; ++dT, ++sA) {
        bool ref = dT->first.isReference();
        if (ref)
            out << "&(";
        serialize_expr(**sA, parent);
        if (ref)
            out << ")";
        if (std::next(sA) != sA_end)
            out << ",";
    }
    out << ")";
    // todo handle references better way
    // fixme array pointers - reference handling
}





/////////////////
// TYPE PARSER //
/////////////////


void nxc::serializer::serialize_type(nxc::type &t) {
    if (t.isConst())
        out << "const ";

    switch (t.which()) {
        case type_variants::basic: {
            nxc::basic_type basic = t.get<basic_type>();
            switch (basic) {
                case basic_type::bool_type:
                    out << "bool";
                    break;
                case basic_type::uchar_type:
                case basic_type::byte_type:
                    out << "uint8_t";
                    break;
                case basic_type::schar_type:
                    out << "int8_t";
                    break;
                case basic_type::sshort_type:
                case basic_type::sint_type:
                    out << "int16_t";
                    break;
                case basic_type::ushort_type:
                case basic_type::uint_type:
                    out << "uint16_t";
                    break;
                case basic_type::slong_type:
                    out << "int32_t";
                    break;
                case basic_type::ulong_type:
                    out << "uint32_t";
                    break;
                case basic_type::float_type:
                    out << "float";
                    break;
                case basic_type::mutex_type:
                    out << "pthread_mutex_t";
                    break;
                case basic_type::string_type:
                    out << "char *";
                    break;
            }
            break;
        }
        case type_variants::array: {
            nxc::arrays arr = t.get<nxc::arrays>();
            serialize_type(arr.base().get());
            for (int i = 0; i < arr.sizes().size(); i++) {
                out << "*";
            }
            break;
        }
        case type_variants::user: {
            nxc::usertype &user = t.get<nxc::usertype>();
            nxc::usertype_kind kind = whichtype(user, syms);
            switch (kind) {
                case usertype_kind::struct_kind: {
                    out << "struct ";
                    struct_data &info = syms.get_struct(user);
                    out << info.getName();
                    if (!info.wasPrinted()) {
                        info.setPrinted(true);
                        out << " {" << std::endl;
                        for (nxc::struct_data::member_type var_id : info.members()) {
                            var_data &memb = syms.get_var(var_id);
                            serialize_type(memb.getType());
                            out << " ";
                            out << memb.getName();
                            out << ";" << std::endl;
                        }
                        out << "}";
                    }
                    break;
                }
                case usertype_kind::enum_kind: {
                    out << "enum ";
                    enum_data &info = syms.get_enum(user);
                    out << info.getName();
                    if (!info.wasPrinted()) {
                        info.setPrinted(true);
                        out << " {" << std::endl;
                        for (nxc::enum_data::member_type &memb : info.getMembers()) {
                            out << memb.first << " = " << memb.second << "," << std::endl;
                        }
                        out << "}";
                    }
                    break;
                }
                case usertype_kind::typedef_kind: {
                    typedef_data &info = syms.get_typedef(user);
                    out << info.getName() << " ";
                    break;
                }
            }
            break;
        }
        case type_variants::variant_t:
            out << "variant";
            break;
        case type_variants::void_t:
            out << "void";
            break;
    }
}


void nxc::serializer::serialize_arraysize(nxc::arrays::definition_type const &info) {
    for (nxc::arrays::size_type arr : info) {
        out << "[";
        if (arr != SIZE_UNKNOWN) {
            out << arr;
        }
        out << "]";
    }
}
