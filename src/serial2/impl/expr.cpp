/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * AST to C source serializer - expression part.
 */


#include <serial2/include/private/serializer.hpp>

void nxc::serializer::serialize_expr(nxc::ast_node &expr, nxc::ID parent) {
    switch (expr.nodeType) {
        case ast_node_type::Multinode:
            serialize_expr(static_cast<nxc::ast_branch &>(expr), parent);
            break;
        case ast_node_type::Value:
            serialize_expr(static_cast<nxc::ast_value &>(expr), parent);
            break;
        case ast_node_type::Typename:
            serialize_expr(static_cast<nxc::ast_typename &>(expr), parent);
            break;
    }
}

void nxc::serializer::serialize_expr(nxc::ast_branch &branch, nxc::ID parent) {
    if(branch.children.size() == 0)
        return;
    switch (branch.op) {
        case ast_op_type::Passthrough:
            serialize_expr(branch.firstChild(), parent);
            break;
        case ast_op_type::Comma:
            serialize_list(branch, parent);
            break;
        case ast_op_type::Assign_Only:
        case ast_op_type::Assign_Multiply:
        case ast_op_type::Assign_Divide:
        case ast_op_type::Assign_Modulo:
        case ast_op_type::Assign_Add:
        case ast_op_type::Assign_Subtract:
        case ast_op_type::Assign_Leftshift:
        case ast_op_type::Assign_Rightshift:
        case ast_op_type::Assign_BitAnd:
        case ast_op_type::Assign_BitXor:
        case ast_op_type::Assign_BitOr:
        case ast_op_type::LogicOr:
        case ast_op_type::LogicAnd:
        case ast_op_type::BitOr:
        case ast_op_type::BitXor:
        case ast_op_type::BitAnd:
        case ast_op_type::Equality_Equal:
        case ast_op_type::Equality_Notequal:
        case ast_op_type::Relation_LessEqual:
        case ast_op_type::Relation_Less:
        case ast_op_type::Relation_Greater:
        case ast_op_type::Relation_GreaterEqual:
        case ast_op_type::Shift_Left:
        case ast_op_type::Shift_Right:
        case ast_op_type::Addition_Add:
        case ast_op_type::Addition_Subtract:
        case ast_op_type::Multiplication_Multiply:
        case ast_op_type::Multiplication_Divide:
        case ast_op_type::Multiplication_Modulo:
        case ast_op_type::Member:
            serialize_two(branch, parent);
            break;
        case ast_op_type::Cast:
            serialize_cast(branch, parent);
            break;
        case ast_op_type::Sizeof_Type:
        case ast_op_type::Sizeof_Expr:
            serialize_sizeof(branch, parent);
            break;
        case ast_op_type::Unary_Plus:
        case ast_op_type::Unary_Minus:
        case ast_op_type::Unary_Complement:
        case ast_op_type::Unary_Negate:
        case ast_op_type::Increment_Pre:
        case ast_op_type::Decrement_Pre:
            serialize_pre(branch, parent);
            break;
        case ast_op_type::Increment_Post:
        case ast_op_type::Decrement_Post:
            serialize_post(branch, parent);
            break;
        case ast_op_type::Index:
            serialize_index(branch, parent);
            break;
        case ast_op_type::Call:
            serialize_call(branch, parent);
            break;
        case ast_op_type::Assign_Abs:
            serialize_assignment_fn(branch, parent, "abs");
            break;
        case ast_op_type::Assign_Sign:
            serialize_assignment_fn(branch, parent, "signum");
            break;
        case ast_op_type::Ternary:
            serialize_ternary(branch, parent);
            break;
    }
}


void nxc::serializer::serialize_expr(nxc::ast_value &expr, nxc::ID parent) {
    // todo come up with something better
    if (expr.annotation != ID_UNKNOWN){
        if (syms.which(expr.annotation) == sym_class::VARIABLE) {
            var_data &var = syms.get_var(expr.annotation);
            if (var.getType().isReference()){
                out << "(*" << expr.value << ")";
                return;
            }
        }
    }
    out << expr.value;
}

void nxc::serializer::serialize_expr(nxc::ast_typename &expr, nxc::ID parent) {
    serialize_type(*expr.name);
}

void nxc::serializer::serialize_two(nxc::ast_branch &list,
                                    nxc::ID parent) {
    ast_op_priority this_prio = getPriority(list.op);
    serialize_prio(list.childAt(0), parent, this_prio);
    out << op_char(list.op);
    serialize_prio(list.childAt(1), parent, this_prio);
}

void nxc::serializer::serialize_list(nxc::ast_branch &list, nxc::ID parent) {
    std::string sep = op_char(list.op);
    ast_op_priority prio = getPriority(list.op);

    auto it = list.children.begin();
    auto end = list.children.end();
    for (; it != end; ++it) {
        serialize_prio(**it, parent, prio);
        if (std::next(it) != end)
            out << sep;
    }
}


void nxc::serializer::serialize_cast(nxc::ast_branch &list, nxc::ID parent) {
    out << "(";
    serialize_expr(list.childAt(0), parent);
    out << ")";
    serialize_prio(list.childAt(1), parent, ast_op_priority::Prefixes);
}

void nxc::serializer::serialize_sizeof(nxc::ast_branch &list, nxc::ID parent) {
    out << "sizeof (";
    serialize_expr(list.childAt(0), parent);
    out << ")";
}

void nxc::serializer::serialize_pre(nxc::ast_branch &list, nxc::ID parent) {
    out << op_char(list.op);
    serialize_prio(list.childAt(0), parent, ast_op_priority::Prefixes);
}

void nxc::serializer::serialize_post(nxc::ast_branch &list, nxc::ID parent) {
    serialize_prio(list.childAt(0), parent, ast_op_priority::Postfixes);
    out << op_char(list.op);
}

void nxc::serializer::serialize_index(nxc::ast_branch &list, nxc::ID parent) {
    serialize_prio(list.childAt(0), parent, ast_op_priority::Postfixes);
    out << "[";
    serialize_expr(list.childAt(1), parent);
    out << "]";
}

void nxc::serializer::serialize_assignment_fn(nxc::ast_branch &list, nxc::ID parent, std::string fn) {
    // todo type discrimination
    out << "((";
    serialize_expr(list.childAt(0), parent);
    out << ") = " << fn << " (";
    serialize_expr(list.childAt(1), parent);
    out << "))";
}

void nxc::serializer::serialize_ternary(nxc::ast_branch &list, nxc::ID parent) {
    serialize_expr(list.childAt(0), parent);
    out << " ? ";
    serialize_expr(list.childAt(1), parent);
    out << " : ";
    serialize_expr(list.childAt(2), parent);
}

std::string nxc::serializer::op_char(nxc::ast_op_type op) {
    switch (op) {
        case ast_op_type::Comma:
            return ", ";
        case ast_op_type::Assign_Only:
            return " = ";
        case ast_op_type::Assign_Multiply:
            return " *= ";
        case ast_op_type::Assign_Divide:
            return " /= ";
        case ast_op_type::Assign_Modulo:
            return " %= ";
        case ast_op_type::Assign_Add:
            return " += ";
        case ast_op_type::Assign_Subtract:
            return " -= ";
        case ast_op_type::Assign_Leftshift:
            return " <<= ";
        case ast_op_type::Assign_Rightshift:
            return " >>= ";
        case ast_op_type::Assign_BitAnd:
            return " &= ";
        case ast_op_type::Assign_BitXor:
            return " ^= ";
        case ast_op_type::Assign_BitOr:
            return " |= ";
        case ast_op_type::LogicOr:
            return " || ";
        case ast_op_type::LogicAnd:
            return " && ";
        case ast_op_type::BitOr:
            return " | ";
        case ast_op_type::BitXor:
            return " ^ ";
        case ast_op_type::BitAnd:
            return " && ";
        case ast_op_type::Equality_Equal:
            return " == ";
        case ast_op_type::Equality_Notequal:
            return " != ";
        case ast_op_type::Relation_LessEqual:
            return " <= ";
        case ast_op_type::Relation_Less:
            return " < ";
        case ast_op_type::Relation_Greater:
            return " > ";
        case ast_op_type::Relation_GreaterEqual:
            return " >= ";
        case ast_op_type::Shift_Left:
            return " << ";
        case ast_op_type::Shift_Right:
            return " >> ";
        case ast_op_type::Addition_Add:
            return " + ";
        case ast_op_type::Addition_Subtract:
            return " - ";
        case ast_op_type::Multiplication_Multiply:
            return " * ";
        case ast_op_type::Multiplication_Divide:
            return " / ";
        case ast_op_type::Multiplication_Modulo:
            return " % ";
        case ast_op_type::Sizeof_Type:
        case ast_op_type::Sizeof_Expr:
            return " sizeof ";
        case ast_op_type::Unary_Plus:
            return " +";
        case ast_op_type::Unary_Minus:
            return " -";
        case ast_op_type::Unary_Complement:
            return " ~";
        case ast_op_type::Unary_Negate:
            return " !";
        case ast_op_type::Increment_Pre:
        case ast_op_type::Increment_Post:
            return " ++";
        case ast_op_type::Decrement_Pre:
        case ast_op_type::Decrement_Post:
            return "-- ";
        case ast_op_type::Member:
            return ".";
        case ast_op_type::Index:
        case ast_op_type::Cast:
        case ast_op_type::Call:
        case ast_op_type::Ternary:
        case ast_op_type::Passthrough:
        case ast_op_type::Assign_Abs:
        case ast_op_type::Assign_Sign:
            return "<none>";
    }
}

void nxc::serializer::serialize_prio(nxc::ast_node &node, nxc::ID parent, nxc::ast_op_priority upper) {
    if (node.nodeType == ast_node_type::Value || node.nodeType == ast_node_type::Typename) {
        serialize_expr(node, parent);
        return;
    }
    ast_branch &branch = static_cast<ast_branch &>(node);
    ast_op_priority this_prio = getPriority(branch.op);

    bool parens = this_prio >= upper;

    if (parens)
        out << "(";
    serialize_expr(branch, parent);
    if (parens)
        out << ")";
}



