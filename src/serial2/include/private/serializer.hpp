/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * AST to C source serializer - private shared header.
 */


#ifndef CUSTOM_ID_SERIALIZER_PRIVATE_HPP
#define CUSTOM_ID_SERIALIZER_PRIVATE_HPP

#include <ostream>
#include <parser2/include/ast.hpp>
#include <global/include/symbols.hpp>
#include <global/include/function_lookup.hpp>
#include <global/include/type_combine.hpp>
#include <parser2/include/private/native_iface.hpp>

namespace nxc {
    class serializer {
    public:
        serializer(std::ostream &out, translation_unit &parsed, symbol_table &syms);

        void start();

        void setIndent(const std::string &level);
        const std::string &getIndent();

        void serialize(translation_unit &unit);

        void serialize_task(efuncinfo &info);
        void serialize_function(efuncinfo &info);
        void serialize_function_safecall(efuncinfo &efuncinfo, func_data &data);
        void serialize_function_normal(efuncinfo &efuncinfo, func_data &data);
        void serialize_function_head_mutex(func_data &data);
        void serialize_function_head(const char *nameappend, func_data &data);
        void serialize_declaration(edeclinfo &info, ID parent);
        void serialize_declaration_name(type &type, const std::string &name);
        void serialize_declaration_init(instance_info &instance, ID parent);

        void declareTasks(translation_unit &unit);
        void serialize_init(initializer &init, ID parent);

        void serialize_stmt_pseudoindent(statement &stmt, ID parent);
        void serialize_stmt(statement &stmt, ID parent);
        void serialize_stmt_compound(statement &stmt, ID parent, bool braces = true);
        void serialize_stmt_if(statement &stmt, ID parent);
        void serialize_stmt_switch(statement &stmt, ID parent);
        void serialize_stmt_do(statement &stmt, ID parent);
        void serialize_stmt_for(statement &stmt, ID parent);
        void serialize_stmt_while(statement &stmt, ID parent);
        void serialize_stmt_repeat(statement &stmt, ID parent);

        void serialize_expr(ast_node &expr,          ID parent);
        void serialize_expr(ast_value &expr,         ID parent);
        void serialize_expr(ast_typename &expr,      ID parent);
        void serialize_expr(ast_branch &expr,        ID parent);
        void serialize_prio(nxc::ast_node &node,     ID parent, ast_op_priority upper);
        void serialize_list(nxc::ast_branch &list,   ID parent);
        void serialize_two(nxc::ast_branch &list,    ID parent);
        void serialize_cast(nxc::ast_branch &list,   ID parent);
        void serialize_sizeof(nxc::ast_branch &list, ID parent);
        void serialize_pre(nxc::ast_branch &list,    ID parent);
        void serialize_post(nxc::ast_branch &list,   ID parent);

        void serialize_index(nxc::ast_branch &list,  ID parent);
        void serialize_assignment_fn(nxc::ast_branch &list, ID parent, std::string fn);
        void serialize_ternary(nxc::ast_branch &list, ID parent);

        void serialize_call(const nxc::ast_branch &call, ID parent);
        void serialize_type(nxc::type &type);
        void serialize_arraysize(nxc::arrays::definition_type const &info);

        static std::string name_task_func(std::string const &name);
        static std::string name_task_thread(std::string const &name);
        static std::string name_task_spawned(std::string const &name);
        static std::string name_func_mutex(nxc::func_data const &data);
        static std::string type_mangle(nxc::type &obj);
        static std::string func_mangle(nxc::func_data const &data);

        static std::string op_char(ast_op_type op);

        void indent();
        void increaseIndent();
        void decreaseIndent();
    private:
        std::ostream &out;
        translation_unit &mainfile;
        symbol_table &syms;
        function_lookup flookup;
        type_combine combiner;
        native_interface natives;

        int indentLevel = 0;
        std::string indentStr;
    };
}

#endif //CUSTOM_ID_SERIALIZER_PRIVATE_HPP
