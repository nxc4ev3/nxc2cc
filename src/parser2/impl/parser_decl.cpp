/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * NXC declaration parser.
 */


#include <parser2/include/private/parser.hpp>


bool nxc::parser::parseDeclaration(nxc::translation_unit &unit) {
    auto decl = PtrCreate<edeclinfo>();
    if (parseDeclaration(*decl)) {
        extdeclaration ref;
        ref.type = extdeclaration_type::Declaration;
        PtrAssignCast(ref.pointer, decl);
        unit.declarations.push_back(std::move(ref));
        return true;
    } else {
        return false;
    }
}

bool nxc::parser::parseDeclaration(nxc::edeclinfo &ref) {
    switch (currentID()) {
        case T2_TYPEDEF:
            ref.modifier = DeclarationModifier::Typedef;
            advance();
            break;
        case T2_STATIC:
            ref.modifier = DeclarationModifier::Static;
            advance();
            break;
        default:
            ref.modifier = DeclarationModifier::None;
            break;
    }

    const_rawtype baseType;
    list_of<initializer_declaration> instances;

    if (!parseConstRawtype(baseType)) {
        return false;
    } else if (!parseDeclarationInitializerList(instances)) {
        return false;
    }
    sym.registerDeclaration(ref, baseType, instances);
    eatToken(T2_SEMICOLON, "semicolon after declaration expected");
    return true;
}

bool nxc::parser::parseConstRawtype(const_rawtype &ref) {
    ref.is_const = tokenIs(T2_CONST);
    if (ref.is_const) {
        advance();
    }
    return parseRawtype(ref.baseType);
}

bool nxc::parser::parseRawtype(rawtype &ref) {
    ref.location = currentPosition();
    bool unsigned_ = tokenIs(T2_UNSIGNED);
    if (unsigned_)
        advance();
    switch (currentID()) {

        case T2_CHAR:
            ref.type = RawType::char_n;
            goto unsigned_set;

        case T2_SHORT:
            ref.type = RawType::short_n;
            goto unsigned_set;

        case T2_INT:
            ref.type = RawType::int_n;
            goto unsigned_set;

        case T2_LONG:
            ref.type = RawType::long_n;
            goto unsigned_set;

        case T2_FLOAT:
            ref.type = RawType::float_f;
            goto advance;

        case T2_BOOL:
            ref.type = RawType::bool_n;
            goto advance;

        case T2_BYTE:
            ref.type = RawType::byte_n;
            goto advance;

        case T2_STRING:
            ref.type = RawType::string_s;
            goto advance;

        case T2_MUTEX:
            ref.type = RawType::mutex_m;
            goto advance;

        case T2_STRUCT: {
            ref.type = RawType::struct_u;
            auto detail = PtrCreate<struct_info>();
            if (parseStruct(*detail)) {
                PtrAssignCast(ref.details, detail);
                return true;
            } else return false;
        }

        case T2_ENUM: {
            ref.type = RawType::enum_u;
            auto detail = PtrCreate<enum_info>();
            if (parseEnum(*detail)) {
                PtrAssignCast(ref.details, detail);
                return true;
            } else return false;
        }

        case T2_IDENTIFIER: {
            ref.type = RawType::unknown_u;
            ref.details = PtrCreateCast<unknown_name, rawtype_detail>(currentValue());
            goto advance;
        }

        default:
            return false;
    }

    unsigned_set:
    {
        auto detail = PtrCreate<is_signed>();
        detail->really = !unsigned_;
        PtrAssignCast(ref.details, detail);
    };

    advance:
    {
        advance();
        return true;
    };
}

bool nxc::parser::parseTypeName(type_name &ref) {
    if (!parseConstRawtype(ref.type))
        return false;
    parseArrays(ref.arrays);
    return true;
}

bool nxc::parser::parseStruct(nxc::struct_info &ref) {
    if (!tokenIs(T2_STRUCT))
        return false;
    ref.location = currentPosition();
    advance();
    if (tokenIs(T2_IDENTIFIER) && currentValue() == "__native") {
        ref.native = true;
        advance();
    }
    if (tokenIs(T2_IDENTIFIER)) {
        ref.name = PtrCreate<std::string>(currentValue());
        advance();
    } else {
        ref.name = nullptr;
    }
    if (!tokenIs(T2_LEFTBRACE)) {
        if (ref.name == nullptr) {
            throwMalformedDeclaration("anonymous struct without definition isn't allowed");
        } else {
            ref.members = nullptr;
            return true;
        }
    }
    advance();
    auto members = PtrCreate<std::vector<struct_declaration >>();
    parseStruct_Members(*members);
    PtrAssign(ref.members, members);
    return true;
}

void nxc::parser::parseStruct_Members(std::vector<nxc::struct_declaration> &members) {
    if (tokenIs(T2_RIGHTBRACE))
        return;
    do {
        nxc::struct_declaration decl;
        parseStruct_Member(decl);
        members.push_back(std::move(decl));
    } while (!tokenIs(T2_RIGHTBRACE));
    advance();
}

void nxc::parser::parseStruct_Member(nxc::struct_declaration &member) {
    if (!parseConstRawtype(member.type))
        throwMalformedDeclaration("cannot parse struct member type");

    if (!parseDeclaratorList(member.instances))
        throwMalformedDeclaration("cannot parse struct member declarator list");

    eatToken(T2_SEMICOLON, "semicolon after struct member expected");
}

bool nxc::parser::parseEnum(nxc::enum_info &ref) {
    if (!tokenIs(T2_ENUM))
        return false;
    advance();
    if (tokenIs(T2_IDENTIFIER) && currentValue() == "__native") {
        ref.native = true;
        advance();
    }
    if (tokenIs(T2_IDENTIFIER)) {
        ref.name = PtrCreate<std::string>(currentValue());
        advance();
    } else {
        ref.name = nullptr;
    }
    if (!tokenIs(T2_LEFTBRACE)) {
        if (ref.name == nullptr) {
            throwMalformedDeclaration("anonymous enum without definition isn't allowed");
        } else {
            ref.members = nullptr;
            return true;
        }
    }
    advance();
    ref.members = PtrCreate<std::vector<enumerator >>();
    parseEnumerators(*ref.members);
    return true;
}

void nxc::parser::parseEnumerators(std::vector<enumerator> &ref) {
    if (tokenIs(T2_RIGHTBRACE))
        return;
    while (!tokenIs(T2_RIGHTBRACE)) {
        nxc::enumerator decl;
        parseEnumerator(decl);
        ref.push_back(std::move(decl));
        if (tokenIs(T2_RIGHTBRACE))
            break;
        else
            eatToken(T2_COMMA, "comma between enum constants expected");
    }
    advance();
}

void nxc::parser::parseEnumerator(enumerator &ref) {
    if (!tokenIs(T2_IDENTIFIER))
        throwUnexpectedToken("enum name expected", T2_IDENTIFIER);
    ref.location = currentPosition();
    ref.name = currentValue();
    advance();

    if (!tokenIs(T2_ASSIGN)) {
        ref.value = nullptr;
        return;
    }
    advance();

    ref.value = PtrCreate<ast_branch>();
    if (!parseConstantExpr(*ref.value))
        throwParseError("enum constant value parsing failed");
}

bool nxc::parser::parseDeclaratorList(std::vector<declarator> &ref) {
    declarator decl;
    if (!parseDeclarator(decl)) {
        return false;
    }
    ref.emplace_back(std::move(decl));
    while (tokenIs(T2_COMMA)) {
        advance();
        if (parseDeclarator(decl)) {
            ref.push_back(std::move(decl));
        } else break;
    }
    return true;
}

bool nxc::parser::parseDeclarator(declarator &decl) {
    if (!tokenIs(T2_IDENTIFIER))
        return false;
    decl.name = currentValue();
    decl.location = currentPosition();
    advance();
    parseArrays(decl.arrays);
    return true;
}

void nxc::parser::parseArrays(nxc::list_of<nxc::declarator_array> &arrays) {
    while (tokenIs(T2_LEFTBRACKET)) {
        nxc::declarator_array pass;
        pass.location = currentPosition();
        advance();
        if (tokenIs(T2_RIGHTBRACKET)) {
            pass.size = nullptr;
        } else {
            pass.size = PtrCreate<ast_branch>();
            if (!parseConstantExpr(*pass.size)) {
                throwParseError("array size parsing failed");
            }
        }
        eatToken(T2_RIGHTBRACKET, "array size ending bracket expected");
        arrays.push_back(std::move(pass));
    }
}

bool nxc::parser::parseDeclarationInitializerList(std::vector<initializer_declaration> &ref) {
    initializer_declaration init;
    if (tokenIs(T2_SEMICOLON)) {
        return true;
    } else if (!parseDeclarationInitializer(init)) {
        return false;
    }
    ref.push_back(std::move(init));
    while (tokenIs(T2_COMMA)) {
        advance();
        if (parseDeclarationInitializer(init)) {
            ref.push_back(std::move(init));
        } else break;
    }
    return true;
}

bool nxc::parser::parseDeclarationInitializer(initializer_declaration &init) {
    if (!parseDeclarator(init.decl))
        return false;
    if (!tokenIs(T2_ASSIGN)) {
        init.value = nullptr;
        return true;
    }
    advance();
    init.value = PtrCreate<initializer>();
    if (!parseInitializer(*init.value))
        throwParseError("declartion initializer parse failed");
    return true;
}

bool nxc::parser::parseInitializer(nxc::initializer &ref) {
    if (tokenIs(T2_LEFTBRACE)) {
        advance();
        auto list = PtrCreate<initializer_array>();
        while (!tokenIs(T2_RIGHTBRACE)) {
            nxc::initializer sub;
            if (!parseInitializer(sub))
                throwParseError("array sub-initializer parse failed");
            list->fields.push_back(std::move(sub));
            if (tokenIs(T2_RIGHTBRACE))
                break;
            eatToken(T2_COMMA, "comma between declarators expected");
        }
        advance();
        ref.type = InitializerType::Array;
        PtrAssignCast(ref.ptr, list);
        return true;
    } else {
        auto expr = PtrCreate<initializer_expr>();
        expr->location = currentPosition();
        expr->expr = PtrCreate<ast_branch>();
        if (parseAssignmentExpr(*expr->expr)) {
            ref.type = InitializerType::Expression;
            PtrAssignCast(ref.ptr, expr);
            return true;
        } else throwParseError("expression initializer parse failed");
    }
    return false;
}
