/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Builtin filtering implementation.
 */


#include <parser2/include/private/natives.hpp>
#include "parser2/include/private/native_iface.hpp"

namespace nxc {
    class native_builtins_impl {
    public:
        native_builtins_impl();

        const std::vector<std::shared_ptr<native_implementor>> &get() const {
            return m_builtins;
        }

    private:
        std::vector<std::shared_ptr<native_implementor>> m_builtins;
    };

    class native_builtins {
    public:
        const std::vector<std::shared_ptr<native_implementor>> &get() const {
            return st_list.get();
        }

    private:
        static native_builtins_impl st_list;
    };
}
nxc::native_builtins_impl nxc::native_builtins::st_list;

void nxc::native_interface::addNative(std::shared_ptr<nxc::native_implementor> implementor) {
    m_handlers.push_back(std::move(implementor));
}

nxc::native_interface::native_interface()
        : m_handlers(native_builtins{}.get()) {}

nxc::native_interface::native_interface(std::vector<std::shared_ptr<nxc::native_implementor>> last) {
    const auto &builtins = native_builtins{}.get();
    m_handlers.reserve(builtins.size() + last.size());
    m_handlers.insert(m_handlers.end(), builtins.begin(), builtins.end());
    m_handlers.insert(m_handlers.end(), last.begin(), last.end());
}

nxc::native_interface::native_interface(std::vector<std::shared_ptr<nxc::native_implementor>> first,
                                        std::vector<std::shared_ptr<nxc::native_implementor>> last) {
    const auto &builtins = native_builtins{}.get();
    m_handlers.reserve(builtins.size() + first.size() + last.size());
    m_handlers.insert(m_handlers.end(), first.begin(), first.end());
    m_handlers.insert(m_handlers.end(), builtins.begin(), builtins.end());
    m_handlers.insert(m_handlers.end(), last.begin(), last.end());
}


bool nxc::native_interface::parse(nxc::ast_branch &call,
                                  const std::vector<nxc::type> &resolved,
                                  nxc::symbol_table &syms) {
    for (auto &handler : m_handlers) {
        if (handler->parseCall(call, resolved, syms))
            return true;
    }
    return false;
}

bool nxc::native_interface::serialize(const nxc::ast_branch &call,
                                      const std::vector<nxc::type> &resolved,
                                      const nxc::symbol_table &syms,
                                      ID parent,
                                      serializer &serializer,
                                      std::ostream &out) {
    for (auto &handler : m_handlers) {
        if (handler->serializeCall(call, resolved, syms, parent, serializer, out))
            return true;
    }
    return false;
}

template<typename Base, typename Derived, typename ...Args>
std::shared_ptr<nxc::native_implementor> make_shared(Args && ...args) {
    return std::shared_ptr<Base>(static_cast<Base*>(new Derived(std::forward<Args>(args)...)));
};



nxc::native_builtins_impl::native_builtins_impl() {
    m_builtins.push_back(make_shared<native_implementor, array_stuff>());
    m_builtins.push_back(make_shared<native_implementor, precedes>());
    m_builtins.push_back(make_shared<native_implementor, tasks>());
    m_builtins.push_back(make_shared<native_implementor, cmath>());
}
