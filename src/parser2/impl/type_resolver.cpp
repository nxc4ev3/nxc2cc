/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * NXC type registrator.
 *  - parses raw type AST into common NXC types
 */


#include <parser2/include/type_resolver.hpp>
#include <eval/include/evaluator.hpp>


void nxc::type_resolver::parse_type(const type_name &spec, nxc::type &result) {
    parse_type(spec.type, result);
    arrayize(result, spec.arrays, result);
}

void nxc::type_resolver::parse_type(const const_rawtype &spec, nxc::type &result) {
    parse_type(spec.baseType, result);
    result.setConst(spec.is_const);
}

bool nxc::type_resolver::is_signed(const rawtype &spec) {
    return is_signed(*spec.details);
}

bool nxc::type_resolver::is_signed(const rawtype_detail &spec) {
    return static_cast<const nxc::is_signed &>(spec).really;
}

// parse type specifier
void nxc::type_resolver::parse_type(const rawtype &spec, nxc::type &result) {
    switch (spec.type) {
        case RawType::bool_n:
            result.setInner(nxc::basic_type::bool_type);
            break;
        case RawType::char_n:
            result.setInner(is_signed(spec) ? nxc::basic_type::schar_type : nxc::basic_type::uchar_type);
            break;
        case RawType::byte_n:
            result.setInner(nxc::basic_type::uchar_type);
            break;
        case RawType::short_n:
            result.setInner(is_signed(spec) ? nxc::basic_type::sshort_type : nxc::basic_type::ushort_type);
            break;
        case RawType::int_n:
            result.setInner(is_signed(spec) ? nxc::basic_type::sint_type : nxc::basic_type::uint_type);
            break;
        case RawType::long_n:
            result.setInner(is_signed(spec) ? nxc::basic_type::slong_type : nxc::basic_type::ulong_type);
            break;
        case RawType::float_f:
            result.setInner(nxc::basic_type::float_type);
            break;
        case RawType::string_s:
            result.setInner(nxc::basic_type::string_type);
            break;
        case RawType::mutex_m:
            result.setInner(nxc::basic_type::mutex_type);
            break;
        case RawType::struct_u:
            parse_type(static_cast<struct_info &>(*spec.details), spec.location, result);
            break;
        case RawType::enum_u:
            parse_type(static_cast<enum_info &>(*spec.details), spec.location, result);
            break;
        case RawType::unknown_u:
            parse_type(static_cast<unknown_name &>(*spec.details), spec.location, result);
            break;
    }
}
template<typename T>
bool type_resolver_check(const std::string &name, nxc::ID parent, nxc::symbol_table::symbol_type &sym, nxc::type &out) {
    T &ref = boost::get<T> (sym);
    if (static_cast<nxc::sym_name      &>(ref).getName()     == name &&
        static_cast<nxc::sym_parent_id &>(ref).getParentID() == parent) {
        out.setInner(nxc::usertype{ref.getID()});
        return true;
    } else {
        return false;
    }
}
bool type_resolver_fn(nxc::scope_data &scope,
                      nxc::symbol_table &tbl,
                      const std::string &name,
                      nxc::type &out) {
    using namespace nxc;
    ID scopeID = scope.getID();

    auto it = tbl.begin();
    auto end = tbl.end();
    // for each symbol
    for (; it != end; ++it) {
        nxc::symbol_table::symbol_type &sym = it->second;

        switch (static_cast<sym_class>(sym.which())) {
            case sym_class::TYPEDEF: {
                if (type_resolver_check<typedef_data>(name, scopeID, sym, out))
                    return true;
                break;
            }
            case sym_class::ENUM: {
                if (type_resolver_check<enum_data>(name, scopeID, sym, out))
                    return true;
                break;
            }
            case sym_class::STRUCT: {
                if (type_resolver_check<struct_data>(name, scopeID, sym, out))
                    return true;
                break;
            }
            default:
                continue;
        }
    }
    // return KO
    return false;
}

void nxc::type_resolver::parse_type(const unknown_name &spec,
                                    const position_type &pos,
                                    nxc::type &out) {
    bool found = tbl.lookup_scopes
            (scope, type_resolver_fn, spec.name, out);

    if (!found) {
        std::string msg("Cannot resolve type: ");
        msg.append(spec.name);
        throw nxc::semantic_error(pos, msg.c_str());
    }
}

void nxc::type_resolver::arrayize(nxc::type base,
                                  const std::vector <declarator_array> &array,
                                  nxc::type &result) {
    if (array.size() == 0) {
        result = base;
        return;
    }
    nxc::arrays resarr;
    resarr.base(base);
    resarr.sizes().clear();
    for (const declarator_array &arr : array) {
        arrays::size_type size = SIZE_UNKNOWN;
        if (!arr.empty()) {
            if (!nxc::eval(size, *arr.size, scope, tbl)) {
                throw nxc::semantic_error(arr.location,
                                          "Array size evaluation failed.");
            }
        }
        resarr.sizes().push_back(size);
    }
    result.setInner(resarr);
    result.setConst(false);
}


bool struct_locator_fn(nxc::scope_data &scope,
                       nxc::symbol_table &table,
                       const std::string &name,
                       nxc::struct_data *&out) {
    bool found = table.search_scope<nxc::struct_data, nxc::sym_class::STRUCT>(scope.getID(),
                                                                              name,
                                                                              out);
    return found && out->getScopeID() != SCOPE_UNKNOWN;
}

// parse struct definition/declaration into usertype and register it into symbol table.
void nxc::type_resolver::parse_type(const struct_info &spec,
                                    const position_type &pos,
                                    nxc::type &out) {
    nxc::struct_data *existing_struct = nullptr;

    // complete struct definition
    if (spec.members) {
        // entry to fill in
        scope_data *sscope = nullptr;
        // this is a named struct
        if (spec.name) {
            // get struct name
            const std::string &name = *spec.name;
            bool colliding = tbl.detect_usertype_offender(scope, name);
            if (colliding) {
                std::string msg("User type collides with struct name: ");
                msg.append(name);
                throw semantic_error(pos, msg.c_str());
            }
            // look for this struct in this scope
            bool found = tbl.search_scope<struct_data, nxc::sym_class::STRUCT>
                    (scope, name, existing_struct);

            // this struct was referenced already
            if (found) {
                if (existing_struct->getScopeID() != SCOPE_UNKNOWN) {
                    std::string msg("Struct redefined: ");
                    msg.append(name);
                    throw semantic_error(pos, msg.c_str());
                }
                // continue adding members to the struct
                // ---
                // this struct is new
            } else {
                existing_struct = &tbl.new_struct();
                existing_struct->setParentID(scope);
                existing_struct->setName(name);
            }
            sscope = &tbl.new_scope(scope);
            // ---
            // this is an unnamed struct
        } else {
            sscope = &tbl.new_scope(scope);
            existing_struct = &tbl.new_struct();
            existing_struct->setParentID(scope);
            existing_struct->setName(std::string("__unnamed").append(std::to_string(tbl.size())));
        }
        existing_struct->setScopeID(sscope);
        for (auto &type : *spec.members) {
            const_rawtype &tspec = type.type;
            nxc::type t;
            parse_type(tspec, t);
            for (auto &instance : type.instances) {
                nxc::type arrayed;
                var_data &var = tbl.new_var();
                var.setParentID(sscope->getID());
                var.setName(instance.name);
                var.setStatic(false);
                existing_struct->members().push_back(var.getID());
                sscope->vars().push_back(var.getID());

                arrayize(arrayed, instance.arrays, t); // no typedefs inside struct
                var.setType(arrayed);
            }
        }
    } else {
        if (!spec.name) {
            throw semantic_error(pos, "Referencing unnamed struct");
        }
        // find existing struct with the right name

        bool found = tbl.lookup_scopes
                (scope, struct_locator_fn, *spec.name, existing_struct);

        // if no structs are found, let caller decide what to do
        if (!found) {
            std::string msg("Unknown struct: ");
            msg.append(*spec.name);
            throw semantic_error(pos, msg.c_str());
        }
    }
    existing_struct->setPrinted(spec.native);
    nxc::usertype idptr(existing_struct->getID());
    out.setInner(idptr);
}


bool enum_locator_fn(nxc::scope_data &scope,
                     nxc::symbol_table &table,
                     const std::string &name,
                     nxc::enum_data *&out) {
    bool found = table.search_scope<nxc::enum_data, nxc::sym_class::ENUM>(scope.getID(), name, out);
    return found && out->isComplete();
}

void nxc::type_resolver::parse_type(const enum_info &spec,
                                    const position_type &pos,
                                    nxc::type &entry) {
    nxc::enum_data *existing_enum;
    if (spec.members) {
        if (spec.name) {
            // get enum name
            std::string const &name = *spec.name;
            bool colliding = tbl.detect_usertype_offender(scope, name);
            if (colliding) {
                std::string msg("User type collides with enum name: ");
                msg.append(name);
                throw semantic_error(pos, msg.c_str());
            }
            // look for this enum in this scope
            bool found = tbl.search_scope<enum_data, nxc::sym_class::ENUM>(scope, name, existing_enum);

            // this enum was referenced already
            if (found) {
                if (existing_enum->isComplete()) {
                    std::string msg("Enum redefined: ");
                    msg.append(name);
                    throw semantic_error(pos, msg.c_str());
                }
                // continue adding members to the enum
                // ---
                // this enum is new
            } else {
                existing_enum = &tbl.new_enum();
                existing_enum->setParentID(scope);
                existing_enum->setName(name);
            }
            // ---
            // this is an unnamed enum
        } else {
            existing_enum = &tbl.new_enum();
            existing_enum->setParentID(scope);
            existing_enum->setName(std::string("__unnamed").append(std::to_string(tbl.size())));
        }
        int64_t last = -1;
        for (auto &enumerator : *spec.members) {
            bool offended = tbl.detect_var_offender(scope, enumerator.name);
            if (offended) {
                std::string msg("Enum constant name collides with existing symbol: ");
                msg.append(enumerator.name);
                throw semantic_error(pos, msg.c_str());
            }
            nxc::enum_data::member_type member;
            member.first = enumerator.name;
            if (enumerator.value) {
                arrays::size_type temp;
                if (!nxc::eval(temp, *enumerator.value, scope, tbl)) {
                    std::string msg("Enum constant evaluation failed: ");
                    msg.append(enumerator.name);
                    throw semantic_error(pos, msg.c_str());
                }
                member.second = (int64_t) temp;
            } else {
                member.second = last + 1;
            }
            last = member.second;
            existing_enum->getMembers().push_back(member);
        }
        existing_enum->setComplete(true);
    } else {
        if (!spec.name) {
            throw semantic_error(pos, "Referencing unnamed enum.");
        }
        const std::string &name = *spec.name;

        // find existing enum with the right name
        bool found = tbl.lookup_scopes
                (scope, enum_locator_fn, name, existing_enum);

        // if no enums are found, let caller decide what to do
        if (!found) {
            std::string msg("Unknown enum: ");
            msg.append(name);
            throw semantic_error(pos, msg.c_str());
        }
    }
    existing_enum->setPrinted(spec.native);
    nxc::usertype pointer(existing_enum->getID());
    entry.setInner(pointer);
}
