/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Implementation of compiler builtins.
 */


#include <global/include/symbols.hpp>
#include <parser2/include/ast.hpp>
#include <parser2/include/private/natives.hpp>
#include <serial2/include/private/serializer.hpp>


void serialize_passthrough(const std::string &name,
                           const nxc::ast_branch &call,
                           nxc::serializer &serial,
                           nxc::ID parent,
                           std::ostream &out);


bool nxc::array_stuff::parseCall(nxc::ast_branch &call,
                                 const std::vector<nxc::type> &resolved,
                                 nxc::symbol_table &syms) {
    const std::string &name = getFunctionName(call);
    if (name == "ArrayLen") {
        ArrayLen(call, resolved, syms);
    } else if (name == "ArrayInit" ||
               name == "ArrayBuild" ||
               name == "ArraySubset" ||
               name == "ArrayIndex" ||
               name == "ArrayReplace" ||
               name == "ArraySum" ||
               name == "ArrayMean" ||
               name == "ArraySumSqr" ||
               name == "ArrayStd" ||
               name == "ArrayMin" ||
               name == "ArrayMax" ||
               name == "ArraySort" ||
               name == "ArrayOp") {
        call.resultType->setInner(void_type{});
    } else {
        return false;
    }
    return true;
}

void nxc::array_stuff::ArrayLen(nxc::ast_branch &call,
                                const std::vector<nxc::type> &resolved,
                                nxc::symbol_table &syms) {
    if (call.nChildren() != 2) {
        throw semantic_error(call.location, "ArrayLen requires one argument");
    }
    nxc::type &firstArg = call.childAt(2).getType();
    firstArg = untypedefize(firstArg, syms);
    if (firstArg.which() != type_variants::array) {
        throw semantic_error(call.location, "ArrayLen requires array as an argument");
    }
    nxc::arrays &arr = firstArg.get<arrays>();
    nxc::arrays::size_type size = arr.sizes()[0];
    if (size == SIZE_UNKNOWN) {
        throw semantic_error(call.location, "ArrayLen doesn't know the size of the array.");
    }
    required_ptr_to<ast_node> p_val = nullptr;
    ast_value &val = PtrCreateCastAssignReturn<ast_value>(p_val,
                                                          ast_value_type::Constant_Integer,
                                                          std::to_string(size));
    val.location = call.location;
    val.annotation = ID_UNKNOWN;
    call.op = ast_op_type::Passthrough;
    call.children.clear();
    call.children.push_back(std::move(p_val));
    call.resultType->setInner(void_type{});
}

bool nxc::array_stuff::serializeCall(const nxc::ast_branch &call,
                                     const std::vector<nxc::type> &resolved,
                                     const nxc::symbol_table &syms,
                                     ID parent,
                                     serializer &serial,
                                     std::ostream &out) {
    const std::string &name = getFunctionName(call);
    if (name == "ArrayInit" ||
        name == "ArrayBuild" ||
        name == "ArraySubset" ||
        name == "ArrayIndex" ||
        name == "ArrayReplace" ||
        name == "ArraySum" ||
        name == "ArrayMean" ||
        name == "ArraySumSqr" ||
        name == "ArrayStd" ||
        name == "ArrayMin" ||
        name == "ArrayMax" ||
        name == "ArraySort" ||
        name == "ArrayOp") {

    }
    return false;
}


bool nxc::precedes::parseCall(nxc::ast_branch &call, const std::vector<nxc::type> &resolved, nxc::symbol_table &syms) {
    const std::string &name = getFunctionName(call);
    if (name == "Precedes") {
        call.resultType->setInner(void_type{});
        return true;
    } else {
        return false;
    }
}

void serialize_passthrough(const std::string &name,
                           const nxc::ast_branch &call,
                           nxc::serializer &serial,
                           nxc::ID parent,
                           std::ostream &out) {
    out << name << "(";
    auto it = call.children.begin() + 1;
    auto end = call.children.end();
    for (; it != end; ++it) {
        serial.serialize_expr(**it, parent);
        if (std::next(it) != end)
            out << ",";
    }
    out << ")";
}

bool nxc::precedes::serializeCall(const nxc::ast_branch &call,
                                  const std::vector<nxc::type> &resolved,
                                  const nxc::symbol_table &syms,
                                  ID parent,
                                  serializer &serial,
                                  std::ostream &out) {
    const std::string &name = getFunctionName(call);
    if (name == "Precedes") {
        serialize_passthrough(name, call, serial, parent, out);
        return true;
    } else {
        return false;
    }
}

bool nxc::tasks::parseCall(nxc::ast_branch &call,
                           const std::vector<nxc::type> &resolved,
                           nxc::symbol_table &syms) {
    const std::string &name = getFunctionName(call);
    if (name == "StartTask" || name == "StopTask" || name == "ExitTo") {
        call.resultType->setInner(void_type{});
        return true;
    } else {
        return false;
    }
}

bool nxc::tasks::serializeCall(const nxc::ast_branch &call,
                               const std::vector<nxc::type> &resolved,
                               const nxc::symbol_table &syms,
                               nxc::ID parent,
                               nxc::serializer &serial,
                               std::ostream &out) {
    const std::string &name = getFunctionName(call);
    if (name == "StartTask" || name == "StopTask" || name == "ExitTo") {
        serialize_passthrough(getRealName(name), call, serial, parent, out);
        return true;
    } else {
        return false;
    }
}

std::string nxc::tasks::getRealName(const std::string &name) {
    if (name == "StartTask") {
        return "__THREAD_START";
    } else if (name == "StopTask") {
        return "__THREAD_STOP";
    } else if (name == "ExitTo") {
        return "__THREAD_CHAIN";
    } else {
        return "<invalid>";
    }
}

bool nxc::cmath::parseCall(nxc::ast_branch &call, const std::vector<nxc::type> &resolved, nxc::symbol_table &syms) {
    const std::string &name = getFunctionName(call);
    for (auto &pair : m_info) {
        if (pair.second == name) {
            int32_t t = pair.first;
            if (t == -1) {
                call.resultType->setInner(void_type{});
            } else {
                call.resultType->setInner((basic_type)t);
            }
            return true;
        }
    }
    return false;
}

bool nxc::cmath::serializeCall(const nxc::ast_branch &call, const std::vector<nxc::type> &resolved,
                               const nxc::symbol_table &syms, nxc::ID parent, nxc::serializer &serial,
                               std::ostream &out) {
    const std::string &name = getFunctionName(call);
    for (auto &pair : m_info) {
        if (pair.second == name) {
            serialize_passthrough(name, call, serial, parent, out);
            return true;
        }
    }
    return false;
}

#define PUSH_FUNC(vec,rettype,name) vec.push_back(std::pair<int32_t, std::string>((int32_t)rettype, name))
nxc::cmath::cmath() {
    PUSH_FUNC(m_info, basic_type::float_type, "acos");
    PUSH_FUNC(m_info, basic_type::float_type, "acosd");
    PUSH_FUNC(m_info, basic_type::float_type, "asin");
    PUSH_FUNC(m_info, basic_type::float_type, "asind");
    PUSH_FUNC(m_info, basic_type::float_type, "atan");
    PUSH_FUNC(m_info, basic_type::float_type, "atand");
    PUSH_FUNC(m_info, basic_type::float_type, "atan2");
    PUSH_FUNC(m_info, basic_type::float_type, "atan2d");
    PUSH_FUNC(m_info, basic_type::float_type, "ceil");
    PUSH_FUNC(m_info, basic_type::float_type, "cos");
    PUSH_FUNC(m_info, basic_type::float_type, "cosd");
    PUSH_FUNC(m_info, basic_type::float_type, "cosh");
    PUSH_FUNC(m_info, basic_type::float_type, "coshd");
    PUSH_FUNC(m_info, basic_type::float_type, "exp");
    PUSH_FUNC(m_info, basic_type::float_type, "floor");
    PUSH_FUNC(m_info, basic_type::bool_type, "isNAN");
    PUSH_FUNC(m_info, basic_type::float_type, "log");
    PUSH_FUNC(m_info, basic_type::float_type, "log10");
    PUSH_FUNC(m_info, basic_type::float_type, "pow");
    PUSH_FUNC(m_info, basic_type::float_type, "abs");
    PUSH_FUNC(m_info, basic_type::float_type, "sign");
    PUSH_FUNC(m_info, basic_type::float_type, "sin");
    PUSH_FUNC(m_info, basic_type::float_type, "sind");
    PUSH_FUNC(m_info, basic_type::float_type, "sqrt");
    PUSH_FUNC(m_info, basic_type::float_type, "tan");
    PUSH_FUNC(m_info, basic_type::float_type, "tand");
    PUSH_FUNC(m_info, basic_type::float_type, "tanh");
    PUSH_FUNC(m_info, basic_type::float_type, "tanhd");
    PUSH_FUNC(m_info, basic_type::float_type, "frac");
    PUSH_FUNC(m_info, basic_type::float_type, "trunc");
    PUSH_FUNC(m_info, basic_type::slong_type, "rand");
    PUSH_FUNC(m_info, -1, "srand");
    PUSH_FUNC(m_info, basic_type::float_type, "strtod");
    PUSH_FUNC(m_info, basic_type::slong_type, "strtol");
    PUSH_FUNC(m_info, basic_type::slong_type, "strtoul");
    PUSH_FUNC(m_info, -1, "memcpy");
    PUSH_FUNC(m_info, -1, "memmove");
    PUSH_FUNC(m_info, basic_type::schar_type, "memcpy");
    PUSH_FUNC(m_info, basic_type::string_type, "strcpy");
    PUSH_FUNC(m_info, basic_type::string_type, "strncpy");
    PUSH_FUNC(m_info, basic_type::string_type, "strcat");
    PUSH_FUNC(m_info, basic_type::string_type, "strncat");
    PUSH_FUNC(m_info, basic_type::sint_type, "strcmp");
    PUSH_FUNC(m_info, basic_type::sint_type, "strncmp");
    PUSH_FUNC(m_info, basic_type::sint_type, "strlen");

    PUSH_FUNC(m_info, basic_type::sint_type, "isalnum");
    PUSH_FUNC(m_info, basic_type::sint_type, "isalpha");
    PUSH_FUNC(m_info, basic_type::sint_type, "iscntrl");
    PUSH_FUNC(m_info, basic_type::sint_type, "isdigit");
    PUSH_FUNC(m_info, basic_type::sint_type, "isgraph");
    PUSH_FUNC(m_info, basic_type::sint_type, "isprint");
    PUSH_FUNC(m_info, basic_type::sint_type, "islower");
    PUSH_FUNC(m_info, basic_type::sint_type, "ispunct");
    PUSH_FUNC(m_info, basic_type::sint_type, "isspace");
    PUSH_FUNC(m_info, basic_type::sint_type, "isupper");
    PUSH_FUNC(m_info, basic_type::sint_type, "isxdigit");
    PUSH_FUNC(m_info, basic_type::sint_type, "tolower");
    PUSH_FUNC(m_info, basic_type::sint_type, "toupper");
}
