/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Integration of the symbol table with the parser.
 */


#include <parser2/include/private/symtrack.hpp>
#include <eval/include/evaluator.hpp>


////////////////
// SYMTRACKER //
////////////////

nxc::symtracker::symtracker(symbol_table &syms)
        : syms(syms),
          currentScopeID(SCOPE_GLOBAL),
          gtparse(syms, currentScope()),
          flookup(syms) {

}

nxc::task_data &nxc::symtracker::registerTask(nxc::efuncinfo &ref, const std::string &name) {
    task_data *signature = find_task(name);
    if (signature == nullptr) {
        signature = &syms.new_task();
        signature->setName(name);
    }
    ref.id = signature->getID();
    return *signature;
}

nxc::scope_data &nxc::symtracker::beginFunctionScope(nxc::func_data &out_func, const my_pos &pos) {
    if (out_func.getBodyID() != SCOPE_UNKNOWN) {
        throwError(pos, "Function redefined", out_func.getName());
    }
    scope_data &out_scope = pushScope();
    out_func.setBodyID(out_scope);

    for (const func_data::arg_type &arg : out_func.getArguments()) {
        var_data &data = syms.new_var();
        data.setArgument(true);
        data.setStatic(false);
        data.setType(arg.first);
        data.setName(arg.second);
        data.setParentID(out_scope.getID());
        out_scope.vars().push_back(data.getID());
    }
    return out_scope;
}

void nxc::symtracker::endFunctionScope() {
    popScope();
}

nxc::scope_data &nxc::symtracker::beginTaskScope(nxc::task_data &task, const my_pos &pos) {
    if (task.getBodyID() != SCOPE_UNKNOWN) {
        throwError(pos, "Task redefinition isn't allowed", task.getName());
    }
    scope_data &scope = pushScope();
    task.setBodyID(scope);
    return scope;
}

void nxc::symtracker::endTaskScope() {
    popScope();
}


nxc::func_data &nxc::symtracker::registerFunction(nxc::efuncinfo &ref,
                                                  const std::string &name,
                                                  FunctionModifierMask modifiers,
                                                  nxc::func_data::arg_list_type params,
                                                  size_t argc,
                                                  const boost::optional<nxc::type> &retval) {
    auto extraArgs = params.begin();
    std::advance(extraArgs, argc);
    params.erase(extraArgs, params.end());
    func_data *out_func = find_func(name, params);

    if (out_func != nullptr) {
        auto &retType = out_func->getReturnType();
        if (retType != retval) {
            throwError(ref.location, "Function defined with different return type", out_func->getName());
        }
        if (out_func->getFlags() != modifiers) {
            throwError(ref.location, "Function defined with different flags", out_func->getName());
        }
    } else {
        out_func = &syms.new_func();
        out_func->setName(name);
        out_func->setReturnType(retval);
        out_func->setFlags(modifiers);
    }
    ref.id = out_func->getID();
    out_func->setArguments(params); // argument variable names

    return *out_func;
}


nxc::func_data &nxc::symtracker::registerFunction(nxc::efuncinfo &ref,
                                                  const std::string &name,
                                                  FunctionModifierMask modifiers,
                                                  const list_of<required_ptr_to<parameter>> &params,
                                                  optional_ptr_to<type_name> &returnType,
                                                  list_of<required_ptr_to<efuncinfo>> &auxiliary) {

    boost::optional<nxc::type> f_retval = resolveFunctionReturn(returnType);
    nxc::func_data::arg_list_type f_args = resolveFunctionArgs(params);

    // TODO this code looks really ugly, I should think of better alternative
    size_t optionals = checkOptArgs(name, ref.location, params);
    size_t allArgs = params.size();
    size_t minArgs = allArgs - optionals;
    nxc::func_data *allArgsRegistered = nullptr;
    for (size_t args = minArgs; args <= allArgs; args++) {
        if (args != allArgs) {
            required_ptr_to<efuncinfo> decl_ptr;
            efuncinfo &newFunc = PtrCreateAssignReturn<efuncinfo>(decl_ptr);

            newFunc.location = ref.location;
            nxc::func_data &registered = registerFunction(newFunc, name, modifiers, f_args, args, f_retval);
            nxc::type retVal = (bool) f_retval ?
                               f_retval.get() :
                               nxc::type(void_type{});

            scope_data &scope = beginFunctionScope(registered, newFunc.location);

            statement &braces = PtrCreateAssignReturn(newFunc.code);
            braces.type = StatementType::Compound;

            compound_statement &compound = PtrCreateCastAssignReturn<compound_statement>(braces.stmt);
            compound.id = scope.getID();

            registerFunction_saveArgs(params, f_args, allArgs, args, scope, compound);

            nxc::expression_declaration line;
            line.type = ExpressionDeclaration::Expression;

            in_stmt &expr_line = PtrCreateCastAssignReturn<in_stmt>(line.ptr);
            expr_line.stmt.type = StatementType::Jump_Return;

            expression_statement &expr_stmt = PtrCreateCastAssignReturn<expression_statement>(expr_line.stmt.stmt);

            ast_branch &call = PtrCreateCastAssignReturn<ast_branch>(expr_stmt.expression);
            call.op = ast_op_type::Call;
            call.annotation = registered.getID();
            call.location = newFunc.location;
            call.resultType = PtrCreate<type>(retVal);

            required_ptr_to<ast_value> name_node = PtrCreate<ast_value>(ast_value_type::Identifier, name);
            name_node->annotation = registered.getID();
            name_node->location = newFunc.location;
            name_node->resultType = PtrCreate<type>(retVal);
            call.children.push_back(std::move(name_node));

            for (size_t arg = 0; arg < allArgs; ++arg) {
                required_ptr_to<ast_value> arg_node = PtrCreate<ast_value>(ast_value_type::Identifier);
                var_data &var = syms.get_var(scope.vars()[arg]);
                arg_node->value = var.getName();
                arg_node->annotation = var.getID();
                arg_node->location = newFunc.location;
                arg_node->resultType = PtrCreate<type>(var.getType());
                call.children.push_back(std::move(arg_node));
            }
            compound.statements.push_back(std::move(line));
            endFunctionScope();

            auxiliary.push_back(std::move(decl_ptr));
        } else {
            nxc::func_data &registered = registerFunction(ref, name, modifiers, f_args, args, f_retval);
            allArgsRegistered = &registered;
        }
    }
    return *allArgsRegistered;
}

void nxc::symtracker::registerFunction_saveArgs(const nxc::list_of<std::unique_ptr<nxc::parameter>> &params,
                                                nxc::func_data::arg_list_type &resolvedParams,
                                                size_t allArgs,
                                                size_t args,
                                                nxc::scope_data &scope,
                                                nxc::compound_statement &compound) {
    auto argIt = resolvedParams.begin();
    advance(argIt, args);
    for (size_t arg = args; arg < allArgs; ++arg, ++argIt) {

        nxc::expression_declaration line;
        line.type = nxc::ExpressionDeclaration::Declaration;
        nxc::in_decl &decl_line = PtrCreateCastAssignReturn<nxc::in_decl>(line.ptr);

        nxc::edeclinfo &decl = PtrCreateAssignReturn(decl_line.declaration);
        decl.modifier = nxc::DeclarationModifier::None;
        if (argIt->first.which() == nxc::type_variants::array) {
            decl.baseType = nxc::PtrCreate<nxc::type>(argIt->first.get<nxc::arrays>().base().get());
        } else {
            decl.baseType = PtrCreate<nxc::type>(argIt->first);
        }
        std::vector<nxc::instance_info> &instances = PtrCreateAssignReturn(decl.instances);

        nxc::instance_info info;
        nxc::var_data &var = this->syms.new_var();
        var.setArgument(false);
        var.setParentID(scope);
        var.setStatic(false);
        var.setType(argIt->first);
        var.setName(argIt->second);
        scope.vars().push_back(var.getID());

        info.id = var.getID();
        info.value = PtrCreate<nxc::initializer>(*params.at(arg)->decl.value);
        instances.push_back(std::move(info));

        compound.statements.push_back(std::move(line));
    }
}

size_t nxc::symtracker::checkOptArgs(const std::string &name,
                                     const position_type &pos,
                                     const nxc::list_of<std::unique_ptr<nxc::parameter>> &params) {
    size_t anyCount = 0;
    for (const auto &arg : params) {
        if (arg->decl.value) {
            anyCount++;
        } else {
            if (anyCount > 0) {
                throwError(pos, "Non-optional arg follows optional arg.", name);
            }
        }
    }
    return anyCount;
}

boost::optional<nxc::type> nxc::symtracker::resolveFunctionReturn(optional_ptr_to<nxc::type_name> &source) {
    if (!source)
        return boost::none;

    boost::optional<nxc::type> type = nxc::type{};
    gtparse.parse_type(*source, *type);
    return type;
}

nxc::func_data::arg_list_type nxc::symtracker::resolveFunctionArgs(
        const nxc::list_of<nxc::required_ptr_to<nxc::parameter>> &source) {
    nxc::func_data::arg_list_type argList;

    for (const auto &param : source) {
        func_data::arg_type arg;
        nxc::type tempInArr;

        gtparse.parse_type(param->type, tempInArr);
        tempInArr.setReference(param->reference);

        gtparse.arrayize(std::move(tempInArr), param->decl.decl.arrays, arg.first);

        arg.second = param->decl.decl.name;

        argList.push_back(std::move(arg));
    }

    return argList;
}

nxc::scope_data &nxc::symtracker::pushScope() {
    scope_data &ref = syms.new_scope(currentScopeID);
    currentScopeID = ref.getID();
    return ref;
}

nxc::scope_data &nxc::symtracker::popScope() {
    scope_data &ref = currentScope();
    currentScopeID = ref.getParentID();
    return currentScope();
}

nxc::scope_data &nxc::symtracker::currentScope() {
    return syms.get_scope(currentScopeID);
}

bool nxc::symtracker::args_equal(nxc::func_data::arg_list_type const &a,
                                 nxc::func_data::arg_list_type const &b) {
    using namespace nxc;
    if (a.size() != b.size())
        return false;
    auto it_a = a.begin(), end_a = a.end();
    auto it_b = b.begin(), end_b = b.end();
    for (; it_a != end_a && it_b != end_b; ++it_a, ++it_b) {
        const nxc::type &x = it_a->first;
        const nxc::type &y = it_b->first;
        if (x.getInner() != y.getInner()
            || x.isReference() != y.isReference()
            || x.isConst() != y.isConst()) {
            return false;
        }
    }
    return true;
}

nxc::task_data *nxc::symtracker::find_task(const std::string &name) {
    using namespace nxc;

    task_data *ptr = nullptr;
    bool ok = syms.find_everywhere<task_data, nxc::sym_class::TASK>(name, ptr);
    if (!ok)
        return nullptr;
    return ptr;
}

nxc::func_data *nxc::symtracker::find_func(const std::string &name,
                                           nxc::func_data::arg_list_type const &args) {
    using namespace nxc;

    auto it = syms.begin();
    auto end = syms.end();
    for (; it != end; ++it) {
        auto &sym = it->second;
        if (sym.which() != static_cast<int>(nxc::sym_class::FUNCTION))
            continue;
        func_data &found = boost::get<func_data>(sym);
        if (found.getName() == name && args_equal(found.getArguments(), args)) {
            return &found;
        }
    }
    return nullptr;
}


void nxc::symtracker::tryEvaluate(nxc::var_data &info, const initializer_declaration &instance) {
    const nxc::type &real = info.getType();

    bool is_initialized = (bool) instance.value;
    bool is_const = real.isConst();
    bool is_expr = real.which() != nxc::type_variants::array;

    if (!is_initialized || !is_const || !is_expr)
        return;

    nxc::type direct = untypedefize(real, syms);
    bool is_basic = direct.which() == nxc::type_variants::basic;
    if (!is_basic)
        return;

    initializer &init = *instance.value;
    if (init.type != InitializerType::Expression)
        return;
    initializer_expr &ex = PtrCastRef<initializer_expr>(init.ptr);

    nxc::basic_type kind = direct.get<nxc::basic_type>();
    switch (kind) {
        case nxc::basic_type::bool_type:
        case nxc::basic_type::byte_type:
        case nxc::basic_type::schar_type:
        case nxc::basic_type::uchar_type:
        case nxc::basic_type::sint_type:
        case nxc::basic_type::uint_type:
        case nxc::basic_type::sshort_type:
        case nxc::basic_type::ushort_type:
        case nxc::basic_type::slong_type:
        case nxc::basic_type::ulong_type:
        case nxc::basic_type::float_type: {
            nxc::evaluator::result_type temp;
            nxc::evaluator ev(syms, currentScope(), false);

            if (ev.eval(*ex.expr, temp)) {
                info.setValue(temp);
            }
            break;
        }
        case nxc::basic_type::mutex_type:
        case nxc::basic_type::string_type:
            return;
    }
}


void nxc::symtracker::registerVar(instance_info &outinfo,
                                  const nxc::type &real,
                                  initializer_declaration &instance,
                                  bool is_static) {
    const std::string &name = instance.decl.name;

    bool offended = syms.detect_var_offender(currentScopeID, name);
    if (offended) {
        throwError(instance.decl.location, "Declaration collides with another symbol", name);
    }
    nxc::var_data &info = syms.new_var();
    info.setParentID(currentScopeID);
    info.setName(std::move(name));
    info.setType(std::move(real));
    info.setStatic(is_static);
    info.setArgument(false);
    currentScope().vars().push_back(info.getID());

    outinfo.id = info;

    PtrAssign(outinfo.value, std::move(instance.value));

    tryEvaluate(info, instance);
}

void nxc::symtracker::registerTypedef(instance_info &outinfo,
                                      const nxc::type &real,
                                      const initializer_declaration &instance) {
    const std::string &name = instance.decl.name;

    bool colliding = syms.detect_usertype_offender(currentScopeID, name);
    if (colliding) {
        throwError(instance.decl.location, "User type collides with typedef name", name);
    }
    nxc::typedef_data &info = syms.new_typedef();
    info.setParentID(currentScopeID);
    info.setName(std::move(name));
    info.setType(std::move(real));
    outinfo.id = info;
    outinfo.value = nullptr;
    if (instance.value) {
        throwError(instance.decl.location, "Typedef initializer makes no sense", name);
    }
}


void nxc::symtracker::registerDeclaration(edeclinfo &decl,
                                          const_rawtype &baseType,
                                          list_of<initializer_declaration> &instances) {
    decl.baseType = PtrCreate<nxc::type>();
    list_of<instance_info> &outInstances = PtrCreateAssignReturn(decl.instances);

    type_resolver tparse(syms, currentScope());
    tparse.parse_type(baseType, *decl.baseType);

    if (instances.size() == 0)
        return;
    bool is_static = decl.modifier == DeclarationModifier::Static;
    bool is_typedef = decl.modifier == DeclarationModifier::Typedef;


    for (initializer_declaration &instance : instances) {
        instance_info info;
        const std::string &name = instance.decl.name;
        nxc::type real;
        tparse.arrayize(*decl.baseType, instance.decl.arrays, real);

        if (is_typedef) { // typedef mode
            registerTypedef(info, real, instance);
        } else { // variable mode
            registerVar(info, real, instance, is_static);
        }
        outInstances.push_back(std::move(info));
    }
}

void nxc::symtracker::resolveTypeName(const nxc::type_name &name, nxc::type &type) {
    type_resolver tparse(syms, currentScope());
    tparse.parse_type(name, type);
}

bool nxc::symtracker::memberType(const nxc::usertype &base,
                                 const std::string &mName,
                                 nxc::type &out,
                                 ID &membID) {
    usertype_kind kind = whichtype(base, syms);
    if (kind != usertype_kind::struct_kind)
        return false;
    struct_data &data = syms.get_struct(base);

    for (ID memb : data.members()) {
        var_data &var = syms.get_var(memb);
        if (var.getName() == mName) {
            membID = var.getID();
            out = var.getType();
            return true;
        }
    }

    return false;
}

bool nxc::symtracker::callType(ast_branch &call,
                               const std::vector<nxc::type> &argTypes) {
    if (natives.parse(call, argTypes, syms))
        return true;
    func_data *finding = flookup.find(getFunctionName(call), argTypes);
    if (finding == nullptr)
        return false;
    call.annotation = finding->getID();
    if (finding->getReturnType()) {
        *call.resultType = finding->getReturnType().get();
    } else {
        call.resultType->setInner(void_type{});
    }
    return true;
}


bool ident_prober(nxc::symbol_table::symbol_type &sym,
                  nxc::scope_data &scope,
                  nxc::symbol_table &tbl,
                  nxc::type &outtype,
                  nxc::ID &outID,
                  std::string const &neededname) {
    using namespace nxc;
    sym_class which = static_cast<sym_class>(sym.which());
    if (which == sym_class::VARIABLE) {
        const var_data &var = boost::get<var_data>(sym);
        if (neededname == var.getName()) {
            // update outtype
            outID = var.getID();
            outtype = var.getType();
            return true;
        }
    } else if (which == sym_class::ENUM) {
        const enum_data &data = boost::get<enum_data>(sym);
        for (enum_data::member_type memb : data.getMembers()) {
            if (neededname == memb.first) {
                outID = data.getID();
                outtype.setConst(true);
                outtype.setInner(enum_datatype);
                return true;
            }
        }
    }
    return false;
}

void nxc::symtracker::identifierType(const std::string &name, nxc::type &out, ID &identID) {
    bool found = syms.lookup_symbols(currentScopeID, sym_mask::VARIABLE, ident_prober, out, identID, name);
    if (!found)
        out.setInner(void_type{});
}

bool nxc::symtracker::indexType(nxc::ast_branch &node) {
    type &arrType = node.firstChild().getType();
    arrType = untypedefize(arrType, syms);
    if (arrType.which() != type_variants::array)
        return false;
    // intentional copy
    arrays arr = arrType.get<arrays>();
    if (arr.sizes().size() > 1) {
        arr.sizes().erase(arr.sizes().begin());
        node.resultType = PtrCreate<type>(arr);
    } else {
        node.resultType = PtrCreate<type>(arr.base().get());
    }
    return true;
}

void nxc::symtracker::throwError(const nxc::my_pos &position, std::string &&what) {
    throw semantic_error(position, what.c_str());
}

void nxc::symtracker::throwError(const nxc::my_pos &position, std::string &&what, const std::string &name) {
    what.append(": ").append(name);
    throw semantic_error(position, what.c_str());
}

nxc::basic_type nxc::symtracker::getIntegerType(nxc::ast_value &value) {
    evaluator eval(syms, currentScope(), true);
    evaluator::result_type val;
    eval.eval(value, val);
    if (val == 0 || val == 1)
        return basic_type::bool_type;
    else if (val <= +0x0000007F && val >= -0x00000080)
        return basic_type::schar_type;
    else if (val <= +0x000000FF && val >= -0x00000000)
        return basic_type::uchar_type;
    else if (val <= +0x00007FFF && val >= -0x00008000)
        return basic_type::sint_type;
    else if (val <= +0x0000FFFF && val >= -0x00000000)
        return basic_type::uint_type;
    else if (val <= +0x7FFFFFFF && val >= -0x80000000)
        return basic_type::slong_type;
    else if (val <= +0xFFFFFFFF && val >= -0x00000000)
        return basic_type::ulong_type;
    else
        return basic_type::float_type;
}
