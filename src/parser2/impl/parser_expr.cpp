/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * NXC expression parser.
 */


#include <parser2/include/private/parser.hpp>
#include <eval/include/evaluator.hpp>


bool nxc::parser::parseOrExpr(nxc::ast_branch &parent) {
    return parseSingleOp_TMerge
            <&parser::parseAndExpr, T2_OROR, ast_op_type::LogicOr>
            (parent, "or-parser failed");
}

bool nxc::parser::parseAndExpr(nxc::ast_branch &parent) {
    return parseSingleOp_TMerge
            <&parser::parseBitOrExpr, T2_ANDAND, ast_op_type::LogicAnd>
            (parent, "and-parser failed");
}

bool nxc::parser::parseBitOrExpr(nxc::ast_branch &parent) {
    return parseSingleOp_TMerge
            <&parser::parseBitXorExpr, T2_OR, ast_op_type::BitOr>
            (parent, "bitor-parser failed");
}

bool nxc::parser::parseBitXorExpr(nxc::ast_branch &parent) {
    return parseSingleOp_TMerge
            <&parser::parseBitAndExpr, T2_XOR, ast_op_type::BitXor>
            (parent, "bitxor-parser failed");
}

bool nxc::parser::parseBitAndExpr(nxc::ast_branch &parent) {
    return parseSingleOp_TMerge
            <&parser::parseEqualityExpr, T2_AND, ast_op_type::BitAnd>
            (parent, "bitand-parser failed");
}

bool nxc::parser::parseExpr(ast_branch &root) {
    return parseSingleOp_TLast
            <&parser::parseAssignmentExpr, T2_COMMA, ast_op_type::Comma>
            (root, "comma-parser failed");
}

bool nxc::parser::getAssignmentOp(nxc::ast_op_type &out) {
    switch (currentID()) {
        case T2_ASSIGN:
            out = ast_op_type::Assign_Only;
            break;
        case T2_ANDASSIGN:
            out = ast_op_type::Assign_BitAnd;
            break;
        case T2_ORASSIGN:
            out = ast_op_type::Assign_BitOr;
            break;
        case T2_XORASSIGN:
            out = ast_op_type::Assign_BitXor;
            break;
        case T2_DIVIDEASSIGN:
            out = ast_op_type::Assign_Divide;
            break;
        case T2_SIGNASSIGN:
            out = ast_op_type::Assign_Sign;
            break;
        case T2_MINUSASSIGN:
            out = ast_op_type::Assign_Subtract;
            break;
        case T2_PERCENTASSIGN:
            out = ast_op_type::Assign_Modulo;
            break;
        case T2_PLUSASSIGN:
            out = ast_op_type::Assign_Add;
            break;
        case T2_ABSASSIGN:
            out = ast_op_type::Assign_Abs;
            break;
        case T2_SHIFTLEFTASSIGN:
            out = ast_op_type::Assign_Leftshift;
            break;
        case T2_SHIFTRIGHTASSIGN:
            out = ast_op_type::Assign_Rightshift;
            break;
        case T2_STARASSIGN:
            out = ast_op_type::Assign_Multiply;
            break;
        default:
            return false;
    }
    return true;
}

bool nxc::parser::parseAssignmentExpr(ast_branch &root) {
    CREATE_BACKUP(bkp);
    auto parent = PtrCreate<ast_branch>();
    if (!parseUnaryExpr(*parent) || !getAssignmentOp(parent->op)) {
        RESTORE_BACKUP(bkp);
        return parseTernaryExpr(root);
    }
    parent->resultType = PtrCreate<nxc::type>(parent->firstChild().getType());
    advance();

    bool result = parseAssignmentExpr(*parent);
    PtrPushCast(root.children, parent);
    return result;
}

bool nxc::parser::parseTernaryExpr(ast_branch &root) {
    if (!parseOrExpr(root)) {
        return false;
    }
    // eat question mark
    if (!tokenIs(T2_QUESTION_MARK))
        return true;
    advance();
    return parseTernary(root);
}


bool nxc::parser::parseTernary(ast_branch &root) {
    // swap || expression with ?: expression
    ast_branch &ternary_tree = CreateSwapLast(root, ast_op_type::Ternary);
    // parse true case
    if (!parseExpr(ternary_tree))
        throwParseError("cannot parse ternary true-case");
    // eat colon
    eatToken(T2_COLON, "ternary colon expected");
    // parse ternary expr
    bool ok = parseTernaryExpr(ternary_tree);
    if (!ok)
        return false;
    ternary_tree.resultType = PtrCreate<nxc::type>();
    combiner.combine(ternary_tree.childAt(1).getType(),
                     ternary_tree.childAt(2).getType(),
                     ternary_tree.getType());
    return true;
}


template<nxc::parser::parser_fn next, nxc::tokenID_type thisID, nxc::ast_op_type thisType>
bool nxc::parser::parseSingleOp_TLast(ast_branch &parent, const char *message) {
    if (!((this->*next)(parent)))
        return false;
    if (!tokenIs(thisID))
        return true;
    ast_branch &thisBranch = CreateSwapLast(parent, thisType);
    thisBranch.resultType = PtrCreate<nxc::type>(thisBranch.children[0]->getType());
    while (tokenIs(thisID)) {
        advance();
        if (!((this->*next)(thisBranch)))
            throwParseError(message);
    }
    *thisBranch.resultType = thisBranch.lastChild().getType();
    return true;
}

/*
template<nxc::parser::parser_fn next, nxc::tokenID_type thisID, nxc::ast_op_type thisType>
bool nxc::parser::parseSingleOp_TMerge(ast_branch &parent, const char *message) {
    if (!((this->*next)(parent)))
        return false;
    if (!tokenIs(thisID))
        return true;
    ast_branch &thisBranch = CreateSwapLast(parent, thisType);
    thisBranch.resultType = PtrCreate<nxc::type>(thisBranch.children[0]->getType());
    while (tokenIs(thisID)) {
        advance();
        if (!((this->*next)(thisBranch)))
            throwParseError(message);

        nxc::type temp;
        ast_node &last = thisBranch.lastChild();
        combiner.combine(last.getType(), *thisBranch.resultType, temp);
        (*thisBranch.resultType) = temp;
    }
    return true;
}*/

template<nxc::parser::parser_fn next, nxc::tokenID_type thisID, nxc::ast_op_type thisType>
bool nxc::parser::parseSingleOp_TMerge(ast_branch &parent, const char *message) {
    auto haveNext = [this]() { return tokenIs(thisID); };
    auto categoryFn = []() { return thisType; };
    return parseMultiOp_TNum<next>(parent, haveNext, categoryFn, message);
}

template<nxc::parser::parser_fn next, typename PredicateFnT, typename CategoryFnT>
bool nxc::parser::parseMultiOp_TNum(ast_branch &node,
                                    PredicateFnT haveNext,
                                    CategoryFnT identify,
                                    const char *message) {
    if (!((this->*next)(node)))
        return false;
    while (haveNext()) {
        nxc::ast_branch &parent = CreateSwapLast(node, identify());
        parent.resultType = PtrCreate<nxc::type>();
        advance();
        if (!((this->*next)(parent)))
            throwParseError(message);

        combiner.combine(parent.children[0]->getType(),
                         parent.children[1]->getType(),
                         *parent.resultType);
    }
    return true;
}


template<nxc::parser::parser_fn next, typename PredicateFnT, typename CategoryFnT>
bool nxc::parser::parseMultiOp_TBool(ast_branch &node,
                                     PredicateFnT haveNext,
                                     CategoryFnT identify,
                                     const char *message) {
    if (!((this->*next)(node)))
        return false;
    while (haveNext()) {
        nxc::ast_branch &parent = CreateSwapLast(node, identify());
        parent.resultType = PtrCreate<nxc::type>(basic_type::bool_type);
        advance();
        if (!((this->*next)(parent)))
            throwParseError(message);
    }
    return true;
}

bool nxc::parser::parseCastExpr(nxc::ast_branch &node) {
    size_t size = node.children.size();
    CREATE_BACKUP(bkp);
    if (parseUnaryExpr(node))
        return true;
    RESTORE_BACKUP(bkp);

    if (size < node.children.size())
        node.children.erase(std::prev(node.children.end()));

    return parseCast(node);
}

bool nxc::parser::parseUnaryExpr(nxc::ast_branch &node) {
    switch (currentID()) {
        case T2_SIZEOF:
            return parseSizeof(node);
        case T2_PLUSPLUS:
        case T2_MINUSMINUS:
            return parsePreStuff(node);
        case T2_PLUS:
        case T2_MINUS:
        case T2_COMPL:
        case T2_NOT:
            return parseUnaryOp(node);
        default:
            return parsePostfixExpr(node);
    }
}

bool nxc::parser::parseSizeof(nxc::ast_branch &node) {
    if (!tokenIs(T2_SIZEOF))
        return false;
    advance();
    eatToken(T2_LEFTPAREN, "sizeof opening parenthesis expected");
    ast_branch &szof = CreatePushReturn<ast_branch>(node.children);
    CREATE_BACKUP(bkp);

    type_name tname;
    if (parseTypeName(tname)) {
        szof.op = ast_op_type::Sizeof_Type;
        ast_typename &container = CreatePushReturn<ast_typename>(szof.children);

        type &ttype = PtrCreateAssignReturn(container.name);
        sym.resolveTypeName(std::move(tname), ttype);
    } else {
        szof.op = ast_op_type::Sizeof_Expr;
        RESTORE_BACKUP(bkp);
        if (!parseUnaryExpr(szof))
            return false;
    }
    szof.resultType = PtrCreate<type>(basic_type::uint_type);
    eatToken(T2_RIGHTPAREN, "sizeof closing parenthesis expected");
    return true;
}

bool nxc::parser::parsePreStuff(nxc::ast_branch &node) {
    nxc::ast_branch &container = CreatePushReturn<ast_branch>(node.children);
    switch (currentID()) {
        case T2_PLUSPLUS:
            container.op = ast_op_type::Increment_Pre;
            break;
        case T2_MINUSMINUS:
            container.op = ast_op_type::Decrement_Pre;
            break;
        default:
            return false;
    }
    advance();
    bool ok = parseUnaryExpr(container);
    if (!ok)
        return false;
    container.resultType = PtrCreate<type>(container.lastChild().getType());
    return true;
}

bool nxc::parser::parseUnaryOp(nxc::ast_branch &node) {
    nxc::ast_branch &container = CreatePushReturn<ast_branch>(node.children);
    switch (currentID()) {
        case T2_PLUS:
            container.op = ast_op_type::Unary_Plus;
            break;
        case T2_MINUS:
            container.op = ast_op_type::Unary_Minus;
            break;
        case T2_COMPL:
            container.op = ast_op_type::Unary_Complement;
            break;
        case T2_NOT:
            container.op = ast_op_type::Unary_Negate;
            break;
        default:
            return false;
    }
    advance();
    bool ok = parseCastExpr(container);
    if (!ok)
        return false;
    container.resultType = PtrCreate<type>(container.lastChild().getType());
    return true;
}

void nxc::parser::callType(nxc::ast_branch &node) {
    node.resultType = PtrCreate<type>();

    const std::string &name = getFunctionName(node);
    std::vector<nxc::type> args = getFunctionArgTypes(node);

    if (!sym.callType(node, args)) {
        std::stringstream str;
        str << "function not found";
        str << " (name = '" << name << "', ";
        str << "argtypes = {";

        auto it  = args.begin();
        auto end = args.end();
        for (; it != end; ++it) {
            str << "'" << (std::string)*it << "'";
            if (std::next(it) != end)
                str << ", ";
        }

        str << "})";
        throwSemanticError(str.str().c_str());
    }
}

void nxc::parser::indexType(nxc::ast_branch &node) {
    if (!sym.indexType(node))
        throwSemanticError("indexing non-array type");
}

void nxc::parser::memberType(nxc::ast_branch &node) {
    type &t = node.childAt(0).getType();
    if (t.which() != type_variants::user)
        throwSemanticError("accessing non-struct type");
    usertype &user = t.get<usertype>();

    ast_value &val = static_cast<ast_value &>(node.childAt(1));

    node.resultType = PtrCreate<type>();
    if (!sym.memberType(user, val.value, *node.resultType, node.annotation))
        throwSemanticError("accessed member type deduction failed");
}

bool nxc::parser::parsePostfixExpr(nxc::ast_branch &node) {
    if (!parsePrimaryExpr(node))
        return false;
    while (tokenIs(T2_LEFTBRACKET) ||
           tokenIs(T2_LEFTPAREN) ||
           tokenIs(T2_DOT) ||
           tokenIs(T2_PLUSPLUS) ||
           tokenIs(T2_MINUSMINUS)) {
        ast_branch &parent = CreateSwapLast(node);
        switch (currentID()) {
            case T2_LEFTBRACKET: {
                advance();
                parent.op = ast_op_type::Index;
                if (!parseExpr(parent))
                    throwParseError("parenthesized-expr parser failed");
                eatToken(T2_RIGHTBRACKET, "indexer closing parenthesis expected");
                // type -> extract array layer
                indexType(parent);
                break;
            }
            case T2_LEFTPAREN: {
                advance();
                parent.op = ast_op_type::Call;
                parsePostfix_Params(parent);
                // type -> search for function type
                callType(parent);
                break;
            }
            case T2_DOT: {
                advance();
                parent.op = ast_op_type::Member;
                parsePostfix_Member(parent);
                // type -> search for type and members
                memberType(parent);
                break;
            }
            case T2_PLUSPLUS: {
                advance();
                parent.op = ast_op_type::Increment_Post;
                // type -> same
                parent.resultType = PtrCreate<type>(parent.lastChild().getType());
                break;
            }
            case T2_MINUSMINUS: {
                advance();
                parent.op = ast_op_type::Decrement_Post;
                // type -> same
                parent.resultType = PtrCreate<type>(parent.lastChild().getType());
                break;
            }
            default:
                return false;
        }
    }
    return true;
}

bool nxc::parser::parsePrimaryExpr(ast_branch &node) {
    if (tokenIs(T2_IDENTIFIER)) {
        ast_value &val = CreatePushReturn<ast_value>(node.children, ast_value_type::Identifier, currentValue());
        val.resultType = PtrCreate<type>();
        sym.identifierType(val.value, *val.resultType, val.annotation);

    } else if (tokenIs(T2_LEFTPAREN)) {
        advance();
        if (!parseExpr(node))
            return false;
        eatToken(T2_RIGHTPAREN, "sub-expr closing parenthesis expected");
        return true;

    } else if (tokenIsType(T2_BoolLiteralTokenType)) {
        ast_value &val = CreatePushReturn<ast_value>(node.children, ast_value_type::Constant_Boolean, currentValue());
        val.resultType = PtrCreate<type>(basic_type::bool_type);

    } else if (tokenIsType(T2_FloatingLiteralTokenType)) {
        ast_value &val = CreatePushReturn<ast_value>(node.children, ast_value_type::Constant_Float, currentValue());
        val.resultType = PtrCreate<type>(basic_type::float_type);


    } else if (tokenIsType(T2_IntegerLiteralTokenType)) {
        ast_value &val = CreatePushReturn<ast_value>(node.children, ast_value_type::Constant_Integer, currentValue());
        val.resultType = PtrCreate<type>(sym.getIntegerType(val));
    } else if (tokenIsType(T2_CharacterLiteralTokenType)) {
        ast_value &val = CreatePushReturn<ast_value>(node.children, ast_value_type::Constant_Character, currentValue());
        val.resultType = PtrCreate<type>(basic_type::schar_type);


    } else if (tokenIsType(T2_StringLiteralTokenType)) {
        ast_value &val = CreatePushReturn<ast_value>(node.children, ast_value_type::String, currentValue());
        val.resultType = PtrCreate<type>(basic_type::string_type);


    } else {
        return false;
    }
    advance();
    return true;
}

void nxc::parser::parsePostfix_Params(ast_branch &node) {
    while (!tokenIs(T2_RIGHTPAREN)) {
        if (!parseAssignmentExpr(node))
            throwParseError("cannot parse arguments");
        if (tokenIs(T2_RIGHTPAREN))
            break;
        eatToken(T2_COMMA, "comma between arguments expected");
    }
    advance();
}

void nxc::parser::parsePostfix_Member(ast_branch &node) {
    requireToken(T2_IDENTIFIER, "member name expected");
    ast_value &val = CreatePushReturn<ast_value>(node.children);
    val.type = ast_value_type::Identifier;
    val.value = currentValue();
    advance();
}

bool nxc::parser::parseCast(nxc::ast_branch &node) {
    if (!tokenIs(T2_LEFTPAREN))
        return false;
    advance();

    nxc::ast_branch &it = CreatePushReturn<ast_branch>(node.children, ast_op_type::Cast);
    nxc::ast_typename &tname = CreatePushReturn<ast_typename>(it.children);

    type_name name;
    if (!parseTypeName(name))
        throwParseError("cannot parse cast destination type");
    type &ttype = PtrCreateAssignReturn(tname.name);
    type &ctype = PtrCreateAssignReturn(it.resultType);
    sym.resolveTypeName(name, ttype);
    ctype = ttype;

    eatToken(T2_RIGHTPAREN, "cast closing parenthesis expected");
    return parseCastExpr(it);
}

bool nxc::parser::parseEqualityExpr(nxc::ast_branch &node) {
    auto predicate = [this]() {
        return tokenIs(T2_EQUAL) ||
               tokenIs(T2_NOTEQUAL);
    };
    auto identify = [this]() {
        return tokenIs(T2_EQUAL) ? ast_op_type::Equality_Equal
                                 : ast_op_type::Equality_Notequal;
    };
    return parseMultiOp_TBool<&parser::parseRelationExpr>(node, predicate, identify, "equality-expr parser failed");
}

bool nxc::parser::parseRelationExpr(nxc::ast_branch &node) {
    auto predicate = [this]() {
        return tokenIs(T2_GREATER) ||
               tokenIs(T2_GREATEREQUAL) ||
               tokenIs(T2_LESS) ||
               tokenIs(T2_LESSEQUAL);
    };
    auto identify = [this]() {
        return tokenIs(T2_GREATER) ? ast_op_type::Relation_Greater :
               tokenIs(T2_GREATEREQUAL) ? ast_op_type::Relation_GreaterEqual :
               tokenIs(T2_LESS) ? ast_op_type::Relation_Less
                                : ast_op_type::Relation_LessEqual;
    };
    return parseMultiOp_TBool<&parser::parseShiftExpr>(node, predicate, identify, "relation-expr parser failed");
}

bool nxc::parser::parseShiftExpr(nxc::ast_branch &node) {
    auto predicate = [this]() {
        return tokenIs(T2_SHIFTLEFT) ||
               tokenIs(T2_SHIFTRIGHT);
    };
    auto identify = [this]() {
        return tokenIs(T2_SHIFTLEFT) ? ast_op_type::Shift_Left
                                     : ast_op_type::Shift_Right;
    };
    return parseMultiOp_TNum<&parser::parseAddExpr>(node, predicate, identify, "shift-expr parser failed");
}

bool nxc::parser::parseAddExpr(nxc::ast_branch &node) {
    auto predicate = [this]() {
        return tokenIs(T2_PLUS) ||
               tokenIs(T2_MINUS);
    };
    auto identify = [this]() {
        return tokenIs(T2_PLUS) ? ast_op_type::Addition_Add
                                : ast_op_type::Addition_Subtract;
    };
    return parseMultiOp_TNum<&parser::parseMultiplyExpr>(node, predicate, identify, "add-expr parser failed");
}

bool nxc::parser::parseMultiplyExpr(nxc::ast_branch &node) {
    auto predicate = [this]() {
        return tokenIs(T2_STAR) ||
               tokenIs(T2_DIVIDE) ||
               tokenIs(T2_PERCENT);
    };
    auto identify = [this]() {
        return tokenIs(T2_STAR) ? ast_op_type::Multiplication_Multiply :
               tokenIs(T2_DIVIDE) ? ast_op_type::Multiplication_Divide
                                  : ast_op_type::Multiplication_Modulo;
    };
    return parseMultiOp_TNum<&parser::parseCastExpr>(node, predicate, identify, "multiply-expr parser failed");
}
