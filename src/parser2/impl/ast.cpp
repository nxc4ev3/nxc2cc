/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Implementation of various AST helper functions.
 */


#include <parser2/include/ast.hpp>
#include <global/include/symbols.hpp>


nxc::FunctionType nxc::efuncinfo::getType(nxc::symbol_table &tbl) const {
    switch (tbl.which(this->id)) {
        case nxc::sym_class::VARIABLE:
        case nxc::sym_class::SCOPE:
        case nxc::sym_class::TYPEDEF:
        case nxc::sym_class::ENUM:
        case nxc::sym_class::STRUCT:
        case nxc::sym_class::ERROR:
            throw std::logic_error("bad efuncinfo query type");
        case nxc::sym_class::TASK:
            return nxc::FunctionType::Task;
        case nxc::sym_class::FUNCTION:
            return nxc::FunctionType::Function;
    }
}


nxc::ast_typename::ast_typename(const nxc::type &t)
        : ast_node(ast_node_type::Typename) {
    name = PtrCreate<type>(t);
}


std::string &nxc::getFunctionName(nxc::ast_branch &call) {
    if (call.op != ast_op_type::Call)
        throw std::logic_error("querying function name on non-call");
    return static_cast<ast_value &>(call.childAt(0)).value;
}
const std::string &nxc::getFunctionName(const nxc::ast_branch &call) {
    if (call.op != ast_op_type::Call)
        throw std::logic_error("querying function name on non-call");
    return static_cast<const ast_value &>(call.childAt(0)).value;
}

std::vector<nxc::type> nxc::getFunctionArgTypes(const nxc::ast_branch &call) {
    if (call.op != ast_op_type::Call)
        throw std::logic_error("querying function name on non-call");

    std::vector<nxc::type> result;

    auto it = call.children.begin() + 1;
    auto end = call.children.end();
    for (; it != end; ++it) {
        result.push_back((*it)->getType());
    }

    return result;
}

nxc::ast_op_priority nxc::getPriority(nxc::ast_op_type op) {
    switch (op) {
        case ast_op_type::Passthrough:
        case ast_op_type::Comma:
            return ast_op_priority::Comma;
        case ast_op_type::Assign_Only:
        case ast_op_type::Assign_Multiply:
        case ast_op_type::Assign_Divide:
        case ast_op_type::Assign_Modulo:
        case ast_op_type::Assign_Add:
        case ast_op_type::Assign_Subtract:
        case ast_op_type::Assign_Leftshift:
        case ast_op_type::Assign_Rightshift:
        case ast_op_type::Assign_BitAnd:
        case ast_op_type::Assign_BitXor:
        case ast_op_type::Assign_BitOr:
        case ast_op_type::Assign_Abs:
        case ast_op_type::Assign_Sign:
            return ast_op_priority::Assignment;
        case ast_op_type::Ternary:
            return ast_op_priority::Ternary;
        case ast_op_type::LogicOr:
            return ast_op_priority::LogicOr;
        case ast_op_type::LogicAnd:
            return ast_op_priority::LogicAnd;
        case ast_op_type::BitOr:
            return ast_op_priority::BitOr;
        case ast_op_type::BitXor:
            return ast_op_priority::BitXor;
        case ast_op_type::BitAnd:
            return ast_op_priority::BitAnd;
        case ast_op_type::Equality_Equal:
        case ast_op_type::Equality_Notequal:
            return ast_op_priority::Equality;
        case ast_op_type::Relation_LessEqual:
        case ast_op_type::Relation_Less:
        case ast_op_type::Relation_Greater:
        case ast_op_type::Relation_GreaterEqual:
            return ast_op_priority::Compare;
        case ast_op_type::Shift_Left:
        case ast_op_type::Shift_Right:
            return ast_op_priority::Shift;
        case ast_op_type::Addition_Add:
        case ast_op_type::Addition_Subtract:
            return ast_op_priority::Addition;
        case ast_op_type::Multiplication_Multiply:
        case ast_op_type::Multiplication_Divide:
        case ast_op_type::Multiplication_Modulo:
            return ast_op_priority::Multiplication;
        case ast_op_type::Cast:
        case ast_op_type::Sizeof_Type:
        case ast_op_type::Sizeof_Expr:
        case ast_op_type::Unary_Plus:
        case ast_op_type::Unary_Minus:
        case ast_op_type::Unary_Complement:
        case ast_op_type::Unary_Negate:
        case ast_op_type::Increment_Pre:
        case ast_op_type::Decrement_Pre:
            return ast_op_priority::Prefixes;
        case ast_op_type::Increment_Post:
        case ast_op_type::Decrement_Post:
        case ast_op_type::Index:
        case ast_op_type::Member:
        case ast_op_type::Call:
            return ast_op_priority::Postfixes;
    }
}

nxc::ast_typename::ast_typename(const nxc::ast_typename &other)
        : ast_node(other.nodeType) {
    this->annotation = other.annotation;
    this->location = other.location;
    if (other.name)
        this->name = PtrCreate<nxc::type>(*other.name);
}

nxc::ast_value::ast_value(const nxc::ast_value &other)
        : ast_node(other.nodeType) {
    this->annotation = other.annotation;
    this->location = other.location;
    this->type = other.type;
    this->value = other.value;
    if (other.resultType)
        this->resultType = PtrCreate<nxc::type>(*other.resultType);
}


nxc::ast_branch::ast_branch(const nxc::ast_branch &other)
        : ast_node(other.nodeType) {
    this->annotation = other.annotation;
    this->location = other.location;
    this->op = other.op;
    if (other.resultType)
        this->resultType = PtrCreate<nxc::type>(*other.resultType);
    for (const required_ptr_to<ast_node> &p_child : other.children) {
        ast_node &child = *p_child;
        switch (child.nodeType) {
            case ast_node_type::Multinode: {
                ast_branch &orig = static_cast<ast_branch &>(child);
                this->children.push_back(PtrCreate<ast_branch>(orig));
                break;
            }
            case ast_node_type::Value: {
                ast_value &orig = static_cast<ast_value &>(child);
                this->children.push_back(PtrCreate<ast_value>(orig));
                break;
            }
            case ast_node_type::Typename: {
                ast_typename &orig = static_cast<ast_typename &>(child);
                this->children.push_back(PtrCreate<ast_typename>(orig));
                break;

            }
        }
    }
}


nxc::ast_typename::ast_typename(nxc::ast_typename &&other)
        : ast_node(other.nodeType) {

    this->annotation = other.annotation;
    this->location = other.location;
    this->name = std::move(other.name);
}

nxc::ast_value::ast_value(nxc::ast_value &&other)
        : ast_node(other.nodeType) {
    this->annotation = other.annotation;
    this->location = other.location;
    this->type = other.type;
    this->value = other.value;
    this->resultType = std::move(other.resultType);
}

nxc::ast_branch::ast_branch(nxc::ast_branch &&other)
        : ast_node(other.nodeType) {
    this->annotation = other.annotation;
    this->location = other.location;
    this->op = other.op;
    this->resultType = std::move(other.resultType);
    this->children = std::move(other.children);
}

nxc::initializer::initializer()
        : type(InitializerType::None), ptr(nullptr) {}

nxc::initializer::initializer(const initializer &other) {
    *this = other;
}

nxc::initializer::initializer(initializer &&other) {
    *this = std::move(other);
}

nxc::initializer &nxc::initializer::operator=(const initializer &other) {
    this->type = other.type;
    if (type == InitializerType::None) {
        ptr = nullptr;
    } else if (type == InitializerType::Expression) {
        initializer_expr copy = PtrCastRef<initializer_expr>(other.ptr);
        ptr = PtrCreate<initializer_expr>(std::move(copy));
    } else if (type == InitializerType::Array) {
        initializer_array copy = PtrCastRef<initializer_array>(other.ptr);
        ptr = PtrCreate<initializer_array>(std::move(copy));
    }
    return *this;
}

nxc::initializer &nxc::initializer::operator=(initializer &&other) {
    this->type = other.type;
    this->ptr = std::move(other.ptr);
    other.type = InitializerType::None;
    return *this;
}


nxc::initializer_expr::initializer_expr() {
    expr = nullptr;
}

nxc::initializer_expr::initializer_expr(const initializer_expr &other) {
    *this = other;
}

nxc::initializer_expr::initializer_expr(initializer_expr &&other) {
    *this = std::move(other);
}

nxc::initializer_expr &nxc::initializer_expr::operator=(const initializer_expr &other) {
    this->location = other.location;
    if (other.expr) {
        ast_node &ref = *other.expr;
        switch(ref.nodeType) {
            case ast_node_type::Multinode:
                this->expr = PtrCreate<ast_branch>(static_cast<ast_branch&>(ref));
                break;
            case ast_node_type::Value:
                this->expr = PtrCreate<ast_value>(static_cast<ast_value&>(ref));
                break;
            case ast_node_type::Typename:
                this->expr = PtrCreate<ast_typename>(static_cast<ast_typename&>(ref));
                break;
        }
    }
    return *this;
}

nxc::initializer_expr &nxc::initializer_expr::operator=(initializer_expr &&other) {
    this->location = std::move(other.location);
    this->expr = std::move(other.expr);
    return *this;
}
