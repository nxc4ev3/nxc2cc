/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * MXC declaration, task and function parser.
 */


#include <parser2/include/private/parser.hpp>
#include <parser2/include/ast/declaration.raw.hpp>


void nxc::parser::parseFile(nxc::translation_unit &ref) {
    while (it != end) {
        if (tokenIs(T2_SEMICOLON)) {
            advance();
            continue;
        }

        CREATE_BACKUP(bkp);
        if (parseTask(ref))
            continue;
        else
            RESTORE_BACKUP(bkp);

        if (parseFunction(ref))
            continue;
        else
            RESTORE_BACKUP(bkp);

        if (parseDeclaration(ref))
            continue;
        else
            throwParseError("task/function/declaration parsing failed");
    }
}

bool nxc::parser::parseTask(nxc::translation_unit &unit) {
    // check for task keyword
    if (!tokenIs(T2_TASK))
        return false;
    advance();

    // allocate structure
    auto task = PtrCreate<efuncinfo>();

    // get name
    requireToken(T2_IDENTIFIER, "task name expected");
    std::string name = currentValue();
    task->location = currentPosition();
    advance();

    // check for parentheses
    eatToken(T2_LEFTPAREN, "opening argument parenthesis expected");
    if (tokenIs(T2_VOID))
        advance();
    eatToken(T2_RIGHTPAREN, "closing argument parenthesis expected");

    // check for code block or semicolon
    task_data &info = sym.registerTask(*task, name);
    if (tokenIs(T2_LEFTBRACE)) {
        task->code = PtrCreate<statement>();

        sym.beginTaskScope(info, currentPosition());
        parseStatement_Compound(*task->code);
        sym.endTaskScope();
    } else {
        eatToken(T2_SEMICOLON, "semicolon after task declaration expected");
    }
    // move ownership to parent structure
    extdeclaration ref;
    PtrAssignCast(ref.pointer, task);
    ref.type = extdeclaration_type::Function;
    unit.declarations.push_back(std::move(ref));
    return true;
}

nxc::FunctionModifierMask nxc::parser::parseFunctionModifiers() {
    FunctionModifierMask mask = FunctionModifierMask::None;
    while (true) {
        if (tokenIs(T2_SAFECALL)) {
            mask |= FunctionModifierMask::Safecall;
            advance();
            continue;
        } else if (tokenIs(T2_INLINE)) {
            mask |= FunctionModifierMask::Inline;
            advance();
            continue;
        } else {
            break;
        }
    }
    return mask;
}

void nxc::parser::parseParameters(nxc::list_of<nxc::required_ptr_to<nxc::parameter>> &params) {
    if (tokenIs(T2_RIGHTPAREN))
        return;
    while (true) {
        auto p_param = PtrCreate<parameter>();
        parseParameter(*p_param);
        PtrPush(params, p_param);
        if (tokenIs(T2_RIGHTPAREN))
            break;
        eatToken(T2_COMMA, "comma between parameters expected");
    }
}

void nxc::parser::parseParameter(nxc::parameter &params) {
    params.decl.decl.location = currentPosition();
    if (!parseConstRawtype(params.type)) {
        throwParseError("cannot parse argument type");
    }
    if (tokenIs(T2_AND)) {
        params.reference = true;
        advance();
    }
    if (tokenIs(T2_IDENTIFIER)) {
        params.decl.decl.name = currentValue();
        advance();
    } else {
        params.decl.decl.name = "";
    }
    parseArrays(params.decl.decl.arrays);
    if (tokenIs(T2_ASSIGN)) {
        advance();
        params.decl.value = PtrCreate<nxc::initializer>();
        parseInitializer(*params.decl.value);
    }
}


bool nxc::parser::parseFunction_Type(nxc::optional_ptr_to<nxc::type_name> &out) {

    if (tokenIs(T2_VOID) || tokenIs(T2_SUB)) {

        out = nullptr;
        advance();
        return true;

    } else { // if this is a function

        // allocate return type
        out = PtrCreate<type_name>();

        // try parsing return type
        return parseConstRawtype(out->type);
    }
}

bool nxc::parser::parseFunction_Name(std::string &name, position_type &location) {
    if (!tokenIs(T2_IDENTIFIER))
        return false;
    name = currentValue();
    location = currentPosition();
    advance();
    return true;
}

bool nxc::parser::parseFunction_Params(nxc::list_of<nxc::required_ptr_to<parameter>> &params) {
    if (!tokenIs(T2_LEFTPAREN)) {
        return false;
    }
    // now we know for sure this is a function
    advance();

    if (tokenIs(T2_VOID)) {
        advance();
    } else {
        // parse parameters
        parseParameters(params);
    }
    eatToken(T2_RIGHTPAREN, "closing argument parenthesis expected");
    return true;
}

bool nxc::parser::parseFunction(nxc::translation_unit &unit) {
    // is this really a function?
    bool confirmed = false;

    // allocate structure(s)
    auto function = PtrCreate<efuncinfo>();

    std::string name;
    FunctionModifierMask modifiers;
    list_of<required_ptr_to<parameter>> params;
    optional_ptr_to<type_name> retval;


    // load modifiers
    modifiers = parseFunctionModifiers();
    // update sureness
    confirmed |= modifiers != FunctionModifierMask::None;

    // if this is a sub
    if (parseFunction_Type(retval)) {
        confirmed |= retval == nullptr;
    } else {
        if (confirmed) {
            throwMalformedDeclaration("cannot parse function return type");
        } else {
            return false;
        }
    }

    // parse function name
    if (!parseFunction_Name(name, function->location)) {
        if (confirmed) {
            throwMalformedDeclaration("cannot parse function name");
        } else {
            return false;
        }
    }

    if (!parseFunction_Params(params))
        return false;

    if (retval)
        parseArrays(retval->arrays);
    // parse code

    list_of<required_ptr_to<efuncinfo>> functions;
    func_data &info = sym.registerFunction(*function, name, modifiers, params, retval, functions);
    if (tokenIs(T2_LEFTBRACE)) {
        function->code = PtrCreate<statement>();

        sym.beginFunctionScope(info, currentPosition());
        parseStatement_Compound(*function->code);
        sym.endFunctionScope();
    } else {
        eatToken(T2_SEMICOLON, "semicolon after function declaration expected");
    }

    functions.insert(functions.begin(), std::move(function));
    for (required_ptr_to<efuncinfo> &infos : functions) {
        extdeclaration container;
        container.type = extdeclaration_type::Function;
        PtrAssignCast(container.pointer, infos);
        unit.declarations.push_back(std::move(container));
    }
    return true;
}


nxc::parse_result nxc::parse(parser::iterator_type begin, parser::iterator_type end) {
    nxc::parse_result result;
    nxc::parser parser(begin, end, result.syms);
    parser.parseFile(result.tree);
    return result;
}
