/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * NXC statement parser.
 */


#include <parser2/include/private/parser.hpp>


bool nxc::parser::parseStatement(nxc::statement &ref) {
    ref.location = currentPosition();
    switch (currentID()) {
        case T2_LEFTBRACE: {
            sym.pushScope();
            bool success = parseStatement_Compound(ref);
            sym.popScope();
            return success;
        }
        case T2_CONTINUE:
        case T2_BREAK:
        case T2_GOTO:
        case T2_RETURN:
            return parseStatement_Jump(ref);
        case T2_START:
        case T2_STOP:
        case T2_PRIORITY:
            return parseStatement_Thread(ref);
        case T2_CASE:
        case T2_DEFAULT:
            return parseStatement_LabelKwd(ref);
        case T2_IF:
        case T2_SWITCH:
            return parseStatement_Select(ref);
        case T2_DO:
        case T2_FOR:
        case T2_WHILE:
        case T2_UNTIL:
        case T2_REPEAT:
            return parseStatement_Iteration(ref);
        default:
            CREATE_BACKUP(bkp);
            if (!parseStatement_LabelIdentifier(ref)) {
                RESTORE_BACKUP(bkp);
                return parseStatement_Expr(ref);
            }
            return true;
    }
}

bool nxc::parser::parseStatement_Expr(nxc::statement &ref) {
    ref.type = StatementType::Expression;
    expression_statement &stmt = PtrCreateCastAssignReturn<expression_statement>(ref.stmt);
    stmt.expression = PtrCreate<ast_branch>();
    if (tokenIs(T2_SEMICOLON)) {
        advance();
        return true;
    }
    if (!parseExpr(*stmt.expression)) {
        return false;
    }
    eatToken(T2_SEMICOLON, "semicolon after statement expected");
    return true;
}

bool nxc::parser::parseStatement_Compound(nxc::statement &ref) {
    eatToken(T2_LEFTBRACE, "compound statement opening brace expected");
    ref.type = StatementType::Compound;

    compound_statement &data = PtrCreateCastAssignReturn<compound_statement>(ref.stmt);
    data.id = sym.currentScope();

    while (!tokenIs(T2_RIGHTBRACE)) {
        nxc::expression_declaration stmt;
        parseStatement_StmtDecl(stmt);
        data.statements.push_back(std::move(stmt));
    }
    advance();
    return true;
}

bool nxc::parser::parseStatement_Jump(nxc::statement &ref) {
    switch (currentID()) {
        case T2_CONTINUE:
            ref.type = StatementType::Jump_Continue;
            break;
        case T2_BREAK:
            ref.type = StatementType::Jump_Break;
            break;
        case T2_GOTO: {
            ref.type = StatementType::Jump_Goto;
            jump_to &jump = PtrCreateCastAssignReturn<jump_to>(ref.stmt);
            advance();
            requireToken(T2_IDENTIFIER, "label name expected");
            jump.label = currentValue();
            break;
        }
        case T2_RETURN: {
            advance();
            if (tokenIs(T2_SEMICOLON)) {
                advance();
                ref.type = StatementType::Jump_Return;
                ref.stmt = nullptr;
                return true;
            }
            if (!parseStatement_Expr(ref))
                throwParseError("cannot parse return value");
            ref.type = StatementType::Jump_Return;
            return true;
        }
    }
    advance();
    eatToken(T2_SEMICOLON, "semicolon after statement expected");
    return true;
}

bool nxc::parser::parseStatement_Thread(nxc::statement &ref) {
    switch (currentID()) {
        case T2_START:
            ref.type = StatementType::Thread_Start;
            break;
        case T2_STOP:
            ref.type = StatementType::Thread_Stop;
            break;
        case T2_PRIORITY: {
            ref.type = StatementType::Thread_Priority;
            thread_priority &cmd = PtrCreateCastAssignReturn<thread_priority>(ref.stmt);
            cmd.priority = PtrCreate<ast_branch>();

            advance();

            requireToken(T2_IDENTIFIER, "task name expected");
            cmd.name = currentValue();
            advance();

            eatToken(T2_COMMA, "comma expected");

            if (!parseConstantExpr(*cmd.priority))
                throwParseError("cannot parse thread priority");

            eatToken(T2_SEMICOLON, "semicolon after statement expected");
            return true;
        }
    }

    advance();
    requireToken(T2_IDENTIFIER, "task name expected");
    thread_name &thr = PtrCreateCastAssignReturn<thread_name>(ref.stmt);
    thr.name = currentValue();

    advance();
    eatToken(T2_SEMICOLON, "semicolon after statement expected");

    return true;
}

bool nxc::parser::parseStatement_LabelKwd(nxc::statement &ref) {
    nxc::statement *pointed;

    if (tokenIs(T2_DEFAULT)) {

        ref.type = StatementType::Label_Default;
        pointed = &PtrCreateCastAssignReturn<label_default>(ref.stmt).child;
        advance();

    } else if (tokenIs(T2_CASE)) {

        ref.type = StatementType::Label_Case;
        label_case &label = PtrCreateCastAssignReturn<label_case>(ref.stmt);
        label.value = PtrCreate<ast_branch>();
        pointed = &label.child;
        advance();

        if (!parseConstantExpr(*label.value))
            throwParseError("cannot parse case value");

    } else {
        return false;
    }
    eatToken(T2_COLON, "colon after label expected");
    return parseStatement(*pointed);
}

bool nxc::parser::parseStatement_LabelIdentifier(nxc::statement &ref) {
    ref.type = StatementType::Label_Identifier; // will be overwritten

    if (!tokenIs(T2_IDENTIFIER))
        return false;
    std::string &&name = currentValue();
    advance();

    if (!tokenIs(T2_COLON))
        return false;
    advance();


    label_named &label = PtrCreateCastAssignReturn<label_named>(ref.stmt);
    label.name = std::move(name);

    return parseStatement(label.child);
}

bool nxc::parser::parseStatement_Select(nxc::statement &ref) {
    if (tokenIs(T2_IF)) {

        ref.type = StatementType::Select_IfElse;
        ifelse_statement &ifelse = PtrCreateCastAssignReturn<ifelse_statement>(ref.stmt);
        ifelse.condition = PtrCreate<ast_branch>();

        advance();
        eatToken(T2_LEFTPAREN, "if opening parenthesis expected");
        if (!parseExpr(*ifelse.condition))
            throwParseError("cannot parse if condition");
        eatToken(T2_RIGHTPAREN, "if closing parenthesis expected");
        if (!parseStatement(ifelse.true_case))
            throwParseError("cannot parse if body");

        if (tokenIs(T2_ELSE)) {
            ifelse.false_case = PtrCreate<statement>();

            advance();
            if (!parseStatement(*ifelse.false_case))
                throwParseError("cannot parse else body");

        } else {
            ifelse.false_case = nullptr;
        }

        return true;
    } else if (tokenIs(T2_SWITCH)) {

        ref.type = StatementType::Select_Switch;
        switch_statement &swytch = PtrCreateCastAssignReturn<switch_statement>(ref.stmt);
        swytch.value = PtrCreate<ast_branch>();

        advance();
        eatToken(T2_LEFTPAREN, "switch opening parenthesis expected");
        if (!parseExpr(*swytch.value))
            throwParseError("cannot parse switch value");
        eatToken(T2_RIGHTPAREN, "switch closing parenthesis expected");

        return parseStatement(swytch.body);
    } else {
        return false;
    }
}

bool nxc::parser::parseStatement_Iteration(statement &ref) {
    switch (currentID()) {
        case T2_FOR: {
            ref.type = StatementType::Iteration_For;
            for_loop_info &info = PtrCreateCastAssignReturn<for_loop_info>(ref.stmt);

            advance();
            eatToken(T2_LEFTPAREN, "for opening parenthesis expected");
            if (tokenIs(T2_SEMICOLON)) {
                info.initializer = nullptr;
                advance();
            } else {
                info.initializer = PtrCreate<expression_declaration>();
                parseStatement_StmtDecl(*info.initializer);
            }
            if (tokenIs(T2_SEMICOLON)) {
                info.condition = nullptr;
            } else {
                info.condition = PtrCreate<ast_branch>();
                if (!parseExpr(*info.condition))
                    throwParseError("cannot parse for condition");
            }
            eatToken(T2_SEMICOLON, "for semicolon expected");
            if (tokenIs(T2_RIGHTPAREN)) {
                info.update = nullptr;
            } else {
                info.update = PtrCreate<ast_branch>();
                if (!parseExpr(*info.update))
                    throwParseError("cannot parse for update");
            }
            eatToken(T2_RIGHTPAREN, "for closing parenthesis expected");
            return parseStatement(info.body);
        }
        case T2_DO: {
            loop_info &info = PtrCreateCastAssignReturn<loop_info>(ref.stmt);
            info.condition = PtrCreate<ast_branch>();

            advance();
            if (!parseStatement(info.body))
                throwParseError("cannot parse do-while body");

            if(tokenIs(T2_WHILE)){
                ref.type = StatementType::Iteration_DoWhile;
            } else if (tokenIs(T2_UNTIL)) {
                ref.type = StatementType::Iteration_DoUntil;
            } else {
                throwParseError("do-while or do-until expected");
            }
            advance();
            eatToken(T2_LEFTPAREN, "do-while opening parenthesis expected");
            if (!parseExpr(*info.condition))
                throwParseError("cannot parse do-while condition");
            eatToken(T2_RIGHTPAREN, "do-while closing parenthesis expected");
            eatToken(T2_SEMICOLON, "semicolon after do-while expected");
            return true;
        }
        case T2_WHILE:
            ref.type = StatementType::Iteration_While;
            break;
        case T2_UNTIL:
            ref.type = StatementType::Iteration_Until;
            break;
        case T2_REPEAT:
            ref.type = StatementType::Iteration_Repeat;
            break;
        default:
            return false;
    }

    loop_info &info = PtrCreateCastAssignReturn<loop_info>(ref.stmt);
    info.condition = PtrCreate<ast_branch>();

    advance();
    eatToken(T2_LEFTPAREN, "loop opening parenthesis expected");
    if (!parseExpr(*info.condition))
        throwParseError("cannot parse loop value");
    eatToken(T2_RIGHTPAREN, "loop closing parenthesis expected");

    return parseStatement(info.body);
}

void nxc::parser::parseStatement_StmtDecl(nxc::expression_declaration &ref) {
    edeclinfo decl;
    statement stmt;
    CREATE_BACKUP(bkp);
    if (parseDeclaration(decl)) {
        ref.type = ExpressionDeclaration::Declaration;
        in_decl &declRef = PtrCreateCastAssignReturn<in_decl>(ref.ptr);
        declRef.declaration = PtrCreate<edeclinfo>(std::move(decl));
    } else {
        RESTORE_BACKUP(bkp);
        if (parseStatement(stmt)) {
            ref.type = ExpressionDeclaration::Expression;
            in_stmt &stmtRef = PtrCreateCastAssignReturn<in_stmt>(ref.ptr);
            stmtRef.stmt = std::move(stmt);

        } else {
            throwParseError("statement/declaration parse failed");
        }
    }
}
