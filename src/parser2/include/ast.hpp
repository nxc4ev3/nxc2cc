/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Parse structures aggregate header.
 */
 
#ifndef CUSTOM_ID_AST_HPP
#define CUSTOM_ID_AST_HPP

#include <parser2/include/ast/declaration.hpp>
#include <parser2/include/ast/declaration.raw.hpp>
#include <parser2/include/ast/enums.hpp>
#include <parser2/include/ast/expr.hpp>
#include <parser2/include/ast/statement.hpp>
#include <parser2/include/ast/file.hpp>

#endif //CUSTOM_ID_AST_HPP
