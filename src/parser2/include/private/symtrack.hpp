/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Integration of the symbol table with the parser.
 */


#ifndef CUSTOM_ID_SYMTRACK_HPP
#define CUSTOM_ID_SYMTRACK_HPP

#include <parser2/include/ast.hpp>
#include <global/include/symbols.hpp>
#include <string>
#include <parser2/include/type_resolver.hpp>
#include <global/include/function_lookup.hpp>
#include <parser2/include/private/native_iface.hpp>

namespace nxc {

    struct resolved_func {
        boost::optional<nxc::type> f_return_type;
        nxc::func_data::arg_list_type f_args;
    };

    class symtracker {
    public:
        symtracker(symbol_table &syms);

        scope_data &pushScope();

        scope_data &popScope();

        scope_data &currentScope();

        task_data &registerTask(nxc::efuncinfo &structure, const std::string &name);

        size_t checkOptArgs(const std::string &name,
                          const position_type &pos,
                          const list_of<required_ptr_to<parameter>> &params);

        func_data &registerFunction(nxc::efuncinfo &ref,
                                    const std::string &name,
                                    FunctionModifierMask modifiers,
                                    const list_of<required_ptr_to<parameter>> &params,
                                    optional_ptr_to<type_name> &returnType,
                                    list_of<required_ptr_to<efuncinfo>> &auxiliary);

        func_data &registerFunction(nxc::efuncinfo &ref,
                                    const std::string &name,
                                    FunctionModifierMask modifiers,
                                    nxc::func_data::arg_list_type params,
                                    size_t argc,
                                    const boost::optional<nxc::type> &returnType);

        scope_data &beginFunctionScope(func_data &data, const my_pos &pos);

        void endFunctionScope();

        scope_data &beginTaskScope(task_data &data, const my_pos &pos);

        void endTaskScope();

        void registerDeclaration(edeclinfo &decl,
                                 const_rawtype &baseType,
                                 list_of<initializer_declaration> &instances);

        void resolveTypeName(const type_name &name, type &type);

        bool callType(ast_branch &call,
                      const std::vector<type> &argTypes);

        bool memberType(const usertype &base,
                        const std::string &member,
                        type &out,
                        ID &membID);

        bool indexType(nxc::ast_branch &node);

        void identifierType(const std::string &name, type &out, ID &identID);

        static void throwError(const my_pos &position, std::string &&what);

        static void throwError(const my_pos &position, std::string &&what, const std::string &name);

        nxc::basic_type getIntegerType(ast_value &value);

    private:
        nxc::symbol_table &syms;
        nxc::ID currentScopeID;
        nxc::type_resolver gtparse;
        nxc::function_lookup flookup;
        nxc::native_interface natives;


        static bool args_equal(nxc::func_data::arg_list_type const &a,
                               nxc::func_data::arg_list_type const &b);

        nxc::task_data *find_task(const std::string &name);

        nxc::func_data *find_func(const std::string &name,
                                  nxc::func_data::arg_list_type const &args);

        boost::optional<type> resolveFunctionReturn(optional_ptr_to<type_name> &source);

        nxc::func_data::arg_list_type resolveFunctionArgs(
                const list_of<required_ptr_to<parameter>> &source);

        void registerTypedef(instance_info &info,
                             const type &real,
                             const initializer_declaration &instance);

        void registerVar(instance_info &info,
                         const type &real,
                         initializer_declaration &instance,
                         bool is_static);

        void tryEvaluate(nxc::var_data &info, const initializer_declaration &instance);

        void registerFunction_saveArgs(const nxc::list_of<std::unique_ptr<nxc::parameter>> &params,
                                       nxc::func_data::arg_list_type &resolvedParams,
                                       size_t allArgs,
                                       size_t args,
                                       nxc::scope_data &scope,
                                       nxc::compound_statement &compound);
    };

}

#endif //CUSTOM_ID_SYMTRACK_HPP
