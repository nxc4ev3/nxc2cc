/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Essential compiler builtins.
 */


#ifndef NXC4EV3_NATIVES_HPP
#define NXC4EV3_NATIVES_HPP


#include "native_iface.hpp"

namespace nxc {

    class array_stuff : public native_implementor {
    public:
        array_stuff() {}

        virtual bool parseCall(nxc::ast_branch &call,
                               const std::vector<nxc::type> &resolved,
                               nxc::symbol_table &syms) override;

        virtual bool serializeCall(const nxc::ast_branch &call,
                                   const std::vector<nxc::type> &resolved,
                                   const nxc::symbol_table &syms,
                                   ID parent,
                                   serializer &serial,
                                   std::ostream &out) override;

        void ArrayLen(nxc::ast_branch &call,
                      const std::vector<nxc::type> &resolved,
                      nxc::symbol_table &syms);

    protected:

    };

    class precedes : public native_implementor {
    public:
        precedes() {}

        virtual bool parseCall(nxc::ast_branch &call,
                               const std::vector<nxc::type> &resolved,
                               nxc::symbol_table &syms) override;

        virtual bool serializeCall(const nxc::ast_branch &call,
                                   const std::vector<nxc::type> &resolved,
                                   const nxc::symbol_table &syms,
                                   ID parent,
                                   serializer &serial,
                                   std::ostream &out) override;
    };
    class tasks : public native_implementor {

    public:
        tasks() {}

        virtual bool parseCall(nxc::ast_branch &call,
                               const std::vector<nxc::type> &resolved,
                               nxc::symbol_table &syms) override;

        virtual bool serializeCall(const nxc::ast_branch &call,
                                   const std::vector<nxc::type> &resolved,
                                   const nxc::symbol_table &syms,
                                   ID parent,
                                   serializer &serial,
                                   std::ostream &out) override;
    protected:
        std::string getRealName(const std::string &name);
    };
    class cmath : public native_implementor {

    public:
        cmath();

        virtual bool parseCall(nxc::ast_branch &call,
                               const std::vector<nxc::type> &resolved,
                               nxc::symbol_table &syms) override;

        virtual bool serializeCall(const nxc::ast_branch &call,
                                   const std::vector<nxc::type> &resolved,
                                   const nxc::symbol_table &syms,
                                   ID parent,
                                   serializer &serial,
                                   std::ostream &out) override;
    private:
        std::vector<std::pair<int32_t, std::string>> m_info;
    };
}

#endif //NXC4EV3_NATIVES_HPP
