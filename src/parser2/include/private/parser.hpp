/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * NXC parser.
 *  - recursive descent parser integrated with symbol table and scope management.
 *  - inspired by previous attempts with Boost::Spirit V.2-based parser, but much more reliable.
 *  - grammar is based on C89 grammar and was extended to support NXC.
 *  - creates annotated parse structures in one pass (although minor two-passes are necessary).
 *  - allows catching compiler builtin calls.
 * 
 * TODOs:
 *  - pseudo-variadic NXC function support
 *  - compile-time string concatenation
 *  - advanced resource management
 */


#ifndef CUSTOM_ID_IMPL_PARSER_HPP
#define CUSTOM_ID_IMPL_PARSER_HPP

#include <parser2/include/ast.hpp>
#include <interface/parser.hpp>
#include <stdint.h>
#include <global/include/type_combine.hpp>
#include "symtrack.hpp"

#define CREATE_BACKUP(name)  iterator_type name = it
#define RESTORE_BACKUP(name) (it = name)

namespace nxc {
    class parser {
    public:
        typedef nxc::token_iterator iterator_type;

        parser(iterator_type begin, iterator_type end, symbol_table &tbl)
                : it(begin), end(end), sym(tbl), combiner(tbl) {}

        void parseFile(translation_unit &ref);


    protected:
        //
        // TASKS
        //
        bool parseTask(nxc::translation_unit &unit);

        //
        // FUNCTIONS
        //
        bool parseFunction(nxc::translation_unit &unit);

        bool parseFunction_Type(nxc::optional_ptr_to<nxc::type_name> &out);

        bool parseFunction_Name(std::string &name, position_type &location);

        bool parseFunction_Params(nxc::list_of<nxc::required_ptr_to<parameter>> &params);

        void parseParameters(list_of<required_ptr_to<parameter>> &params);

        void parseParameter(parameter &param);

        FunctionModifierMask parseFunctionModifiers();

        //
        // DECLARATIONS
        //
        bool parseDeclaration(translation_unit &unit);

        bool parseDeclaration(edeclinfo &ref);

        bool parseConstRawtype(const_rawtype &ref);

        bool parseRawtype(rawtype &ref);

        bool parseTypeName(type_name &ref);

        //
        // DECLARATORS
        //
        bool parseDeclarationInitializerList(std::vector<initializer_declaration> &ref);

        bool parseDeclarationInitializer(initializer_declaration &init);

        bool parseInitializer(initializer &ref);

        bool parseDeclaratorList(std::vector<declarator> &ref);

        bool parseDeclarator(declarator &decl);

        void parseArrays(list_of<declarator_array> &arrays);


        //
        // STRUCTS
        //
        bool parseStruct(struct_info &ref);

        void parseStruct_Members(std::vector<struct_declaration> &members);

        void parseStruct_Member(struct_declaration &member);

        //
        // ENUMS
        //
        bool parseEnum(enum_info &ref);

        void parseEnumerators(std::vector<enumerator> &ref);

        void parseEnumerator(enumerator &ref);

        //
        // STATEMENTS
        //
        bool parseStatement(statement &ref);

        bool parseStatement_Expr(statement &ref);

        bool parseStatement_Compound(statement &ref);

        bool parseStatement_Jump(statement &ref);

        bool parseStatement_Thread(statement &ref);

        bool parseStatement_LabelKwd(statement &ref);

        bool parseStatement_LabelIdentifier(statement &ref);

        bool parseStatement_Select(statement &ref);

        bool parseStatement_Iteration(statement &ref);

        void parseStatement_StmtDecl(expression_declaration &ref);

        //
        // EXPRESSIONS & AST
        //
        inline bool parseExpr(ast_node &node) {
            return parseExpr(static_cast<ast_branch &>(node));
        }

        inline bool parseAssignmentExpr(ast_node &node) {
            return parseAssignmentExpr(static_cast<ast_branch &>(node));
        }

        inline bool parseConstantExpr(ast_node &node) {
            return parseTernaryExpr(static_cast<ast_branch &>(node));
        }

        inline bool parseConstantExpr(ast_branch &node) {
            return parseTernaryExpr(node);
        }

        bool parseExpr(ast_branch &node);

        bool parseAssignmentExpr(ast_branch &node);

        bool parseTernaryExpr(ast_branch &node);

        bool parseTernary(ast_branch &node);

        bool parseOrExpr(ast_branch &node);

        bool parseAndExpr(ast_branch &node);

        bool parseBitOrExpr(ast_branch &node);

        bool parseBitXorExpr(ast_branch &node);

        bool parseBitAndExpr(ast_branch &node);

        bool parseEqualityExpr(ast_branch &node);

        bool parseRelationExpr(ast_branch &node);

        bool parseShiftExpr(ast_branch &node);

        bool parseAddExpr(ast_branch &node);

        bool parseMultiplyExpr(ast_branch &node);

        bool parseCastExpr(ast_branch &node);

        bool parseCast(ast_branch &node);

        bool parseUnaryExpr(ast_branch &node);

        bool parseSizeof(ast_branch &node);

        bool parsePreStuff(ast_branch &node);

        bool parseUnaryOp(ast_branch &node);

        bool parsePostfixExpr(ast_branch &node);

        void parsePostfix_Params(ast_branch &node);

        void parsePostfix_Member(ast_branch &node);

        bool parsePrimaryExpr(ast_branch &node);

        typedef bool (nxc::parser::*parser_fn)(ast_branch &node);

        template<parser_fn next, tokenID_type thisID, ast_op_type thisType>
        bool parseSingleOp_TLast(ast_branch &node, const char *message);

        template<parser_fn next, tokenID_type thisID, ast_op_type thisType>
        bool parseSingleOp_TMerge(ast_branch &node, const char *message);

        template<parser_fn next, typename PredicateFnT, typename CategoryFnT>
        bool parseMultiOp_TNum(ast_branch &parent,
                          PredicateFnT haveNext,
                          CategoryFnT identify,
                          const char *message);

        template<parser_fn next, typename PredicateFnT, typename CategoryFnT>
        bool parseMultiOp_TBool(ast_branch &parent,
                          PredicateFnT haveNext,
                          CategoryFnT identify,
                          const char *message);

        bool getAssignmentOp(ast_op_type &out);

        void indexType(ast_branch &node);

        void callType(ast_branch &node);

        void memberType(ast_branch &node);

    private:
        inline void checkBounds() const {
            if (it == end)
                throw std::logic_error("Token access after end of list");
        }
        inline tokenID_type currentID() const {
            checkBounds();
            /*
            tokenID_type result = getID(*it);
            auto bla_str = get_token_name(result);
            std::string str(bla_str.begin(), bla_str.end());
            std::cerr << str << std::endl;
            std::cerr.flush();
            */
            return getID(*it);
        }

        inline value_type currentValue() const {
            checkBounds();
            return getValue(*it);
        }

        inline position_type currentPosition() const {
            checkBounds();
            return getPosition(*it);
        }


        inline void requireToken(tokenID_type type, const char *error) const {
            if (!tokenMatchesID(currentID(), type)) {
                throwUnexpectedToken(error, type);
            }
        }

        inline void eatToken(tokenID_type type, const char *error) {
            if (!tokenMatchesID(currentID(), type)) {
                throwUnexpectedToken(error, type);
            }
            advance();
        }

        inline bool tokenIs(tokenID_type type) const {
            return tokenMatchesID(currentID(), type);
        }

        inline bool tokenIsType(tokenCat_type type) const {
            return tokenMatchesCategory(currentID(), type, T2_TokenTypeMask);
        }

        inline void throwSyntaxError(const char *desc) const {
            throw nxc::syntax_error(getLine(*it),
                                    getColumn(*it),
                                    getFile(*it),
                                    currentValue(),
                                    (uint32_t) currentID(),
                                    desc);
        }

        inline void throwUnexpectedToken(const char *desc, tokenID_type expected) const {
            throw nxc::unexpected_token(getLine(*it),
                                        getColumn(*it),
                                        getFile(*it),
                                        currentValue(),
                                        (uint32_t) currentID(),
                                        (uint32_t) expected,
                                        desc);
        }

        inline void throwSemanticError(const char *desc) const {
            throw nxc::semantic_error(currentPosition(),
                                      desc);
        }
        inline void throwParseError(const char *desc) const {
            throw nxc::parsing_failed(getLine(*it),
                                      getColumn(*it),
                                      getFile(*it),
                                      currentValue(),
                                      (uint32_t) currentID(),
                                      desc);
        }

        inline void throwMalformedDeclaration(const char *desc) const {
            throw nxc::malformed_declaration(getLine(*it),
                                             getColumn(*it),
                                             getFile(*it),
                                             currentValue(),
                                             (uint32_t) currentID(),
                                             desc);
        }

        inline void advance() {
            ++it;
        }

        iterator_type it;
        iterator_type end;
        symtracker sym;
        type_combine combiner;
    };
}

#endif //CUSTOM_ID_IMPL_PARSER_HPP
