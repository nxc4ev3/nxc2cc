/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Builtin function filter - custom parsing and serializing.
 */


#ifndef NXC4EV3_NATIVE_IFACE_HPP
#define NXC4EV3_NATIVE_IFACE_HPP

#include <vector>
#include <memory>
#include <global/include/macros.hpp>

namespace nxc {
    struct type;

    class symbol_table;

    struct ast_branch;

    class serializer;

    class native_implementor {
    public:
        virtual ~native_implementor() {}

        virtual bool parseCall(nxc::ast_branch &call,
                               const std::vector<nxc::type> &resolved,
                               nxc::symbol_table &syms) = 0;

        virtual bool serializeCall(const nxc::ast_branch &call,
                                   const std::vector<nxc::type> &resolved,
                                   const nxc::symbol_table &syms,
                                   ID parent,
                                   serializer &serial,
                                   std::ostream &out) = 0;
    };

    class native_interface {
    public:
        native_interface();

        native_interface(std::vector<std::shared_ptr<native_implementor>> last);

        native_interface(std::vector<std::shared_ptr<native_implementor>> first,
                         std::vector<std::shared_ptr<native_implementor>> last);

        void addNative(std::shared_ptr<native_implementor> implementor);

        bool parse(nxc::ast_branch &call,
                   const std::vector<nxc::type> &resolved,
                   nxc::symbol_table &syms);

        bool serialize(const nxc::ast_branch &call,
                       const std::vector<nxc::type> &resolved,
                       const nxc::symbol_table &syms,
                       ID parent,
                       serializer &serializer,
                       std::ostream &out);

    private:
        std::vector<std::shared_ptr<native_implementor>> m_handlers;
    };

}


#endif //NXC4EV3_NATIVE_IFACE_HPP
