/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Declaration parse structures.
 */

#ifndef CUSTOM_ID_DECLARATION_HPP
#define CUSTOM_ID_DECLARATION_HPP

#include <string>
#include <interface/tokens.hpp>
#include <parser2/include/ast/enums.hpp>
#include <parser2/include/ast/pointers.hpp>
#include <global/include/macros.hpp>

namespace nxc {
    /////////////////////
    // PREDECLARATIONS //
    /////////////////////

    // <EXTERNAL>
    struct statement;
    struct ast_node;
    class symbol_table;
    class type;
    // </EXTERNAL>

    // <TENTATIVE>
    struct initializer_expr;
    struct initializer_array;
    // </TENTATIVE>

    // <BASES>
    struct extdeclaration_base {
        virtual ~extdeclaration_base() {}
    };
    struct initializer_base {
        virtual ~initializer_base() {}
    };
    // </BASES>



    /////////////////
    // INITIALIZER //
    /////////////////

    struct initializer {
        initializer();
        initializer(const initializer &other);
        initializer(initializer &&other);
        initializer &operator=(const initializer &other);
        initializer &operator=(initializer &&other);
        InitializerType type;
        required_ptr_to<initializer_base> ptr;
    };

    struct initializer_expr : initializer_base {
        initializer_expr();
        initializer_expr(const initializer_expr &other);
        initializer_expr(initializer_expr &&other);
        initializer_expr &operator=(const initializer_expr &other);
        initializer_expr &operator=(initializer_expr &&other);
        virtual ~initializer_expr() {}
        position_type location;
        required_ptr_to<ast_node> expr;
    };

    struct initializer_array : initializer_base {
        virtual ~initializer_array() {}
        list_of<initializer> fields;
    };

    ////////////////////////////
    // TOP LEVEL DECLARATIONS //
    ////////////////////////////

    struct efuncinfo : extdeclaration_base {
        ID id;
        position_type location;
        optional_ptr_to<statement> code;
        FunctionType getType(symbol_table &tbl) const;
    };
    struct instance_info {
        ID id;
        optional_ptr_to<initializer> value;
    };
    struct edeclinfo : extdeclaration_base {
        DeclarationModifier modifier;
        required_ptr_to<type> baseType;
        required_ptr_to<list_of<instance_info>> instances;
    };
}

#endif //CUSTOM_ID_DECLARATION_HPP
