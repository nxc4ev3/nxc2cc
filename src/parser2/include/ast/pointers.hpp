/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Smart pointer templates and utility functions.
 */

#ifndef CUSTOM_ID_ALIASES_HPP
#define CUSTOM_ID_ALIASES_HPP

#include <memory>
#include <vector>
#include <utility>

namespace nxc {
    template<typename T>
    using ptr_to = std::unique_ptr<T>;
    template<typename T>
    using optional_ptr_to = std::unique_ptr<T>;
    template<typename T>
    using required_ptr_to = std::unique_ptr<T>;
    template<typename T>
    using list_of = std::vector<T>;

    template<typename Dst, typename Src>
    inline std::unique_ptr<Dst> PtrCast(std::unique_ptr<Src> &&p) {
        Dst *d = static_cast<Dst *>(p.release());
        return std::unique_ptr<Dst>(d);
    }

    template<typename Dst, typename Src>
    inline std::unique_ptr<Dst> PtrCast(std::unique_ptr<Src> &p) {
        Dst *d = static_cast<Dst *>(p.release());
        return std::unique_ptr<Dst>(d);
    }

    template<typename Dst, typename Src>
    inline Dst &PtrCastRef(std::unique_ptr<Src> &p) {
        return static_cast<Dst &>(*p);
    }

    template<typename Dst, typename Src>
    inline const Dst &PtrCastRef(const std::unique_ptr<Src> &p) {
        return static_cast<Dst &>(*p);
    }

    template<typename T, typename CastT, typename... Args>
    inline std::unique_ptr<CastT> PtrCreateCast(Args &&... args) {
        return std::unique_ptr<CastT>(static_cast<CastT *> (new T(std::forward<Args>(args)...)));
    }

    template<typename T, typename... Args>
    inline std::unique_ptr<T> PtrCreate(Args &&... args) {
        return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
    }

    template<typename T, typename... Args>
    inline T &PtrCreateAssignReturn(std::unique_ptr<T> &ptr, Args &&...args) {
        ptr = PtrCreate<T, Args...>(std::forward<Args>(args)...);
        return *ptr;
    };

    template<typename T, typename Ptr, typename... Args>
    inline T &PtrCreateCastAssignReturn(std::unique_ptr<Ptr> &ptr, Args &&...args) {
        ptr = PtrCreateCast<T, Ptr, Args...>(std::forward<Args>(args)...);
        return static_cast<T &>(*ptr);
    };

    template<typename T>
    inline void PtrAssign(std::unique_ptr<T> &dest, std::unique_ptr<T> &&src) {
        dest = std::move(src);
    }

    template<typename T, typename CastT>
    inline void PtrAssignCast(std::unique_ptr<CastT> &dest, std::unique_ptr<T> &&src) {
        dest = PtrCast<CastT>(std::move(src));
    }

    template<typename T>
    inline void PtrAssign(std::unique_ptr<T> &dest, std::unique_ptr<T> &src) {
        dest = std::move(src);
    }

    template<typename T, typename CastT>
    inline void PtrAssignCast(std::unique_ptr<CastT> &dest, std::unique_ptr<T> &src) {
        dest = PtrCast<CastT>(std::move(src));
    }

    template<typename T>
    inline void PtrPush(std::vector<std::unique_ptr<T>> &dest, std::unique_ptr<T> &src) {
        dest.push_back(std::move(src));
    }

    template<typename T>
    inline void PtrPush(std::vector<std::unique_ptr<T>> &dest, std::unique_ptr<T> &&src) {
        dest.push_back(std::move(src));
    }

    template<typename T, typename CastT>
    inline void PtrPushCast(std::vector<std::unique_ptr<CastT>> &dest, std::unique_ptr<T> &src) {
        dest.push_back(PtrCast<CastT, T>(std::move(src)));
    }

    template<typename T, typename CastT>
    inline void PtrPushCast(std::vector<std::unique_ptr<CastT>> &dest, std::unique_ptr<T> &&src) {
        dest.push_back(PtrCast<CastT, T>(std::move(src)));
    }
}
#endif //CUSTOM_ID_ALIASES_HPP
