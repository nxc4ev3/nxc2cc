/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Translation-unit parse structures.
 */

#ifndef CUSTOM_ID_TRANSLATION_UNIT_HPP
#define CUSTOM_ID_TRANSLATION_UNIT_HPP

#include <memory>
#include <vector>
#include <parser2/include/ast/pointers.hpp>
#include <parser2/include/ast/enums.hpp>

namespace nxc {

    /**
     * External declaration subtype base class (for pointer casting)
     */
    struct extdeclaration_base;
    /**
     * Function declaration
     */
    struct efuncinfo;
    /**
     * Variable/typedef declaration
     */
    struct edeclinfo;

    /**
     * External declaration container (variant)
     */
    struct extdeclaration {
    public:
        /**
         * Declaration type.
         */
        extdeclaration_type type = extdeclaration_type::None;
        /**
         * Pointer to a subclass of extdeclaration_base, according to declaration type above.
         */
        std::unique_ptr<extdeclaration_base> pointer = nullptr;
    };


    /**
     * List of external declarations; basically whole NXC file
     */
    struct translation_unit {
        /**
         * Declaration list
         */
        std::vector<extdeclaration> declarations;
    };
};

#endif //CUSTOM_ID_TRANSLATION_UNIT_HPP
