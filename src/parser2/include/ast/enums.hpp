/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Enumerations for parse structures.
 */

#ifndef CUSTOM_ID_ENUMS_HPP
#define CUSTOM_ID_ENUMS_HPP

#define BITMASK_ENUM(type)                                                       \
    inline constexpr type operator&(type __x, type __y) {                        \
        return static_cast<type>(static_cast<int>(__x) & static_cast<int>(__y)); \
    }                                                                            \
    inline constexpr type operator|(type __x, type __y) {                        \
        return static_cast<type>(static_cast<int>(__x) | static_cast<int>(__y)); \
    }                                                                            \
    inline constexpr type operator^(type __x, type __y) {                        \
        return static_cast<type>(static_cast<int>(__x) ^ static_cast<int>(__y)); \
    }                                                                            \
    inline constexpr type operator~(type __x) {                                  \
        return static_cast<type>(~static_cast<int>(__x));                        \
    }                                                                            \
    inline type& operator&=(type &__x, type __y) {                               \
        __x = __x & __y; return __x;                                             \
    }                                                                            \
    inline type& operator|=(type &__x, type __y) {                               \
        __x = __x | __y; return __x;                                             \
    }                                                                            \
    inline type& operator^=(type &__x, type __y) {                               \
        __x = __x ^ __y; return __x;                                             \
    }

namespace nxc {
    enum class FunctionModifierMask : int {
        None = 0x0,
        Inline = 0x1,
        Safecall = 0x2,
        SafecallInline = 0x1 | 0x2
    };

    BITMASK_ENUM(FunctionModifierMask)

    enum class DeclarationModifier {
        None,
        Typedef,
        Static
    };

    enum class RawType {
        bool_n,

        char_n,
        byte_n,

        short_n,
        int_n,

        long_n,

        float_f,

        string_s,

        mutex_m,

        struct_u,
        enum_u,
        unknown_u
    };

    enum class InitializerType {
        None,
        Expression,
        Array
    };
    enum class StatementType {
        // detail = nullptr_t
        None,
        // detail = expression_statement => expression
        Expression,

        // detail = compound_statement => list_of<statement>
        Compound,

        // detail = nullptr_t
        Jump_Continue,
        // detail = nullptr_t
        Jump_Break,
        // detail = jump_to => std::string
        Jump_Goto,
        // detail = expression_statement => expression
        Jump_Return,

        
        
        // detail = expression_statement => expression
        Mutex_Release,
        // detail = expression_statement => expression
        Mutex_Acquire,
        // detail = thread_name => std::string
        Thread_Start,
        // detail = thread_name => std::string
        Thread_Stop,
        // detail = thread_name => std::string
        Thread_ExitTo,
        // detail = thread_priority => std::string + expression
        Thread_Priority,
        // detail = thread_names => std::vector<std::string>
        Thread_Follows,
        // detail = thread_names => std::vector<std::string>
        Thread_Precedes,

        // detail = label_named => std::string + statement
        Label_Identifier,
        // detail = label_case => expression + statement
        Label_Case,
        // detail = label_default => statement
        Label_Default,

        // detail = ifelse_info => expression + statement + statement
        Select_IfElse,
        // detail = switch_info => expression + compound_statement
        Select_Switch,

        // detail = loop_info => expression + statement
        Iteration_DoWhile,
        Iteration_DoUntil,
        // detail = for_info => expr/decl + expression + expression + statement
        Iteration_For,
        // detail = loop_info => expression + statement
        Iteration_While,
        // detail = loop_info => expression + statement
        Iteration_Until,
        // detail = loop_info => expression + statement
        Iteration_Repeat
    };
    enum class ExpressionDeclaration {
        Expression,
        Declaration
    };
    /**
     * External declaration type
     */
    enum class extdeclaration_type {
        /**
         * The pointer is empty.
         */
                None,
        /**
         * The pointer points to a function declaration.
         */
                Function,
        /**
         * The pointer points to a variable/typedef declaration.
         */
                Declaration
    };
    enum class FunctionType {
        Task, Function
    };

}
#undef BITMASK_ENUM

#endif //CUSTOM_ID_ENUMS_HPP
