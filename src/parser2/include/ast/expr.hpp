/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Expression AST.
 */

#ifndef CUSTOM_ID_EXPR_HPP
#define CUSTOM_ID_EXPR_HPP

#include <parser2/include/ast/pointers.hpp>
#include <interface/tokens.hpp>

namespace nxc {
    // <EXTERNAL>
    struct type;
    // </EXTERNAL>

    enum class ast_op_type {
        Passthrough,
        Comma,

        Assign_Only,
        Assign_Multiply,
        Assign_Divide,
        Assign_Modulo,
        Assign_Add,
        Assign_Subtract,
        Assign_Leftshift,
        Assign_Rightshift,
        Assign_BitAnd,
        Assign_BitXor,
        Assign_BitOr,
        Assign_Abs,
        Assign_Sign,

        Ternary,

        LogicOr,
        LogicAnd,
        BitOr,
        BitXor,
        BitAnd,

        Equality_Equal,
        Equality_Notequal,

        Relation_LessEqual,
        Relation_Less,
        Relation_Greater,
        Relation_GreaterEqual,

        Shift_Left,
        Shift_Right,

        Addition_Add,
        Addition_Subtract,

        Multiplication_Multiply,
        Multiplication_Divide,
        Multiplication_Modulo,

        Cast,

        Sizeof_Type,
        Sizeof_Expr,
        Unary_Plus,
        Unary_Minus,
        Unary_Complement,
        Unary_Negate,
        Increment_Pre,
        Decrement_Pre,

        Increment_Post,
        Decrement_Post,
        Index,
        Member,
        Call
    };
    enum class ast_value_type {
        Identifier,
        String,
        Constant_Boolean,
        Constant_Integer,
        Constant_Float,
        Constant_Character
    };
    enum class ast_node_type {
        Multinode, Value, Typename
    };
    enum class ast_op_priority {
        Comma = 15,
        Assignment = 14,
        Ternary = 13,
        LogicOr = 12,
        LogicAnd = 11,
        BitOr = 10,
        BitXor = 9,
        BitAnd = 8,
        Equality = 7,
        Compare = 6,
        Shift = 5,
        Addition = 4,
        Multiplication = 3,
        Prefixes = 2,
        Postfixes = 1
    };
    ast_op_priority getPriority(ast_op_type op);


    struct ast_node {
        ast_node(ast_node_type type) : nodeType(type) {}

        virtual ~ast_node() {}

        ast_node_type nodeType;
        position_type location;

        virtual nxc::type       &getType()       = 0;
        virtual nxc::type const &getType() const = 0;

        ID annotation = ID_UNKNOWN;
    };

    struct ast_value : public ast_node {
        ast_value(ast_value &&other);
        ast_value(const ast_value &other);
        ast_value()
                : ast_node(ast_node_type::Value) {}

        ast_value(ast_value_type type)
                : ast_node(ast_node_type::Value),
                  type(type) {}

        ast_value(ast_value_type type, const std::string &value)
                : ast_node(ast_node_type::Value),
                  type(type),
                  value(value) {}

        ast_value(ast_value_type type, std::string &&value)
                : ast_node(ast_node_type::Value),
                  type(type),
                  value(std::move(value)) {}

        virtual ~ast_value() {}

        nxc::type &getType() override {
            return *resultType;
        }
        nxc::type const &getType() const override {
            return *resultType;
        }

        ast_value_type type;
        std::string value;
        required_ptr_to <nxc::type> resultType;
    };


    struct ast_typename : public ast_node {
        ast_typename(ast_typename &&other);
        ast_typename(const ast_typename &other);
        ast_typename() : ast_node(ast_node_type::Typename) {}
        ast_typename(const nxc::type &t);

        virtual ~ast_typename() {}

        nxc::type &getType() override {
            return *name;
        }
        nxc::type const &getType() const override {
            return *name;
        }

        required_ptr_to <nxc::type> name;
    };

    struct ast_branch : public ast_node {
        ast_branch(ast_branch &&other);
        ast_branch(const ast_branch &other);
        ast_branch() : ast_node(ast_node_type::Multinode),
                       op(ast_op_type::Passthrough) {}

        ast_branch(ast_op_type op) : ast_node(ast_node_type::Multinode),
                                     op(op) {}

        virtual ~ast_branch() {}

        nxc::type &getType() override {
            return *resultType;
        }
        nxc::type const &getType() const override {
            return *resultType;
        }

        ast_op_type op;
        std::vector<nxc::required_ptr_to<nxc::ast_node>> children;
        required_ptr_to <nxc::type> resultType;

        nxc::ast_node &firstChild() {
            return *children.at(0);
        }

        nxc::ast_node const &firstChild() const {
            return *children.at(0);
        }

        nxc::ast_node &lastChild() {
            return *children.at(children.size() - 1);
        }

        nxc::ast_node const &lastChild() const {
            return *children.at(children.size() - 1);
        }

        nxc::ast_node &childAt(size_t n) {
            return *children.at(n);
        }

        nxc::ast_node const &childAt(size_t n) const {
            return *children.at(n);
        }

        required_ptr_to<nxc::ast_node> &operator[](size_t n) {
            return children[n];
        }

        required_ptr_to<nxc::ast_node> const &operator[](size_t n) const {
            return children[n];
        }
        size_t nChildren() const {
            return children.size();
        }
    };
    ///////////////
    // AST swaps //
    ///////////////

    template<typename... Args>
    ast_branch &CreateSwap(std::unique_ptr<ast_node> &head, Args... args) {

        using namespace std;
        std::unique_ptr<ast_node> child = PtrCreateCast<ast_branch, ast_node>(args...);
        swap(child, head);

        ast_branch &head_ref = static_cast<ast_branch &>(*head);
        PtrPush(head_ref.children, child);
        return head_ref;
    }

    template<typename ...Args>
    ast_branch &CreateSwapFirst(nxc::ast_branch &parent, Args... args) {
        return CreateSwap<Args...>(*parent.children.begin(), args...);
    }

    template<typename ...Args>
    ast_branch &CreateSwapLast(nxc::ast_branch &parent, Args... args) {
        return CreateSwap<Args...>(*parent.children.rbegin(), args...);
    };


    template<typename T, typename ArrT, typename... Args>
    T &CreatePushReturn(std::vector<std::unique_ptr<ArrT>> &vec, Args... args) {
        std::unique_ptr<ArrT> newT = PtrCreateCast<T, ArrT>(args...);
        T &ref = static_cast<T &>(*newT);
        vec.push_back(std::move(newT));
        return ref;
    };

    std::string &getFunctionName(ast_branch &call);
    const std::string &getFunctionName(const ast_branch &call);
    std::vector<nxc::type> getFunctionArgTypes(const ast_branch &call);
}

#endif //CUSTOM_ID_EXPR_HPP
