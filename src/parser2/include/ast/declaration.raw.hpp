/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Declaration raw parse structures. For parser-internal use only.
 */

#ifndef CUSTOM_ID_DECLARATION_RAW_HPP
#define CUSTOM_ID_DECLARATION_RAW_HPP

#include <string>
#include <interface/tokens.hpp>
#include <parser2/include/ast/enums.hpp>
#include <parser2/include/ast/pointers.hpp>
#include <global/include/macros.hpp>

namespace nxc {

    struct rawtype_detail {
    };
    struct ast_node;
    struct initializer;

    ////////////////
    // DECLARATOR //
    ////////////////

    struct declarator_array {
    public:
        position_type location;
        required_ptr_to <ast_node> size;
        inline bool empty() const { return size == nullptr; }
    };

    struct declarator {
        declarator() {}

        declarator(const declarator &other) = delete;

        declarator(declarator &&other) {
            using namespace std;
            swap(location, other.location);
            swap(name, other.name);
            swap(arrays, other.arrays);
        }

        position_type location;
        std::string name;
        list_of <declarator_array> arrays;
    };

    /////////////////
    // BASIC TYPES //
    /////////////////

    struct rawtype {
        RawType type = RawType::int_n;
        position_type location;
        optional_ptr_to <rawtype_detail> details;
    };
    struct const_rawtype {
        bool is_const = false;
        rawtype baseType;
    };
    struct type_name {
        const_rawtype type;
        list_of <declarator_array> arrays;
    };
    struct is_signed : rawtype_detail {
        bool really = true;
    };

    ////////////////
    // USER TYPES //
    ////////////////

    struct unknown_name : rawtype_detail {
        unknown_name() {}

        unknown_name(const std::string &name) : name(name) {}

        unknown_name(std::string &&name) : name(std::move(name)) {}

        std::string name;
    };

    struct struct_declaration {
        const_rawtype type;
        list_of <declarator> instances;
    };
    struct struct_info : rawtype_detail {
        position_type location;
        bool native = false;
        optional_ptr_to <std::string> name;
        optional_ptr_to <list_of<struct_declaration>> members;
    };
    struct enumerator {
        position_type location;
        std::string name;
        optional_ptr_to <ast_node> value;
    };
    struct enum_info : rawtype_detail {
        bool native = false;
        optional_ptr_to <std::string> name;
        optional_ptr_to <list_of<enumerator>> members;
    };


    struct initializer_declaration {
        declarator decl;
        optional_ptr_to <initializer> value;
    };

    struct parameter {
        const_rawtype type;
        bool reference;
        initializer_declaration decl;
    };
}

#endif //CUSTOM_ID_DECLARATION_RAW_HPP
