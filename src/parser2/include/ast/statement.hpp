/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Statement parse structures.
 */


#ifndef CUSTOM_ID_STATEMENT_HPP
#define CUSTOM_ID_STATEMENT_HPP

#include <parser2/include/ast/pointers.hpp>
#include <interface/tokens.hpp>
#include "enums.hpp"

namespace nxc {
    struct ast_node;
    struct edeclinfo;

    struct statement_base {
        virtual ~statement_base() {}
    };
    struct statement {
        position_type location;
        StatementType type;
        required_ptr_to<statement_base> stmt;
    };



    struct in_base {
        virtual ~in_base() {}
    };

    struct in_decl : in_base {
        required_ptr_to<edeclinfo> declaration;
    };

    struct in_stmt : in_base {
        statement stmt;
    };

    struct expression_statement : statement_base {
        optional_ptr_to<ast_node> expression;
    };


    struct expression_declaration {
        ExpressionDeclaration type;
        required_ptr_to<in_base> ptr;
    };

    struct compound_statement : statement_base {
        ID id;
        list_of<expression_declaration> statements;
    };

    struct jump_to : statement_base {
        std::string label;
    };

    struct thread_name : statement_base {
        std::string name;
    };
    
    struct thread_names : statement_base {
        std::vector<std::string> names;
    };

    struct thread_priority : statement_base {
        std::string name;
        required_ptr_to<ast_node> priority;
    };

    struct label_named : statement_base {
        std::string name;
        statement child;
    };

    struct label_case : statement_base {
        required_ptr_to<ast_node> value;
        statement child;
    };

    struct label_default : statement_base {
        statement child;
    };

    struct ifelse_statement : statement_base {
        required_ptr_to<ast_node> condition;
        statement true_case;
        optional_ptr_to<statement> false_case;
    };

    struct switch_statement : statement_base {
        required_ptr_to<ast_node> value;
        statement body;
    };

    struct loop_info : statement_base {
        required_ptr_to<ast_node> condition;
        statement body;
    };

    struct for_loop_info : statement_base {
        optional_ptr_to<expression_declaration> initializer;
        optional_ptr_to<ast_node> condition;
        optional_ptr_to<ast_node> update;
        statement body;
    };
}

#endif //CUSTOM_ID_STATEMENT_HPP
