/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * NXC type registrator.
 *  - parses raw type AST into common NXC types
 */


#ifndef CUSTOM_ID_TYPE_PARSER_HPP
#define CUSTOM_ID_TYPE_PARSER_HPP

#include <global/include/symbols.hpp>
#include <parser2/include/ast.hpp>

namespace nxc {
    class type_resolver {
    public:
        type_resolver(symbol_table &tbl, scope_data &scope)
                : tbl(tbl), scope(scope) {}

        bool is_signed(const rawtype_detail &info);
        bool is_signed(const rawtype        &info);

        void parse_type(const type_name     &spec, type &out);
        void parse_type(const const_rawtype &spec, type &out);
        void parse_type(const rawtype       &spec, type &out);

        void parse_type(const struct_info  &spec, const position_type &pos, nxc::type &out);
        void parse_type(const enum_info    &spec, const position_type &pos, nxc::type &out);
        void parse_type(const unknown_name &spec, const position_type &pos, nxc::type &out);

        void arrayize(nxc::type base_part,
                      const list_of<declarator_array> &array_part,
                      nxc::type &out);

    private:
        symbol_table &tbl;
        scope_data &scope;
    };
}


#endif //CUSTOM_ID_TYPE_PARSER_HPP
