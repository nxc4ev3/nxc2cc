/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Public parser interface.
 */


#ifndef CUSTOM_ID_PARSER_HPP
#define CUSTOM_ID_PARSER_HPP

#include <parser2/include/ast/file.hpp>
#include <global/include/symbols.hpp>
#include <interface/lexer.hpp>

namespace nxc {
    typedef std::list<my_token> token_batch;
    typedef std::list<my_token>::iterator token_iterator;

    struct parse_result {
        translation_unit tree;
        symbol_table syms;
    };

    parse_result parse(token_iterator begin,
                       token_iterator end);
}


#endif //CUSTOM_ID_PARSER_HPP
