/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Public lexer API <-> SLex + Boost::Wave bridge
 */


#include <interface/lexer.hpp>
#include <interface/tokens.hpp>
#include <lexer/types.hpp>

nxc::my_pos::my_pos() {}

nxc::my_pos::my_pos(const file_pos_t &ref) :
        row(ref.get_line()),
        col(ref.get_column()),
        file(ref.get_file().begin(),
             ref.get_file().end()) {}

nxc::my_pos::my_pos(const std::string &file, ssize_t row, ssize_t col)
        : row(row), col(col), file(file) {}


nxc::my_token::my_token() {}

nxc::my_token::my_token(const token_t &ref)
        : value(ref.get_value().begin(),
                ref.get_value().end()),
          type(ref.get_token2_id()),
          position(ref.get_position()) {}

bool isWhitespace(const token_t &token) {
    using namespace nxc;
    bool whitespace = tokenMatchesType(token, T2_WhiteSpaceTokenType);
    bool eof        = tokenMatchesType(token, T2_EOFTokenType);
    bool eol        = tokenMatchesType(token, T2_EOLTokenType);
    bool unknown    = tokenMatchesType(token, T2_UnknownTokenType);
    bool internal   = tokenMatchesType(token, T2_InternalTokenType);
    bool preprocess = tokenMatchesType(token, T2_PPTokenType);
    return whitespace || eof || eol || unknown || internal || preprocess;
}

nxc::lexer_result nxc::lex(const std::string &filename,
                           const std::string &bindir,
                           const std::string &sysdir) {
    std::ifstream in(filename);
    in.unsetf(std::ios::skipws);
    return lex(in, filename, bindir, sysdir);
}



nxc::lexer_result nxc::lex(std::istream &input,
                           const std::string &filename,
                           const std::string &bindir,
                           const std::string &sysdir) {
    lexer_result result;
    std::string data(std::istreambuf_iterator<char>(input.rdbuf()),
                     std::istreambuf_iterator<char>());
    auto in_iter = data.begin();
    auto in_end  = data.end();

    boost::wave::nxc_hooks<token_t> hooks(bindir, result.downloads);

    pp_ctx_t context(in_iter, in_end, filename.c_str(), hooks);
    using boost::wave::language_support;
    context.set_language(language_support(
            language_support::support_c99 |
            language_support::support_option_include_guard_detection |
            language_support::support_option_insert_whitespace |
            language_support::support_option_single_line));

    context.add_sysinclude_path(sysdir.c_str());

    pp_iter_t out_iter = context.begin();
    pp_iter_t out_end  = context.end();

    out_iter.force_include("nxclib.nxc", true);

    for (; out_iter != out_end; ++out_iter) {
        token_t ref = std::move(*out_iter);
        if (!isWhitespace(ref)) {
            result.tokens.push_back(my_token(ref));
        }
    }
    return result;
}

