/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Public lexer/preprocessor interface.
 */


#ifndef CUSTOM_ID_LEXER_HPP
#define CUSTOM_ID_LEXER_HPP

#include <lexer/token2_ids.hpp>
#include <string>
#include <sstream>
#include <vector>
#include <list>

// isolation from Boost::Wave
/* // not working right now
namespace boost {
    namespace wave {
        namespace util {

            template<typename StringT>
            class file_position;

            template<typename E, class A>
            class AllocatorStringStorage;

            template<typename E,
                    class T,
                    class A,
                    class Storage>
            class flex_string;

            template<
                    typename Storage,
                    typename Align
            >
            class CowString;

            typedef file_position<
                    boost::wave::util::flex_string<
                            char,
                            std::char_traits<char>,
                            std::allocator<char>,
                            boost::wave::util::CowString<
                                    boost::wave::util::AllocatorStringStorage<char>
                            >
                    >
            > file_position_type;
        }
        namespace cpplexer {

            template<typename PositionT>
            class slex_token;
        }
    }
}
*/
#include <boost/wave/util/file_position.hpp>
#include <lexer/slex_token.hpp>

typedef boost::wave::util::file_position_type file_pos_t;
typedef boost::wave::cpplexer::slex_token<file_pos_t> token_t;

// lexer stuff
namespace nxc {

    struct my_pos {
        my_pos();
        my_pos(const std::string &file, ssize_t row, ssize_t col);
        my_pos(const file_pos_t &ref);

        ssize_t row, col;
        std::string file;
    };

    struct my_token {
        my_token();

        my_token(const token_t &ref);

        operator boost::wave::token2_id() const {
            return type;
        }

        operator const std::string &() const {
            return value;
        }

        operator const my_pos &() const {
            return position;
        }

        std::string value;
        boost::wave::token2_id type;
        my_pos position;
    };

    struct lexer_result {
        lexer_result() {}

        std::vector<std::string> downloads;
        std::list<my_token> tokens;
    };

    lexer_result lex(const std::string &filename,
                     const std::string &bindir,
                     const std::string &sysdir);

    lexer_result lex(std::istream &input,
                     const std::string &filename,
                     const std::string &bindir,
                     const std::string &sysdir);

    inline my_pos createLocation(const std::string &file, ssize_t row, ssize_t col) {
        return my_pos(file, row, col);
    }

    template<typename _CharT, typename _Traits>
    inline std::basic_ostream<_CharT, _Traits> &
    operator<<(std::basic_ostream<_CharT, _Traits> &out, my_pos fi) {
        if (fi.file.size() > 0) {
            out << fi.file;
        } else {
            out << "<unknown>";
        }
        if (fi.col == -1) {
            if (fi.row != -1) {
                out << "@" << fi.row;
            }
        } else {
            out << "@" << fi.row << "." << fi.col;
        }
        return out;
    }

    inline std::string pos2str(const my_pos &pos) {
        std::ostringstream str;
        str << pos << pos.col;
        return str.str();
    }

    inline std::string pos2str(const my_token &token) { return pos2str(token.position); }
}
#endif //CUSTOM_ID_LEXER_HPP
