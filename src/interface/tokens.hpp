/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Shorthands for accessing token data.
 */


#ifndef CUSTOM_ID_TOKEN_MACRO_HPP
#define CUSTOM_ID_TOKEN_MACRO_HPP

#include <lexer/token2_ids.hpp>
#include <interface/lexer.hpp>
#include <stdint.h>
#include <sstream>

#define tid(id)                 nxc::tokenID_type::T2_ ## id
#define cid(cat)                nxc::tokenCat_type::T2_ ## cat

namespace nxc {
    using namespace boost::wave;

    typedef my_token        token_type;
    typedef my_pos          position_type;
    typedef std::string     value_type;
    typedef token2_id       tokenID_type;
    typedef token2_category tokenCat_type;
    typedef uint32_t        tokenMask_type;


    constexpr tokenMask_type FullMask = 0xFFFFFFFF;

    inline bool tokenMatchesMask(tokenMask_type tokenID,
                                 tokenMask_type andMask,
                                 tokenMask_type expectedResult) {
        return (tokenID & andMask) == expectedResult;
    }

    inline bool tokenMatchesMask(tokenMask_type tokenID,
                                 tokenMask_type andMask) {
        return (tokenID & andMask) != 0;
    }

//#define tokenMatchesMaskN(tokenID, andMask, expectedResult) nxc::tokenMatchesMask(tokenID, tid(andMask), cid(expectedResult))

    inline bool tokenMatchesCategory(tokenMask_type tokenID,
                                     tokenMask_type catID,
                                     tokenMask_type catMask) {
        return tokenMatchesMask(tokenID, catMask, catID & catMask);
    }

//#define tokenMatchesCategoryN(tokenID, andMask, expectedResult) nxc::tokenMatchesCategory(tokenID, cid(catID), cid(catMask))

    inline bool tokenMatchesType(tokenMask_type tokenID,
                                 tokenMask_type type) {
        return tokenMatchesCategory(tokenID, type, cid(TokenTypeMask));
    }

//#define tokenMatchesTypeN(tokenID, typeMask) nxc::tokenMatchesType(tokenID, cid(typeMask))

    inline bool tokenMatchesID(tokenMask_type tokenID, tokenMask_type typeID) {
        return tokenID == typeID;
    }

//#define tokenMatchesIDN(tokenID, typeID) nxc::tokenMatchesID(tokenID, tid(typeID))

    inline tokenID_type getID(const token_type &token) {
        return token.type;
    }

    inline const position_type &getPosition(const token_type &token) {
        return token.position;
    }

    inline value_type getValue(const token_type &token) {
        return token.value;
    }

    inline int getLine(const token_type &token) {
        return token.position.row;
    }

    inline int getColumn(const token_type &token) {
        return token.position.col;
    }

    inline std::string getFile(const token_type &token) {
        return token.position.file;
    }

    inline std::string getPositionSummary(const token_type &token) {
        std::stringstream stream;
        stream << "line " << getLine(token) << ", ";
        stream << "column " << getColumn(token) << " ";
        stream << "in file \"" << getFile(token) << "\"";
        return stream.str();
    }
}

#endif //CUSTOM_ID_TOKEN_MACRO_HPP
