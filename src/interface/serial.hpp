/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Public C serializing component interface.
 */


#ifndef CUSTOM_ID_SERIALIZER_HPP
#define CUSTOM_ID_SERIALIZER_HPP

#include <ostream>
#include <interface/parser.hpp>

namespace nxc {

    void serialize(std::ostream &out, parse_result &data);
    void serialize(std::ostream &out, parse_result &data, const std::string &indent);
}

#endif //CUSTOM_ID_SERIALIZER_HPP
