/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Command line interface.
 */

#include <main.hpp>
#include <interface/lexer.hpp>
#include <iostream>
#include <fstream>


#include <interface/parser.hpp>
#include <parser2/include/ast.hpp>
#include <global/include/nxcexcept.hpp>
#include <boost/wave/cpp_exceptions.hpp>
#include <interface/serial.hpp>


// import lexer if not separately instantiated
#if !defined(BOOST_WAVE_SEPARATE_LEXER_INSTANTIATION)
#include "slex/cpp_slex_lexer.hpp"
#endif


using std::string;
using std::getline;
using std::ofstream;
using std::ifstream;
using std::cout;
using std::cerr;
using std::endl;
using std::ostream;


bool openOut(const string &file, ofstream &out) {
    out.open(file, std::ios::out | std::ios::trunc);
    return out.good();
}

bool openIn(const string &file, ifstream &in) {
    in.open(file);
    if (in.good()) {
        in.unsetf(std::ios::skipws);
        return true;
    }
    return false;
}

int main(int argc, char *argv[]) {
    cout << WELCOME_STRING   << endl;
    cout << COPYRIGHT_STRING << endl;
    cout.flush();
    if (argc != 6) {
        cerr << "Invalid argument count." << endl;
        cerr << "Usage: " << argv[0] << " <sysdir> <bindir> <input nxc> <output c> <output dl>" << endl;
        cerr << "<sysdir>    ... Path to the directory with appropriate NXC headers." << endl;
        cerr << "<bindir>    ... Path to the directory from which should resources be pulled." << endl;
        cerr << "<input nxc> ... Path to the file to be transpiled to C." << endl;
        cerr << "<output c>  ... Path where the C file should be saved." << endl;
        cerr << "<output dl> ... Path where the file download list should be saved." << endl;
        cerr.flush();
        return 1;
    }

    
    try {
        string sysdir(argv[1]);
        string bindir(argv[2]);
        string input (argv[3]);
        string output(argv[4]);
        string dllist(argv[5]);

        ifstream inputStream;
        ofstream outputStream, fileStream;

        if (!openIn(input, inputStream)){
            cerr << "Cannot open input file. "         << strerror(errno) << endl;
            return 1;
        }
        if (!openOut(output, outputStream)) {
            cerr << "Cannot create output file. "      << strerror(errno) << endl;
            return 1;
        }
        if (!openOut(dllist, fileStream)) {
            cerr << "Cannot create download listing. " << strerror(errno) << endl;
            return 1;
        }
        
        cout << "P1: lexer + preprocessor...";
        cout.flush();
        nxc::lexer_result lexed = nxc::lex(inputStream, input, bindir, sysdir);
        cout << " OK." << endl;
        cout.flush();

        // save #download file list
        for (std::string const &p : lexed.downloads) {
            fileStream << p << endl;
        }
        
        auto it = lexed.tokens.begin();
        auto end = lexed.tokens.end();


        cout << "P2: parser...";
        cout.flush();
        nxc::parse_result parsed = nxc::parse(it, end);
        cout << " OK." << endl;
        cout.flush();

        cout << "P3: C serializer...";
        cout.flush();

        if (argc >= 3)
            nxc::serialize(outputStream, parsed, "  ");
        else
            nxc::serialize(outputStream, parsed);

        cout << " OK." << endl;
        cout.flush();
    }


    catch (nxc::semantic_error const &ex) {
        cerr << "Exception was thrown:" << endl;
        cerr << "[semantic] " << ex.what() << endl;
        cerr.flush();
        return 2;
    }
    catch (nxc::serialize_error const &ex) {
        cerr << "Exception was thrown:" << endl;
        cerr << "[serial] " << ex.what() << endl;
        cerr.flush();
        return 2;
    }
    catch (nxc::syntax_error const &ex) {
        cerr << "Exception was thrown:" << endl;
        cerr << "[parser] " << ex.what() << endl;
        cerr.flush();
        return 2;
    }
    catch (boost::wave::cpp_exception const &e) {
        cerr << "Exception was thrown:" << endl;
        cerr << "[preprocess]" << e.file_name() << "(" << e.line_no() << "): " << e.description() << endl;
        cerr.flush();
        return 2;
    }
    catch (std::exception const &e) {
        cerr << "Unexpected exception was thrown: " << e.what() << endl;
        cerr.flush();
        throw;
        return 3;
    }
    catch (...) {
        cerr << "Unexpected unknown exception was thrown." << endl;
        cerr.flush();
        throw;
        return 4;
    }
    cout << "Transcompilation succeeded." << endl;
    return 0;
}
