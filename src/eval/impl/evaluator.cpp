/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Compile-time evaluation of constants.
 */


#include <parser2/include/ast.hpp>
#include <global/include/symbols.hpp>
#include "eval/include/evaluator.hpp"


bool nxc::eval(int64_t &result,
               const ast_node &expr,
               scope_data &scope,
               symbol_table &tbl) {
    evaluator engine(tbl, scope);
    return engine.eval(expr, result);
}

bool nxc::evaluator::eval_assignment(const ast_branch &branch, result_type &out) {
    if (branch.op != ast_op_type::Assign_Only) {
        return false;
    }
    if (!eval(branch.childAt(1), out))
        return false;
    nxc::type to = branch.childAt(0).getType();
    return cast(out, to, out);
}

bool nxc::evaluator::eval(const ast_node &node, result_type &out) {
    if (node.nodeType == ast_node_type::Typename)
        return eval_type(static_cast<const ast_typename &>(node), out);
    if (node.nodeType == ast_node_type::Value)
        return eval_value(static_cast<const ast_value &>(node), out);
    const ast_branch &branch = static_cast<const ast_branch &>(node);
    switch (branch.op) {
        case ast_op_type::Passthrough:
        case ast_op_type::Comma:
            return eval(branch.lastChild(), out);
        case ast_op_type::Assign_Only:
        case ast_op_type::Assign_Multiply:
        case ast_op_type::Assign_Divide:
        case ast_op_type::Assign_Modulo:
        case ast_op_type::Assign_Add:
        case ast_op_type::Assign_Subtract:
        case ast_op_type::Assign_Leftshift:
        case ast_op_type::Assign_Rightshift:
        case ast_op_type::Assign_BitAnd:
        case ast_op_type::Assign_BitXor:
        case ast_op_type::Assign_BitOr:
        case ast_op_type::Assign_Abs:
        case ast_op_type::Assign_Sign:
            return eval_assignment(branch, out);
        case ast_op_type::Ternary:
            return eval_ternary(branch, out);
        case ast_op_type::LogicOr:
            return eval_two(branch, out, [](result_type a, result_type b) { return a || b; });
        case ast_op_type::LogicAnd:
            return eval_two(branch, out, [](result_type a, result_type b) { return a && b; });
        case ast_op_type::BitOr:
            return eval_two(branch, out, [](result_type a, result_type b) { return a | b; });
        case ast_op_type::BitXor:
            return eval_two(branch, out, [](result_type a, result_type b) { return a ^ b; });
        case ast_op_type::BitAnd:
            return eval_two(branch, out, [](result_type a, result_type b) { return a & b; });
        case ast_op_type::Equality_Equal:
            return eval_two(branch, out, [](result_type a, result_type b) { return a == b; });
        case ast_op_type::Equality_Notequal:
            return eval_two(branch, out, [](result_type a, result_type b) { return a != b; });
        case ast_op_type::Relation_LessEqual:
            return eval_two(branch, out, [](result_type a, result_type b) { return a <= b; });
        case ast_op_type::Relation_Less:
            return eval_two(branch, out, [](result_type a, result_type b) { return a < b; });
        case ast_op_type::Relation_Greater:
            return eval_two(branch, out, [](result_type a, result_type b) { return a > b; });
        case ast_op_type::Relation_GreaterEqual:
            return eval_two(branch, out, [](result_type a, result_type b) { return a >= b; });
        case ast_op_type::Shift_Left:
            return eval_two(branch, out, [](result_type a, result_type b) { return a << b; });
        case ast_op_type::Shift_Right:
            return eval_two(branch, out, [](result_type a, result_type b) { return a >> b; });
        case ast_op_type::Addition_Add:
            return eval_two(branch, out, [](result_type a, result_type b) { return a + b; });
        case ast_op_type::Addition_Subtract:
            return eval_two(branch, out, [](result_type a, result_type b) { return a - b; });
        case ast_op_type::Multiplication_Multiply:
            return eval_two(branch, out, [](result_type a, result_type b) { return a * b; });
        case ast_op_type::Multiplication_Divide:
            return eval_two(branch, out, [](result_type a, result_type b) { return a / b; });
        case ast_op_type::Multiplication_Modulo:
            return eval_two(branch, out, [](result_type a, result_type b) { return a % b; });
        case ast_op_type::Cast:
            return eval_cast(branch, out);
        case ast_op_type::Unary_Plus:
            return eval_one(branch, out, [](result_type a) { return +a; });
        case ast_op_type::Unary_Minus:
            return eval_one(branch, out, [](result_type a) { return -a; });
        case ast_op_type::Unary_Complement:
            return eval_one(branch, out, [](result_type a) { return ~a; });
        case ast_op_type::Unary_Negate:
            return eval_one(branch, out, [](result_type a) { return !a; });
        case ast_op_type::Sizeof_Type:
            return eval_sizeof_type(branch, out);
        case ast_op_type::Sizeof_Expr:
            return eval_sizeof_expr(branch, out);
        case ast_op_type::Increment_Pre:
        case ast_op_type::Decrement_Pre:
        case ast_op_type::Increment_Post:
        case ast_op_type::Decrement_Post:
        case ast_op_type::Index:
        case ast_op_type::Member:
        case ast_op_type::Call:
            return false;
    }
}

bool nxc::evaluator::eval_value(const nxc::ast_value &node, nxc::evaluator::result_type &out) {
    switch (node.type) {
        case ast_value_type::Identifier:
            return eval_identifier(node.value, out);
        case ast_value_type::String:
            return false;
        case ast_value_type::Constant_Boolean:
            if (node.value == "true")
                out = true;
            else if (node.value == "false")
                out = false;
            else
                return false;
            return true;
        case ast_value_type::Constant_Integer:
            return eval_integer(node.value, out);
        case ast_value_type::Constant_Float:
            return eval_float(node.value, out);
        case ast_value_type::Constant_Character:
            return eval_character(node.value, out);
    }
}

bool nxc::evaluator::eval_type(const nxc::ast_typename &node, nxc::evaluator::result_type &out) {
    // One day I might implement complete interpreter...
    return false;
}


bool identifier_resolver_fn(nxc::symbol_table::symbol_type &sym,
                            nxc::scope_data &scope,
                            nxc::symbol_table &tbl,
                            const std::string &name,
                            nxc::evaluator::result_type &result,
                            bool log) {
    using namespace nxc;
    sym_class which = static_cast<sym_class>(sym.which());
    if (which == sym_class::VARIABLE) {
        var_data &data = boost::get<var_data>(sym);
        if (data.getName() == name) {
            if (data.hasValue()) {
                result = data.getValue();
                return true;
            } else {
                if (!log)
                    return false;
                throw nxc::semantic_error(-1,-1,"","Using not statically eval()-able variable as constant isn't possible.");
            }
        }
    } else if (which == sym_class::ENUM) {
        enum_data &data = boost::get<enum_data>(sym);
        for (enum_data::member_type const &enumik : data.getMembers()) {
            if (enumik.first == name) {
                result = enumik.second;
                return true;
            }
        }
    }
    return false;
}

bool nxc::evaluator::eval_identifier(const std::string &name, nxc::evaluator::result_type &out) {
    return syms.lookup_symbols
            (scope,
             sym_mask::ENUM | sym_mask::VARIABLE,
             identifier_resolver_fn,
             name,
             out,
             log);
}

bool nxc::evaluator::eval_integer(const std::string &value, nxc::evaluator::result_type &out) {
    size_t end = 0;
    if (value.length() >= 1 && value[0] == '0') {
        if (value.length() >= 3 && value[1] == 'x') {
            out = std::stol(value, &end, 16);
        } else {
            out = std::stol(value, &end, 8);
        }
    } else {
        out = std::stol(value, &end, 10);
    }
    return value.length() == end;
}

bool nxc::evaluator::eval_float(const std::string &value, nxc::evaluator::result_type &out) {
    size_t len;
    out = (nxc::evaluator::result_type) std::stof(value, &len);
    return len == value.size();
}

bool nxc::evaluator::eval_character(std::string value, nxc::evaluator::result_type &out) {
    if (value.length() < 3)
        return false;
    value.erase(value.length() - 1, 1);
    value.erase(0, 1);
    if (value[0] == '\\') {
        if (value.length() < 2)
            return false;
        switch (value[1]) {
            case 'a':
                out = '\a';
                break;
            case 'b':
                out = '\b';
                break;
            case 'f':
                out = '\f';
                break;
            case 'n':
                out = '\n';
                break;
            case 'r':
                out = '\r';
                break;
            case 't':
                out = '\t';
                break;
            case 'v':
                out = '\v';
                break;
            case '\\':
                out = '\\';
                break;
            case '\'':
                out = '\'';
                break;
            case '\"':
                out = '\"';
                break;
            case '\?':
                out = '\?';
                break;
            case 'x': {
                std::string rest = value.substr(2);
                size_t end;
                out = (char) std::stoi(rest, &end, 16);
                return end == rest.length();
            }
            default: {
                std::string rest = value.substr(1);
                size_t end;
                out = (char) std::stoi(rest, &end, 8);
                return end == rest.length();
            }
        }
        return value.size() == 2;
    }
    if (value.length() != 1)
        return false;
    out = value[0];
    return true;
}

bool nxc::evaluator::cast(nxc::evaluator::result_type in, nxc::type to, result_type &out) {
    to = untypedefize(to, syms);
    if (to.which() == type_variants::variant_t)
        return true;
    if (to.which() != type_variants::basic)
        return false;
    switch (to.get<basic_type>()) {
        case basic_type::bool_type:
            out = (bool) in;
            return true;
        case basic_type::uchar_type:
        case basic_type::byte_type:
            out = (uint8_t) in;
            return true;
        case basic_type::schar_type:
            out = (int8_t) in;
            return true;
        case basic_type::sint_type:
        case basic_type::sshort_type:
            out = (int16_t) in;
            return true;
        case basic_type::uint_type:
        case basic_type::ushort_type:
            out = (uint16_t) in;
            return true;
        case basic_type::slong_type:
            out = (int32_t) in;
            return true;
        case basic_type::ulong_type:
            out = (uint32_t) in;
            return true;
            // todo float is not really supported
        case basic_type::float_type:
            out = (result_type) (float) in;
            return true;
        case basic_type::mutex_type:
        case basic_type::string_type:
            return false;
    }
}

bool nxc::evaluator::eval_pair(const nxc::ast_branch &branch,
                               result_pair &out) {
    result_type r1 = 0, r2 = 0;
    if (eval(branch.childAt(0), r1) &&
        eval(branch.childAt(1), r2)) {

        out.first = r1;
        out.second = r2;
        return true;
    } else {
        return false;
    }
}

bool nxc::evaluator::eval_cast(const nxc::ast_branch &branch, nxc::evaluator::result_type &out) {
    result_type temp = 0;
    type dest = branch.childAt(0).getType();

    return eval(branch.childAt(1), temp) && cast(temp, dest, out);
}

bool nxc::evaluator::eval_ternary(const nxc::ast_branch &branch, nxc::evaluator::result_type &out) {
    result_type cond = 0;
    if (!eval(branch.childAt(0), cond))
        return false;

    return eval(branch.childAt(cond ? 1 : 2), cond);
}

bool nxc::evaluator::eval_sizeof_type(const nxc::ast_branch &branch, nxc::evaluator::result_type &out) {
    const type &qtype = static_cast<const ast_typename &>(branch.childAt(0)).getType();
    return eval_sizeof(qtype, out);
}

bool nxc::evaluator::eval_sizeof_expr(nxc::ast_branch branch, nxc::evaluator::result_type &out) {
    nxc::type &exprT = branch.childAt(0).getType();
    required_ptr_to<ast_typename> ptr = PtrCreate<ast_typename>(exprT);
    branch.children[0] = std::move(ptr);

    return eval_sizeof_type(branch, out);
}

bool nxc::evaluator::eval_sizeof(nxc::type t, nxc::evaluator::result_type &out) {
    t = untypedefize(t, syms);
    if (t.which() == type_variants::basic) {
        switch(t.get<basic_type>()) {
            case basic_type::bool_type:
            case basic_type::byte_type:
            case basic_type::schar_type:
            case basic_type::uchar_type:
                out = 1;
                return true;
            case basic_type::sint_type:
            case basic_type::uint_type:
            case basic_type::sshort_type:
            case basic_type::ushort_type:
                out = 2;
                return true;
            case basic_type::slong_type:
            case basic_type::ulong_type:
                out = 4;
                return true;
            case basic_type::float_type:
                out = 4;
                return true;
            case basic_type::string_type:
            case basic_type::mutex_type:
                return false;
        }
    } else if (t.which() == type_variants::user) {
        switch (whichtype(t.get<usertype>(), syms)) {
            case usertype_kind::enum_kind:
                return eval_sizeof(nxc::type{enum_datatype}, out);
            case usertype_kind::struct_kind:
            case usertype_kind::typedef_kind:
                return false;
        }
    } else { // qtype.which() == type_variants::array | type_variants::variant_t | type_variants::void_t
        return false;
    }
}
