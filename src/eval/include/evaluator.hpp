/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Compile-time evaluation of constants.
 */



#ifndef CUSTOM_ID_EVALUATOR_HPP
#define CUSTOM_ID_EVALUATOR_HPP

#include <cstdint>

namespace nxc {
    class symbol_table;

    class scope_data;

    struct ast_node;
    struct ast_branch;

    class evaluator {
    public:
        typedef int64_t result_type;
        typedef std::pair<result_type, result_type> result_pair;

        evaluator(symbol_table &tbl, scope_data &scope, bool log = true)
                : syms(tbl), scope(scope), log(log) {}

        bool eval(const ast_node &node, result_type &out);
        bool eval_value(const ast_value &node, result_type &out);
        bool eval_type(const ast_typename &node, result_type &out);
        bool eval_identifier(const std::string &name, result_type &out);
        bool eval_integer(const std::string &value, result_type &out);
        bool eval_float(const std::string &value, result_type &out);
        bool eval_character(std::string value, result_type &out);
        bool eval_assignment(const ast_branch &branch, result_type &out);
        bool eval_cast(const ast_branch &branch, result_type &out);

        bool eval_ternary(const ast_branch &branch, result_type &out);
        bool eval_sizeof_type(const ast_branch &branch, result_type &out);
        bool eval_sizeof_expr(ast_branch branch, result_type &out);

        bool eval_pair(const ast_branch &branch, result_pair &out);

        bool eval_sizeof(type t, result_type &out);

        template<typename Predicate>
        bool eval_one(const ast_branch &branch, result_type &out, Predicate pred) {
            result_type temp;
            if (eval(branch, temp)) {
                out = pred(temp);

                return true;
            } else {
                return false;
            }
        }

        template<typename Predicate>
        bool eval_two(const ast_branch &branch, result_type &out, Predicate pred) {
            result_pair pair;
            if (eval_pair(branch, pair)) {
                out = pred(pair.first, pair.second);

                return true;
            } else {
                return false;
            }
        }
        /*
        template<typename Predicate>
        bool eval_two_foldLeft(const ast_branch &branch, result_type &out, Predicate pred) {
            result_type running = 0;
            if (!eval(branch.childAt(0), running))
                return false;
            auto it  = branch.children.begin() + 1;
            auto end = branch.children.end();
            for (; it != end; ++it){
                result_type local = 0;
                if (!eval(**it, local))
                    return false;
                running = pred(running, local);
            }
            return true;
        }*/
    protected:
        bool cast(result_type in, nxc::type to, result_type &out);

        symbol_table &syms;
        scope_data &scope;
        bool log;
    };

    bool eval(int64_t &result,
              const ast_node &expr,
              scope_data &scope,
              symbol_table &tbl);
}

#endif //CUSTOM_ID_EVALUATOR_HPP
