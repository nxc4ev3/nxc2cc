/**
 * NXC2CC - the NXC to C transpiler
 * 
 * 
 * Token to string conversion functions.
 * 
 * Copyright (C) 2001-2012  Hartmut Kaiser
 *           (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * 
 * Authors: Hartmut Kaiser
 *          Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */

#ifndef CUSTOM_ID_TOKEN2_UTIL_HPP
#define CUSTOM_ID_TOKEN2_UTIL_HPP

#include <main.hpp>

namespace boost {
    namespace wave {

///////////////////////////////////////////////////////////////////////////////
//  return a token name
        BOOST_WAVE_DECL
        BOOST_WAVE_STRINGTYPE get_token_name(token2_id tokid);

///////////////////////////////////////////////////////////////////////////////
//  return a token name
        BOOST_WAVE_DECL
        char const *get_token_value(token2_id tokid);

    }
}

#endif //CUSTOM_ID_TOKEN2_UTIL_HPP
