/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Custom preprocessing directives - download and import.
 */


#ifndef CUSTOM_ID_HOOKS_HPP
#define CUSTOM_ID_HOOKS_HPP

#include <ios>
#include <list>

#include "token2_ids.hpp"
#include <boost/wave/whitespace_handling.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/throw_exception.hpp>

#include <main.hpp>

namespace fs = boost::filesystem;

namespace boost { namespace wave {

template<typename TokenT>
class nxc_hooks : public boost::wave::context_policies::eat_whitespace<TokenT> {
public:
	nxc_hooks(std::string const &bindir_, std::vector<std::string> &downloads);

	template<typename ContextT, typename ContainerT>
	bool found_unknown_directive(
			ContextT const &ctx, ContainerT const &line, ContainerT &pending);

	std::vector<std::string> const &get_download_list() {
		return download;
	}

protected:
	inline void insert_download(std::string const &filename) {
		download.push_back(filename);
	};

	template<typename ContainerT,
			typename PositionT>
	void insert_file(fs::path const &file,
	                 std::string const &identifier,
	                 ContainerT &output,
	                 PositionT base);
private:
	fs::path bindir;
	std::vector<std::string> &download;
	std::string except_str;
};

}
}

#if BOOST_WAVE_SEPARATE_HOOK_INSTANTIATION == 0
#include "hooks.impl.hpp"
#endif


#endif //CUSTOM_ID_HOOKS_HPP
