/**
 * NXC2CC - the NXC to C transpiler
 * 
 * 
 * A generic NXC lexer token IDs.
 * 
 * Copyright (C) 2001-2012  Hartmut Kaiser
 *           (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * 
 * Authors: Hartmut Kaiser
 *          Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */

#include <main.hpp>
#include "token2_ids.hpp"

// this must occur after all of the includes and before any code appears
#ifdef BOOST_HAS_ABI_HEADERS
#include BOOST_ABI_PREFIX
#endif

///////////////////////////////////////////////////////////////////////////////
namespace boost {
namespace wave {

///////////////////////////////////////////////////////////////////////////////
//  return a token name
BOOST_WAVE_STRINGTYPE get_token_name(token2_id tokid) {
//  Table of token names
//
//      Please note that the sequence of token names must match the sequence of
//      token id's defined in then enum token_id above.
	static char const *tok_names[] = {
			/* 256 */   "AND",
			/* 257 */   "ANDAND",
			/* 258 */   "ASSIGN",
			/* 259 */   "ANDASSIGN",
			/* 260 */   "OR",
			/* 261 */   "ORASSIGN",
			/* 262 */   "XOR",
			/* 263 */   "XORASSIGN",
			/* 264 */   "COMMA",
			/* 265 */   "COLON",
			/* 266 */   "DIVIDE",
			/* 267 */   "DIVIDEASSIGN",
			/* 268 */   "DOT",
			/* 269 */   "SIGNASSIGN",
			/* 270 */   "ELLIPSIS",
			/* 271 */   "EQUAL",
			/* 272 */   "GREATER",
			/* 273 */   "GREATEREQUAL",
			/* 274 */   "LEFTBRACE",
			/* 275 */   "LESS",
			/* 276 */   "LESSEQUAL",
			/* 277 */   "LEFTPAREN",
			/* 278 */   "LEFTBRACKET",
			/* 279 */   "MINUS",
			/* 280 */   "MINUSASSIGN",
			/* 281 */   "MINUSMINUS",
			/* 282 */   "PERCENT",
			/* 283 */   "PERCENTASSIGN",
			/* 284 */   "NOT",
			/* 285 */   "NOTEQUAL",
			/* 286 */   "OROR",
			/* 287 */   "PLUS",
			/* 288 */   "PLUSASSIGN",
			/* 289 */   "PLUSPLUS",
			/* 290 */   "ABSASSIGN",
			/* 291 */   "<invalid>",
			/* 292 */   "QUESTION_MARK",
			/* 293 */   "RIGHTBRACE",
			/* 294 */   "RIGHTPAREN",
			/* 295 */   "RIGHTBRACKET",
			/* 296 */   "<invalid>",
			/* 297 */   "SEMICOLON",
			/* 298 */   "SHIFTLEFT",
			/* 299 */   "SHIFTLEFTASSIGN",
			/* 300 */   "SHIFTRIGHT",
			/* 301 */   "SHIFTRIGHTASSIGN",
			/* 302 */   "STAR",
			/* 303 */   "COMPL",
			/* 304 */   "STARASSIGN",
			/* 305 */   "ASM",
			/* 306 */   "BYTE",
			/* 307 */   "BOOL",
			/* 308 */   "FALSE",
			/* 309 */   "TRUE",
			/* 310 */   "BREAK",
			/* 311 */   "CASE",
			/* 312 */   "MUTEX",
			/* 313 */   "CHAR",
			/* 314 */   "PRIORITY",
			/* 315 */   "CONST",
			/* 316 */   "REPEAT",
			/* 317 */   "CONTINUE",
			/* 318 */   "DEFAULT",
			/* 319 */   "SAFECALL",
			/* 320 */   "DO",
			/* 321 */   "START",
			/* 322 */   "STOP",
			/* 323 */   "ELSE",
			/* 324 */   "ENUM",
			/* 325 */   "STRING",
			/* 326 */   "SUB",
			/* 327 */   "TASK",
			/* 328 */   "FLOAT",
			/* 329 */   "FOR",
			/* 330 */   "UNTIL",
			/* 331 */   "GOTO",
			/* 332 */   "IF",
			/* 333 */   "INLINE",
			/* 334 */   "INT",
			/* 335 */   "LONG",
			/* 336 */   "<invalid>",
			/* 337 */   "<invalid>",
			/* 338 */   "<invalid>",
			/* 339 */   "<invalid>",
			/* 340 */   "<invalid>",
			/* 341 */   "<invalid>",
			/* 342 */   "<invalid>",
			/* 343 */   "<invalid>",
			/* 344 */   "<invalid>",
			/* 345 */   "RETURN",
			/* 346 */   "SHORT",
			/* 347 */   "<invalid>",
			/* 348 */   "SIZEOF",
			/* 349 */   "STATIC",
			/* 350 */   "<invalid>",
			/* 351 */   "STRUCT",
			/* 352 */   "SWITCH",
			/* 353 */   "<invalid>",
			/* 354 */   "<invalid>",
			/* 355 */   "<invalid>",
			/* 356 */   "<invalid>",
			/* 357 */   "TYPEDEF",
			/* 358 */   "<invalid>",
			/* 359 */   "<invalid>",
			/* 360 */   "<invalid>",
			/* 361 */   "UNSIGNED",
			/* 362 */   "<invalid>",
			/* 363 */   "<invalid>",
			/* 364 */   "VOID",
			/* 365 */   "WHILE",
			/* 366 */   "<invalid>",
			/* 367 */   "<invalid>",
			/* 368 */   "PP_DEFINE",
			/* 369 */   "PP_IF",
			/* 370 */   "PP_IFDEF",
			/* 371 */   "PP_IFNDEF",
			/* 372 */   "PP_ELSE",
			/* 373 */   "PP_ELIF",
			/* 374 */   "PP_ENDIF",
			/* 375 */   "PP_ERROR",
			/* 376 */   "PP_LINE",
			/* 377 */   "PP_PRAGMA",
			/* 378 */   "PP_UNDEF",
			/* 379 */   "PP_WARNING",
			/* 380 */   "IDENTIFIER",
			/* 381 */   "<invalid>",
			/* 382 */   "<invalid>",
			/* 383 */   "<invalid>",
			/* 384 */   "INTLIT",
			/* 385 */   "<invalid>",
			/* 386 */   "FLOATLIT",
			/* 387 */   "CCOMMENT",
			/* 388 */   "CPPCOMMENT",
			/* 389 */   "CHARLIT",
			/* 390 */   "STRINGLIT",
			/* 391 */   "CONTLINE",
			/* 392 */   "SPACE",
			/* 393 */   "<invalid>",
			/* 394 */   "NEWLINE",
			/* 395 */   "POUND_POUND",
			/* 396 */   "POUND",
			/* 397 */   "ANY",
			/* 398 */   "PP_INCLUDE",
			/* 399 */   "PP_QHEADER",
			/* 400 */   "<invalid>",
			/* 401 */   "EOF",
			/* 402 */   "EOI",
			/* 403 */   "PP_NUMBER",
			/* 404 */   "<invalid>",
			/* 405 */   "<invalid>",
			/* 406 */   "<invalid>",
			/* 407 */   "<invalid>",
			/* 408 */   "<invalid>",
			/* 409 */   "<invalid>",
			/* 410 */   "<invalid>",
			/* 411 */   "<invalid>",
			/* 412 */   "<invalid>",
			/* 413 */   "<invalid>",
			/* 414 */   "<invalid>",
			/* 415 */   "<invalid>",
			/* 416 */   "<invalid>",
			/* 417 */   "<invalid>",
			/* 418 */   "<invalid>",
			/* 419 */   "<invalid>",
			/* 420 */   "<invalid>",
			/* 421 */   "<invalid>",
			/* 422 */   "<invalid>",
			/* 423 */   "<invalid>",
			/* 424 */   "<invalid>",
			/* 425 */   "<invalid>",
			/* 426 */   "<invalid>",
			/* 427 */   "<invalid>",
			/* 428 */   "<invalid>",
			/* 429 */   "<invalid>",
			/* 430 */   "<invalid>",
			/* 431 */   "<invalid>",
			/* 432 */   "<invalid>",
	};

	// make sure, I have not forgotten any commas (as I did more than once)
	BOOST_STATIC_ASSERT(
			sizeof(tok_names) / sizeof(tok_names[0]) == T2_LAST_TOKEN - T2_FIRST_TOKEN
	);

	unsigned int id = BASEID_FROM_TOKEN2(tokid) - T2_FIRST_TOKEN;
	return (id < T2_LAST_TOKEN - T2_FIRST_TOKEN) ? tok_names[id] : "<UnknownToken>";
}

///////////////////////////////////////////////////////////////////////////////
//  return a token name
char const *
get_token_value(token2_id tokid) {
//  Table of token values
//
//      Please note that the sequence of token names must match the sequence of
//      token id's defined in then enum token_id above.
	static char const *tok_values[] = {
			/* 256 */   "&",
			/* 257 */   "&&",
			/* 258 */   "=",
			/* 259 */   "&=",
			/* 260 */   "|",
			/* 261 */   "|=",
			/* 262 */   "^",
			/* 263 */   "^=",
			/* 264 */   ",",
			/* 265 */   ":",
			/* 266 */   "/",
			/* 267 */   "/=",
			/* 268 */   ".",
			/* 269 */   "+-=",
			/* 270 */   "...",
			/* 271 */   "==",
			/* 272 */   ">",
			/* 273 */   ">=",
			/* 274 */   "{",
			/* 275 */   "<",
			/* 276 */   "<=",
			/* 277 */   "(",
			/* 278 */   "[",
			/* 279 */   "-",
			/* 280 */   "-=",
			/* 281 */   "--",
			/* 282 */   "%",
			/* 283 */   "%=",
			/* 284 */   "!",
			/* 285 */   "!=",
			/* 286 */   "||",
			/* 287 */   "+",
			/* 288 */   "+=",
			/* 289 */   "++",
			/* 290 */   "||=",
			/* 291 */   "",
			/* 292 */   "?",
			/* 293 */   "}",
			/* 294 */   ")",
			/* 295 */   "]",
			/* 296 */   "",
			/* 297 */   ";",
			/* 298 */   "<<",
			/* 299 */   "<<=",
			/* 300 */   ">>",
			/* 301 */   ">>=",
			/* 302 */   "*",
			/* 303 */   "~",
			/* 304 */   "*=",
			/* 305 */   "asm",
			/* 306 */   "byte",
			/* 307 */   "bool",
			/* 308 */   "false",
			/* 309 */   "true",
			/* 310 */   "break",
			/* 311 */   "case",
			/* 312 */   "mutex",
			/* 313 */   "char",
			/* 314 */   "priority",
			/* 315 */   "const",
			/* 316 */   "repeat",
			/* 317 */   "continue",
			/* 318 */   "default",
			/* 319 */   "safecall",
			/* 320 */   "do",
			/* 321 */   "start",
			/* 322 */   "stop",
			/* 323 */   "else",
			/* 324 */   "enum",
			/* 325 */   "string",
			/* 326 */   "sub",
			/* 327 */   "tas",
			/* 328 */   "float",
			/* 329 */   "for",
			/* 330 */   "until",
			/* 331 */   "goto",
			/* 332 */   "if",
			/* 333 */   "inline",
			/* 334 */   "int",
			/* 335 */   "long",
			/* 336 */   "",
			/* 337 */   "",
			/* 338 */   "",
			/* 339 */   "",
			/* 340 */   "",
			/* 341 */   "",
			/* 342 */   "",
			/* 343 */   "",
			/* 344 */   "",
			/* 345 */   "return",
			/* 346 */   "short",
			/* 347 */   "",
			/* 348 */   "SizeOf",
			/* 349 */   "static",
			/* 350 */   "",
			/* 351 */   "struct",
			/* 352 */   "switch",
			/* 353 */   "",
			/* 354 */   "",
			/* 355 */   "",
			/* 356 */   "",
			/* 357 */   "typedef",
			/* 358 */   "",
			/* 359 */   "",
			/* 360 */   "",
			/* 361 */   "unsigned",
			/* 362 */   "",
			/* 363 */   "",
			/* 364 */   "void",
			/* 365 */   "while",
			/* 366 */   "",
			/* 367 */   "",
			/* 368 */   "#define",
			/* 369 */   "#if",
			/* 370 */   "#ifdef",
			/* 371 */   "#ifndef",
			/* 372 */   "#else",
			/* 373 */   "#elif",
			/* 374 */   "#endif",
			/* 375 */   "#error",
			/* 376 */   "#line",
			/* 377 */   "#pragma",
			/* 378 */   "#undef",
			/* 379 */   "#warning",
			/* 380 */   "",   // identifier
			/* 381 */   "",   // octalint
			/* 382 */   "",   // decimalint
			/* 383 */   "",   // hexlit
			/* 384 */   "",   // intlit
			/* 385 */   "",   // longintlit (not valid anymore)
			/* 386 */   "",   // floatlit
			/* 387 */   "",   // ccomment
			/* 388 */   "",   // cppcomment
			/* 389 */   "",   // charlit
			/* 390 */   "",   // stringlit
			/* 391 */   "",   // contline
			/* 392 */   "",   // space
			/* 393 */   "",   // space2
			/* 394 */   "\n",
			/* 395 */   "##",
			/* 396 */   "#",
			/* 397 */   "",   // any
			/* 398 */   "#include",
			/* 399 */   "#include",
			/* 400 */   "",
			/* 401 */   "",   // eof
			/* 402 */   "",   // eoi
			/* 403 */   "",   // pp-number
			/* 404 */   "",
			/* 405 */   "",
			/* 406 */   "",
			/* 407 */   "",
			/* 408 */   "",
			/* 409 */   "",
			/* 410 */   "",
			/* 411 */   "",
			/* 412 */   "",
			/* 413 */   "",
			/* 414 */   "",
			/* 415 */   "",
			/* 416 */   "",
			/* 417 */   "",
			/* 418 */   "",
			/* 419 */   "",
			/* 420 */   "",
			/* 421 */   "",
			/* 422 */   "",
			/* 423 */   "",
			/* 424 */   "",
			/* 425 */   "",
			/* 426 */   "",
			/* 427 */   "",
			/* 428 */   "",
			/* 429 */   "",
			/* 430 */   "",
			/* 431 */   "",
			/* 432 */   "",
	};

	// make sure, I have not forgotten any commas (as I did more than once)
	BOOST_STATIC_ASSERT(
			sizeof(tok_values) / sizeof(tok_values[0]) == T2_LAST_TOKEN - T2_FIRST_TOKEN
	);

	unsigned int id = BASEID_FROM_TOKEN2(tokid) - T2_FIRST_TOKEN;
	return (id < T2_LAST_TOKEN - T2_FIRST_TOKEN) ? tok_values[id] : "<UnknownToken>";
}

///////////////////////////////////////////////////////////////////////////////
}   // namespace wave
}   // namespace boost

// the suffix header occurs after all of the code
#ifdef BOOST_HAS_ABI_HEADERS
#include BOOST_ABI_SUFFIX
#endif


