/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Typedefs for lexing NXC with SLex lexer and Wave preprocessor.
 */

#ifndef CUSTOM_ID_TYPES_HPP
#define CUSTOM_ID_TYPES_HPP

#include <boost/wave/util/file_position.hpp>
#include <boost/wave/cpp_iteration_context.hpp>
#include <boost/wave/cpp_context.hpp>
#include "slex_token.hpp"
#include "slex_iterator.hpp"
#include "hooks.hpp"

typedef boost::wave::util::file_position_type               file_pos_t;
typedef boost::wave::cpplexer::slex_token<file_pos_t>       token_t;
typedef boost::wave::cpplexer::slex::slex_iterator<token_t> lexer_t;
typedef boost::wave::context<std::string::iterator,
    lexer_t,
    boost::wave::iteration_context_policies::load_file_to_string,
    boost::wave::nxc_hooks<token_t>>                        pp_ctx_t;
typedef pp_ctx_t::iterator_type                             pp_iter_t;



#endif //CUSTOM_ID_TYPES_HPP
