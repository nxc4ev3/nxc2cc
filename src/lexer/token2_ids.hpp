/**
 * NXC2CC - the NXC to C transpiler
 * 
 * 
 * A generic NXC lexer token IDs.
 * 
 * Copyright (C) 2001-2012  Hartmut Kaiser
 *           (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * 
 * Authors: Hartmut Kaiser
 *          Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */

#ifndef LEXER_TOKEN2_IDS_HPP
#define LEXER_TOKEN2_IDS_HPP

#include <string>


///////////////////////////////////////////////////////////////////////////////
namespace boost {
namespace wave {

///////////////////////////////////////////////////////////////////////////////
//  assemble tokenids
#define TOKEN2_FROM_ID(id, cat)   ((id) | (cat))
#define ID_FROM_TOKEN2(tok)       ((tok) & ~T2_TokenTypeMask)
#define BASEID_FROM_TOKEN2(tok)   ((tok) & ~T2_ExtTokenTypeMask)

///////////////////////////////////////////////////////////////////////////////
//  the token_category helps to classify the different token types 
enum token2_category : uint32_t {
    T2_IdentifierTokenType         = 0x10080000,
    T2_ParameterTokenType          = 0x11080000,
    T2_ExtParameterTokenType       = 0x11180000,
    T2_KeywordTokenType            = 0x20080000,
    T2_OperatorTokenType           = 0x30080000,
    T2_LiteralTokenType            = 0x40080000,
    T2_IntegerLiteralTokenType     = 0x41080000,
    T2_FloatingLiteralTokenType    = 0x42080000,
    T2_StringLiteralTokenType      = 0x43080000,
    T2_CharacterLiteralTokenType   = 0x44080000,
    T2_BoolLiteralTokenType        = 0x45080000,
    T2_PPTokenType                 = 0x50080000,
    T2_PPConditionalTokenType      = 0x50880000,

    T2_UnknownTokenType            = 0xA0000000,
    T2_EOLTokenType                = 0xB0000000,
    T2_EOFTokenType                = 0xC0000000,
    T2_WhiteSpaceTokenType         = 0xD0000000,
    T2_InternalTokenType           = 0xE0080000,

    T2_TokenTypeMask               = 0xFF000000,
    T2_AltTokenType                = 0x00100000,
    T2_PPTokenFlag                 = 0x00080000,   // these are 'real' pp-tokens
    T2_ExtTokenTypeMask            = 0xFFF00000,
    T2_ExtTokenOnlyMask            = 0x00F00000,
    T2_TokenValueMask              = 0x000FFFFF,
    T2_MainTokenMask               = 0xFF0FFFFF    // T2_TokenTypeMask|T2_TokenValueMask
};
///////////////////////////////////////////////////////////////////////////////
//  the token_id assigns unique numbers to the different C++ lexemes
enum token2_id {
    T2_UNKNOWN      = 0,
    T2_FIRST_TOKEN  = 256,
    T2_AND          = TOKEN2_FROM_ID(T2_FIRST_TOKEN, T2_OperatorTokenType),
    T2_ANDAND       = TOKEN2_FROM_ID(257, T2_OperatorTokenType),
    T2_ASSIGN       = TOKEN2_FROM_ID(258, T2_OperatorTokenType),
    T2_ANDASSIGN    = TOKEN2_FROM_ID(259, T2_OperatorTokenType),
    T2_OR           = TOKEN2_FROM_ID(260, T2_OperatorTokenType),
    T2_ORASSIGN     = TOKEN2_FROM_ID(261, T2_OperatorTokenType),
    T2_XOR          = TOKEN2_FROM_ID(262, T2_OperatorTokenType),
    T2_XORASSIGN    = TOKEN2_FROM_ID(263, T2_OperatorTokenType),
    T2_COMMA        = TOKEN2_FROM_ID(264, T2_OperatorTokenType),
    T2_COLON        = TOKEN2_FROM_ID(265, T2_OperatorTokenType),
    T2_DIVIDE       = TOKEN2_FROM_ID(266, T2_OperatorTokenType),
    T2_DIVIDEASSIGN = TOKEN2_FROM_ID(267, T2_OperatorTokenType),
    T2_DOT          = TOKEN2_FROM_ID(268, T2_OperatorTokenType),
	T2_SIGNASSIGN   = TOKEN2_FROM_ID(269, T2_OperatorTokenType),
    T2_ELLIPSIS     = TOKEN2_FROM_ID(270, T2_OperatorTokenType),
    T2_EQUAL        = TOKEN2_FROM_ID(271, T2_OperatorTokenType),
    T2_GREATER      = TOKEN2_FROM_ID(272, T2_OperatorTokenType),
    T2_GREATEREQUAL = TOKEN2_FROM_ID(273, T2_OperatorTokenType),
    T2_LEFTBRACE    = TOKEN2_FROM_ID(274, T2_OperatorTokenType),
    T2_LESS         = TOKEN2_FROM_ID(275, T2_OperatorTokenType),
    T2_LESSEQUAL    = TOKEN2_FROM_ID(276, T2_OperatorTokenType),
    T2_LEFTPAREN    = TOKEN2_FROM_ID(277, T2_OperatorTokenType),
    T2_LEFTBRACKET  = TOKEN2_FROM_ID(278, T2_OperatorTokenType),
    T2_MINUS        = TOKEN2_FROM_ID(279, T2_OperatorTokenType),
    T2_MINUSASSIGN  = TOKEN2_FROM_ID(280, T2_OperatorTokenType),
    T2_MINUSMINUS   = TOKEN2_FROM_ID(281, T2_OperatorTokenType),
    T2_PERCENT      = TOKEN2_FROM_ID(282, T2_OperatorTokenType),
    T2_PERCENTASSIGN = TOKEN2_FROM_ID(283, T2_OperatorTokenType),
    T2_NOT          = TOKEN2_FROM_ID(284, T2_OperatorTokenType),
    T2_NOTEQUAL     = TOKEN2_FROM_ID(285, T2_OperatorTokenType),
    T2_OROR         = TOKEN2_FROM_ID(286, T2_OperatorTokenType),
    T2_PLUS         = TOKEN2_FROM_ID(287, T2_OperatorTokenType),
    T2_PLUSASSIGN   = TOKEN2_FROM_ID(288, T2_OperatorTokenType),
    T2_PLUSPLUS     = TOKEN2_FROM_ID(289, T2_OperatorTokenType),
	T2_ABSASSIGN    = TOKEN2_FROM_ID(290, T2_OperatorTokenType),
    // 291
    T2_QUESTION_MARK = TOKEN2_FROM_ID(292, T2_OperatorTokenType),
    T2_RIGHTBRACE   = TOKEN2_FROM_ID(293, T2_OperatorTokenType),
    T2_RIGHTPAREN   = TOKEN2_FROM_ID(294, T2_OperatorTokenType),
    T2_RIGHTBRACKET = TOKEN2_FROM_ID(295, T2_OperatorTokenType),
    // 296
    T2_SEMICOLON    = TOKEN2_FROM_ID(297, T2_OperatorTokenType),
    T2_SHIFTLEFT    = TOKEN2_FROM_ID(298, T2_OperatorTokenType),
    T2_SHIFTLEFTASSIGN = TOKEN2_FROM_ID(299, T2_OperatorTokenType),
    T2_SHIFTRIGHT   = TOKEN2_FROM_ID(300, T2_OperatorTokenType),
    T2_SHIFTRIGHTASSIGN = TOKEN2_FROM_ID(301, T2_OperatorTokenType),
    T2_STAR         = TOKEN2_FROM_ID(302, T2_OperatorTokenType),
    T2_COMPL        = TOKEN2_FROM_ID(303, T2_OperatorTokenType),
    T2_STARASSIGN   = TOKEN2_FROM_ID(304, T2_OperatorTokenType),


    T2_ASM          = TOKEN2_FROM_ID(305, T2_KeywordTokenType),
	T2_BYTE         = TOKEN2_FROM_ID(306, T2_KeywordTokenType),
    T2_BOOL         = TOKEN2_FROM_ID(307, T2_KeywordTokenType),
    T2_FALSE        = TOKEN2_FROM_ID(308, T2_BoolLiteralTokenType),
    T2_TRUE         = TOKEN2_FROM_ID(309, T2_BoolLiteralTokenType),
    T2_BREAK        = TOKEN2_FROM_ID(310, T2_KeywordTokenType),
    T2_CASE         = TOKEN2_FROM_ID(311, T2_KeywordTokenType),
	T2_MUTEX        = TOKEN2_FROM_ID(312, T2_KeywordTokenType),
    T2_CHAR         = TOKEN2_FROM_ID(313, T2_KeywordTokenType),
	T2_PRIORITY     = TOKEN2_FROM_ID(314, T2_KeywordTokenType),
    T2_CONST        = TOKEN2_FROM_ID(315, T2_KeywordTokenType),
	T2_REPEAT       = TOKEN2_FROM_ID(316, T2_KeywordTokenType),
    T2_CONTINUE     = TOKEN2_FROM_ID(317, T2_KeywordTokenType),
    T2_DEFAULT      = TOKEN2_FROM_ID(318, T2_KeywordTokenType),
	T2_SAFECALL     = TOKEN2_FROM_ID(319, T2_KeywordTokenType),
    T2_DO           = TOKEN2_FROM_ID(320, T2_KeywordTokenType),
	T2_START        = TOKEN2_FROM_ID(321, T2_KeywordTokenType),
	T2_STOP         = TOKEN2_FROM_ID(322, T2_KeywordTokenType),
    T2_ELSE         = TOKEN2_FROM_ID(323, T2_KeywordTokenType),
    T2_ENUM         = TOKEN2_FROM_ID(324, T2_KeywordTokenType),
	T2_STRING       = TOKEN2_FROM_ID(325, T2_KeywordTokenType),
	T2_SUB          = TOKEN2_FROM_ID(326, T2_KeywordTokenType),
	T2_TASK         = TOKEN2_FROM_ID(327, T2_KeywordTokenType),
    T2_FLOAT        = TOKEN2_FROM_ID(328, T2_KeywordTokenType),
    T2_FOR          = TOKEN2_FROM_ID(329, T2_KeywordTokenType),
	T2_UNTIL        = TOKEN2_FROM_ID(330, T2_KeywordTokenType),
    T2_GOTO         = TOKEN2_FROM_ID(331, T2_KeywordTokenType),
    T2_IF           = TOKEN2_FROM_ID(332, T2_KeywordTokenType),
    T2_INLINE       = TOKEN2_FROM_ID(333, T2_KeywordTokenType),
    T2_INT          = TOKEN2_FROM_ID(334, T2_KeywordTokenType),
    T2_LONG         = TOKEN2_FROM_ID(335, T2_KeywordTokenType),
    // 336 - 344
    T2_RETURN       = TOKEN2_FROM_ID(345, T2_KeywordTokenType),
    T2_SHORT        = TOKEN2_FROM_ID(346, T2_KeywordTokenType),
    // 347
    T2_SIZEOF       = TOKEN2_FROM_ID(348, T2_OperatorTokenType),
    T2_STATIC       = TOKEN2_FROM_ID(349, T2_KeywordTokenType),
    // 350
    T2_STRUCT       = TOKEN2_FROM_ID(351, T2_KeywordTokenType),
    T2_SWITCH       = TOKEN2_FROM_ID(352, T2_KeywordTokenType),
    // 353 - 356
    T2_TYPEDEF      = TOKEN2_FROM_ID(357, T2_KeywordTokenType),
    // 358 - 360
    T2_UNSIGNED     = TOKEN2_FROM_ID(361, T2_KeywordTokenType),
    // 362 - 363
    T2_VOID         = TOKEN2_FROM_ID(364, T2_KeywordTokenType),
    T2_WHILE        = TOKEN2_FROM_ID(365, T2_KeywordTokenType),

    T2_PP_DEFINE    = TOKEN2_FROM_ID(368, T2_PPTokenType),
    T2_PP_IF        = TOKEN2_FROM_ID(369, T2_PPConditionalTokenType),
    T2_PP_IFDEF     = TOKEN2_FROM_ID(370, T2_PPConditionalTokenType),
    T2_PP_IFNDEF    = TOKEN2_FROM_ID(371, T2_PPConditionalTokenType),
    T2_PP_ELSE      = TOKEN2_FROM_ID(372, T2_PPConditionalTokenType),
    T2_PP_ELIF      = TOKEN2_FROM_ID(373, T2_PPConditionalTokenType),
    T2_PP_ENDIF     = TOKEN2_FROM_ID(374, T2_PPConditionalTokenType),
    T2_PP_ERROR     = TOKEN2_FROM_ID(375, T2_PPTokenType),
    T2_PP_LINE      = TOKEN2_FROM_ID(376, T2_PPTokenType),
    T2_PP_PRAGMA    = TOKEN2_FROM_ID(377, T2_PPTokenType),
    T2_PP_UNDEF     = TOKEN2_FROM_ID(378, T2_PPTokenType),
    T2_PP_WARNING   = TOKEN2_FROM_ID(379, T2_PPTokenType),


    T2_IDENTIFIER   = TOKEN2_FROM_ID(380, T2_IdentifierTokenType),
	// 381 - 383
    T2_INTLIT       = TOKEN2_FROM_ID(384, T2_IntegerLiteralTokenType),
	// 385
    T2_FLOATLIT     = TOKEN2_FROM_ID(386, T2_FloatingLiteralTokenType),
    T2_CCOMMENT     = TOKEN2_FROM_ID(387, T2_WhiteSpaceTokenType|T2_AltTokenType),
    T2_CPPCOMMENT   = TOKEN2_FROM_ID(388, T2_WhiteSpaceTokenType|T2_AltTokenType),
    T2_CHARLIT      = TOKEN2_FROM_ID(389, T2_CharacterLiteralTokenType),
    T2_STRINGLIT    = TOKEN2_FROM_ID(390, T2_StringLiteralTokenType),
    T2_CONTLINE     = TOKEN2_FROM_ID(391, T2_EOLTokenType),
    T2_SPACE        = TOKEN2_FROM_ID(392, T2_WhiteSpaceTokenType),
	// 393
    T2_NEWLINE      = TOKEN2_FROM_ID(394, T2_EOLTokenType),
    T2_GENERATEDNEWLINE      = TOKEN2_FROM_ID(394, T2_EOLTokenType|T2_AltTokenType),
    T2_POUND_POUND           = TOKEN2_FROM_ID(395, T2_OperatorTokenType),
    T2_POUND                 = TOKEN2_FROM_ID(396, T2_OperatorTokenType),
    T2_ANY          = TOKEN2_FROM_ID(397, T2_UnknownTokenType),
    T2_PP_INCLUDE   = TOKEN2_FROM_ID(398, T2_PPTokenType),
    T2_PP_QHEADER   = TOKEN2_FROM_ID(399, T2_PPTokenType),
    T2_EOF          = TOKEN2_FROM_ID(401, T2_EOFTokenType),      // end of file reached
    T2_EOI          = TOKEN2_FROM_ID(402, T2_EOFTokenType),      // end of input reached
    T2_PP_NUMBER    = TOKEN2_FROM_ID(403, T2_InternalTokenType),
// 404 - 432

    T2_LAST_TOKEN_ID = 433,
    T2_LAST_TOKEN = 433,

// pseudo tokens to help streamlining macro replacement, these should not 
// returned from the lexer nor should these be returned from the pp-iterator
    T2_NONREPLACABLE_IDENTIFIER = TOKEN2_FROM_ID(T2_LAST_TOKEN+1, T2_IdentifierTokenType),
    T2_PLACEHOLDER = TOKEN2_FROM_ID(T2_LAST_TOKEN+2, T2_WhiteSpaceTokenType),
    T2_PLACEMARKER = TOKEN2_FROM_ID(T2_LAST_TOKEN+3, T2_InternalTokenType),
    T2_PARAMETERBASE = TOKEN2_FROM_ID(T2_LAST_TOKEN+4, T2_ParameterTokenType),
    T2_EXTPARAMETERBASE = TOKEN2_FROM_ID(T2_LAST_TOKEN+4, T2_ExtParameterTokenType)
};

///////////////////////////////////////////////////////////////////////////////
//  redefine the TOKEN2_FROM_ID macro to be more type safe
#undef TOKEN2_FROM_ID
#define TOKEN2_FROM_ID(id, cat)   boost::wave::token2_id((id) | (cat))

#undef ID_FROM_TOKEN2
#define ID_FROM_TOKEN2(tok)                                                    \
    boost::wave::token2_id((tok) &                                             \
        ~(boost::wave::T2_TokenTypeMask|boost::wave::T2_PPTokenFlag))               \
    /**/

#undef BASEID_FROM_TOKEN2
#define BASEID_FROM_TOKEN2(tok)                                                \
    boost::wave::token2_id((tok) &                                             \
         ~(boost::wave::T2_ExtTokenTypeMask|boost::wave::T2_PPTokenFlag))           \
    /**/
#define BASE_TOKEN2(tok)                                                       \
    boost::wave::token2_id((tok) & boost::wave::T2_MainTokenMask)                 \
    /**/
#define CATEGORY_FROM_TOKEN2(tok) ((tok) & boost::wave::T2_TokenTypeMask)
#define EXTCATEGORY_FROM_TOKEN2(tok) ((tok) & boost::wave::T2_ExtTokenTypeMask)
#define IS_CATEGORY2(tok, cat)                                                 \
    (CATEGORY_FROM_TOKEN2(tok) == CATEGORY_FROM_TOKEN2(cat))   \
    /**/
#define IS_EXTCATEGORY2(tok, cat)                                              \
    (EXTCATEGORY_FROM_TOKEN2(tok) == EXTCATEGORY_FROM_TOKEN2(cat)) \
    /**/

///////////////////////////////////////////////////////////////////////////////
// verify whether the given token(-id) represents a valid pp_token
inline bool is_pp_token2(boost::wave::token2_id id)
{
    return (id & boost::wave::T2_PPTokenFlag) != 0;
}

template <typename TokenT>
inline bool is_pp_token2(TokenT const& tok)
{
    return is_pp_token2(boost::wave::token2_id(tok));
}

///////////////////////////////////////////////////////////////////////////////
}   // namespace wave
}   // namespace boost

#endif // #ifndef LEXER_TOKEN2_IDS_HPP
