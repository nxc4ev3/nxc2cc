/**
 * NXC2CC - the NXC to C transpiler
 * 
 * 
 * SLex (Spirit Lex) based C++ lexer
 * 
 * Copyright (C) 2001-2012  Hartmut Kaiser
 *           (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * 
 * Authors: Hartmut Kaiser
 *          Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 * 
 */

#if !defined(SLEX_LEXER_HPP_5E8E1DF0_BB41_4938_B7E5_A4BB68222FF6_INCLUDED)
#define SLEX_LEXER_HPP_5E8E1DF0_BB41_4938_B7E5_A4BB68222FF6_INCLUDED

#include <string>

#if defined(BOOST_SPIRIT_DEBUG)
#include <iostream>
#endif // defined(BOOST_SPIRIT_DEBUG)

#include <boost/assert.hpp>
#include <boost/spirit/include/classic_core.hpp>

#include "../token2_ids.hpp"
#include <boost/wave/wave_config.hpp>
#include <boost/wave/language_support.hpp>
#include <boost/wave/util/file_position.hpp>
#include <boost/wave/util/time_conversion_helper.hpp>
#include <boost/wave/cpplexer/validate_universal_char.hpp>
#include <boost/wave/cpplexer/convert_trigraphs.hpp>
#include <boost/wave/cpplexer/cpplexer_exceptions.hpp>

#if BOOST_WAVE_SUPPORT_PRAGMA_ONCE != 0

#include <boost/wave/cpplexer/detect_include_guards.hpp>

#endif

#include <boost/wave/cpplexer/cpp_lex_interface.hpp>

#include "../slex_interface.hpp"
#include "../slex_token.hpp"
#include "../slex_iterator.hpp"

#include "lexer.hpp"   // "spirit/lexer.hpp"

#ifndef SLEX_DFA_FILENAME
#define SLEX_DFA_FILENAME "nxc2cc_slex.dfa"
#endif

///////////////////////////////////////////////////////////////////////////////

namespace boost {
	namespace wave {
		namespace cpplexer {
			namespace slex {
				namespace lexer {

///////////////////////////////////////////////////////////////////////////////
//  The following numbers are the array sizes of the token regex's which we
//  need to specify to make the CW compiler happy (at least up to V9.5).

#define INIT_DATA_SIZE              118
#define INIT_DATA_PP_NUMBER_SIZE    2

///////////////////////////////////////////////////////////////////////////////
// 
//  encapsulation of the boost::spirit::classic::slex based cpp lexer
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  The following lexer_base class was necessary to workaround a CodeWarrior 
//  bug (at least up to CW V9.5).
template<typename IteratorT, typename PositionT>
class lexer_base
		: public boost::spirit::classic::lexer<
				boost::wave::util::position_iterator<IteratorT, PositionT> > {
protected:
	typedef boost::wave::util::position_iterator<IteratorT, PositionT>
			iterator_type;
	typedef typename std::iterator_traits<IteratorT>::value_type char_type;
	typedef boost::spirit::classic::lexer<iterator_type> base_type;

	lexer_base();

// initialization data (regular expressions for the token definitions)
	struct lexer_data {
		token2_id tokenid;                       // token data
		char_type const *tokenregex;            // associated token to match
		typename base_type::callback_t tokencb; // associated callback function
		unsigned int lexerstate;                // valid for lexer state
	};
};

///////////////////////////////////////////////////////////////////////////////
template<typename IteratorT, typename PositionT>
class lexer
		: public lexer_base<IteratorT, PositionT> {
public:
	typedef boost::wave::cpplexer::slex_token<PositionT> token_type;

	void init_dfa(boost::wave::language_support language);

// get time of last compilation
	static std::time_t get_compilation_time() { return compilation_time.get_time(); }

// helper for calculation of the time of last compilation
	static boost::wave::util::time_conversion_helper compilation_time;

private:
	typedef lexer_base<IteratorT, PositionT> base_type;

	static typename base_type::lexer_data const init_data[INIT_DATA_SIZE];          // common patterns
	static typename base_type::lexer_data const init_data_pp_number[INIT_DATA_PP_NUMBER_SIZE];  // pp-number only patterns
};

///////////////////////////////////////////////////////////////////////////////
//  data required for initialization of the lexer (token definitions)
#define OR                  "|"
#define esc(c)                "\\" c

// definition of some sub-token regexps to simplify the regex definitions
#define BLANK               "[ \\t]"
#define CCOMMENT            \
    esc("/") esc("*") "[^*]*" esc("*") "+" "(" "[^/*][^*]*" esc("*") "+" ")*" esc("/")

#define PPSPACE             "(" "(" BLANK ")" OR "(" CCOMMENT ")" ")*"

#define OCTALDIGIT          "[0-7]"
#define DIGIT               "[0-9]"
#define HEXDIGIT            "[0-9a-fA-F]"
#define OPTSIGN             "[-+]?"
#define EXPSTART            "(" "[eE]" "[-+]" ")"
#define EXPONENT            "(" "[eE]" OPTSIGN "[0-9]+" ")"
#define NONDIGIT            "[a-zA-Z_]"

#define INTEGER             \
    "(" "(" "(0x|0X)" HEXDIGIT "+" ")" OR "(" "0" OCTALDIGIT "*" ")" OR "(" "[1-9]" DIGIT "*" ")" ")"

#define INTEGER_SUFFIX      "(" "[uU]?" ")"

#define BACKSLASH           esc("\\")
#define ESCAPESEQ           "(" BACKSLASH "(" \
                                "[abfnrtv?'\"]" OR \
                                BACKSLASH OR \
                                "(" "x" HEXDIGIT "+" ")" OR \
                                "(" OCTALDIGIT OCTALDIGIT "?" OCTALDIGIT "?" ")" \
                            "))"
#define POUNDDEF            "(" "#" ")"
#define NEWLINEDEF          "(" "\n" OR "\r" OR "\r\n" ")"

#define INCLUDEDEF          "include"

#define PP_NUMBERDEF        esc(".") "?" DIGIT "(" DIGIT OR NONDIGIT OR EXPSTART OR esc(".") ")*"

///////////////////////////////////////////////////////////////////////////////
//  lexer state constants
#define LEXER_STATE_NORMAL  0
#define LEXER_STATE_PP      1

#define NUM_LEXER_STATES    1

//  helper for initializing token data
#define TOKEN_DATA(id, regex)                                                 \
        { T2_##id, regex, 0, LEXER_STATE_NORMAL }                              \
    /**/

#define TOKEN_DATA_EX(id, regex, callback)                                    \
        { T2_##id, regex, callback, LEXER_STATE_NORMAL }                       \
    /**/

///////////////////////////////////////////////////////////////////////////////
// common C++/C99 token definitions
template<typename IteratorT, typename PositionT>
typename lexer_base<IteratorT, PositionT>::lexer_data const
		lexer<IteratorT, PositionT>::init_data[INIT_DATA_SIZE] =
		{
				TOKEN_DATA(AND, "&"),
				TOKEN_DATA(ANDAND, "&&"),
				TOKEN_DATA(ASSIGN, "="),
				TOKEN_DATA(ANDASSIGN, "&="),
				TOKEN_DATA(OR, esc("|")),
				TOKEN_DATA(ORASSIGN, esc("|=")),
				TOKEN_DATA(ABSASSIGN, esc("|") esc("|=")),
				TOKEN_DATA(XOR, esc("^")),
				TOKEN_DATA(XORASSIGN, esc("^=")),
				TOKEN_DATA(COMMA, ","),
				TOKEN_DATA(COLON, ":"),
				TOKEN_DATA(DIVIDEASSIGN, esc("/=")),
				TOKEN_DATA(DIVIDE, esc("/")),
				TOKEN_DATA(DOT, esc(".")),
				TOKEN_DATA(ELLIPSIS, esc(".") esc(".") esc(".")),
				TOKEN_DATA(EQUAL, "=="),
				TOKEN_DATA(GREATER, ">"),
				TOKEN_DATA(GREATEREQUAL, ">="),
				TOKEN_DATA(LEFTBRACE, esc("{")),
				TOKEN_DATA(LESS, "<"),
				TOKEN_DATA(LESSEQUAL, "<="),
				TOKEN_DATA(LEFTPAREN, esc("(")),
				TOKEN_DATA(LEFTBRACKET, esc("[")),
				TOKEN_DATA(MINUS, esc("-")),
				TOKEN_DATA(MINUSASSIGN, esc("-=")),
				TOKEN_DATA(MINUSMINUS, esc("-") esc("-")),
				TOKEN_DATA(PERCENT, esc("%")),
				TOKEN_DATA(PERCENTASSIGN, esc("%=")),
				TOKEN_DATA(NOT, "!"),
				TOKEN_DATA(NOTEQUAL, "!="),
				TOKEN_DATA(OROR, esc("|") esc("|")),
				TOKEN_DATA(PLUS, esc("+")),
				TOKEN_DATA(PLUSASSIGN, esc("+=")),
				TOKEN_DATA(PLUSPLUS, esc("+") esc("+")),
				TOKEN_DATA(QUESTION_MARK, esc("?")),
				TOKEN_DATA(RIGHTBRACE, esc("}")),
				TOKEN_DATA(RIGHTPAREN, esc(")")),
				TOKEN_DATA(RIGHTBRACKET, esc("]")),
				TOKEN_DATA(SEMICOLON, ";"),
				TOKEN_DATA(SHIFTLEFT, "<<"),
				TOKEN_DATA(SHIFTLEFTASSIGN, "<<="),
				TOKEN_DATA(SHIFTRIGHT, ">>"),
				TOKEN_DATA(SHIFTRIGHTASSIGN, ">>="),
				TOKEN_DATA(SIGNASSIGN, esc("+-=")),
				TOKEN_DATA(STAR, esc("*")),
				TOKEN_DATA(COMPL, esc("~")),
				TOKEN_DATA(STARASSIGN, esc("*=")),
				TOKEN_DATA(ASM, "asm"),
				TOKEN_DATA(BYTE, "byte"),
				TOKEN_DATA(BOOL, "bool"),
				TOKEN_DATA(FALSE, "false"),
				TOKEN_DATA(TRUE, "true"),
				TOKEN_DATA(BREAK, "break"),
				TOKEN_DATA(CASE, "case"),
				TOKEN_DATA(CHAR, "char"),
				TOKEN_DATA(CONST, "const"),
				TOKEN_DATA(CONTINUE, "continue"),
				TOKEN_DATA(DEFAULT, "default"),
				TOKEN_DATA(DO, "do"),
				TOKEN_DATA(ELSE, "else"),
				TOKEN_DATA(ENUM, "enum"),
				TOKEN_DATA(FLOAT, "float"),
				TOKEN_DATA(FOR, "for"),
				TOKEN_DATA(GOTO, "goto"),
				TOKEN_DATA(IF, "if"),
				TOKEN_DATA(INLINE, "inline"),
				TOKEN_DATA(INT, "int"),
				TOKEN_DATA(LONG, "long"),
				TOKEN_DATA(MUTEX, "mutex"),
				TOKEN_DATA(PRIORITY, "priority"),
				TOKEN_DATA(REPEAT, "repeat"),
				TOKEN_DATA(RETURN, "return"),
				TOKEN_DATA(SAFECALL, "safecall"),
				TOKEN_DATA(SHORT, "short"),
				TOKEN_DATA(SIZEOF, "SizeOf"),
				TOKEN_DATA(STATIC, "static"),
				TOKEN_DATA(STRUCT, "struct"),
				TOKEN_DATA(STRING, "string"),
				TOKEN_DATA(START, "start"),
				TOKEN_DATA(STOP, "stop"),
				TOKEN_DATA(SUB, "sub"),
				TOKEN_DATA(SWITCH, "switch"),
				TOKEN_DATA(TASK, "task"),
				TOKEN_DATA(TYPEDEF, "typedef"),
				TOKEN_DATA(UNSIGNED, "unsigned"),
				TOKEN_DATA(UNTIL, "until"),
				TOKEN_DATA(VOID, "void"),
				TOKEN_DATA(WHILE, "while"),
				TOKEN_DATA(PP_DEFINE,   POUNDDEF PPSPACE "define"),
				TOKEN_DATA(PP_IF,       POUNDDEF PPSPACE "if"),
				TOKEN_DATA(PP_IFDEF,    POUNDDEF PPSPACE "ifdef"),
				TOKEN_DATA(PP_IFNDEF,   POUNDDEF PPSPACE "ifndef"),
				TOKEN_DATA(PP_ELSE,     POUNDDEF PPSPACE "else"),
				TOKEN_DATA(PP_ELIF,     POUNDDEF PPSPACE "elif"),
				TOKEN_DATA(PP_ENDIF,    POUNDDEF PPSPACE "endif"),
				TOKEN_DATA(PP_ERROR,    POUNDDEF PPSPACE "error"),
				TOKEN_DATA(PP_QHEADER,  POUNDDEF PPSPACE INCLUDEDEF
								PPSPACE esc("\"") "[^\\n\\r\"]+" esc("\"")),
				TOKEN_DATA(PP_INCLUDE,  POUNDDEF PPSPACE INCLUDEDEF PPSPACE),
				TOKEN_DATA(PP_LINE,     POUNDDEF PPSPACE "line"),
				TOKEN_DATA(PP_PRAGMA,   POUNDDEF PPSPACE "pragma"),
				TOKEN_DATA(PP_UNDEF,    POUNDDEF PPSPACE "undef"),
				TOKEN_DATA(PP_WARNING,  POUNDDEF PPSPACE "warning"),
				TOKEN_DATA(INTLIT, INTEGER INTEGER_SUFFIX "?"),
				TOKEN_DATA(FLOATLIT,
				           "("
						   "(" "(" DIGIT "*"
						           esc(".")
						           DIGIT "+"
						       ")" OR
							   "(" DIGIT "+"
						           esc(".")
						       ")" ")"
						           EXPONENT "?"
						   ")"     OR
						   "("     DIGIT "+"
						           EXPONENT ")"),
				TOKEN_DATA(CCOMMENT, CCOMMENT),
				TOKEN_DATA(CPPCOMMENT, esc("/") esc("/[^\\n\\r]*") NEWLINEDEF),
				TOKEN_DATA(CHARLIT,
						"'"
							"("	ESCAPESEQ OR "[^\\n\\r\\\\']" ")+"
						"'"),
				TOKEN_DATA(STRINGLIT,
				        esc("\"")
						    "(" ESCAPESEQ OR "[^\\n\\r\\\\\"]" ")*"
						esc("\"")),
				TOKEN_DATA(IDENTIFIER, "([a-zA-Z_])([a-zA-Z0-9_])*"),
				TOKEN_DATA(SPACE, "[ \t\v\f]+"),
				TOKEN_DATA(CONTLINE, esc("\\") "\n"),
				TOKEN_DATA(NEWLINE, NEWLINEDEF),
				TOKEN_DATA(POUND_POUND, "##"),
				TOKEN_DATA(POUND, "#"),
				TOKEN_DATA(ANY, "."),     // this should be the last recognized token
				{token2_id(0)}           // this should be the last entry
		};

///////////////////////////////////////////////////////////////////////////////
// PP-number token definitions
template<typename IteratorT, typename PositionT>
typename lexer_base<IteratorT, PositionT>::lexer_data const
		lexer<IteratorT, PositionT>::init_data_pp_number[INIT_DATA_PP_NUMBER_SIZE] =
		{
				TOKEN_DATA(PP_NUMBER, PP_NUMBERDEF),
				{token2_id(0)}       // this should be the last entry
		};

///////////////////////////////////////////////////////////////////////////////
//  undefine macros, required for regular expression definitions

#undef INCLUDEDEF
#undef POUNDDEF
#undef CCOMMENT
#undef PPSPACE
#undef DIGIT
#undef OCTALDIGIT
#undef HEXDIGIT
#undef NONDIGIT
#undef OPTSIGN
#undef EXPSTART
#undef EXPONENT
#undef INTEGER_SUFFIX
#undef INTEGER
#undef BACKSLASH
#undef ESCAPESEQ
#undef PP_NUMBERDEF

#undef esc
#undef OR

#undef TOKEN_DATA
#undef TOKEN_DATA_EX

///////////////////////////////////////////////////////////////////////////////
// initialize cpp lexer with token data
template<typename IteratorT, typename PositionT>
inline
lexer_base<IteratorT, PositionT>::lexer_base()
		:   base_type(NUM_LEXER_STATES) {
}

template<typename IteratorT, typename PositionT>
inline void
lexer<IteratorT, PositionT>::init_dfa(boost::wave::language_support lang) {
	if (this->has_compiled_dfa())
		return;

// if pp-numbers should be preferred, insert the corresponding rule first
	//if (boost::wave::need_prefer_pp_numbers(lang)) {
	if (false) {
		for (int j = 0; 0 != init_data_pp_number[j].tokenid; ++j) {
			this->register_regex(init_data_pp_number[j].tokenregex,
			                     init_data_pp_number[j].tokenid, init_data_pp_number[j].tokencb,
			                     init_data_pp_number[j].lexerstate);
		}
	}

	for (int i = 0; 0 != init_data[i].tokenid; ++i) {
		this->register_regex(init_data[i].tokenregex, init_data[i].tokenid,
		                     init_data[i].tokencb, init_data[i].lexerstate);
	}
}

///////////////////////////////////////////////////////////////////////////////
// get time of last compilation of this file
template<typename IteratorT, typename PositionT>
boost::wave::util::time_conversion_helper
lexer<IteratorT, PositionT>::compilation_time(__DATE__ " " __TIME__);

///////////////////////////////////////////////////////////////////////////////
}   // namespace lexer

///////////////////////////////////////////////////////////////////////////////
//  
template<typename IteratorT, typename PositionT>
inline void
init_lexer(lexer::lexer<IteratorT, PositionT> &lexer,
           boost::wave::language_support language, bool force_reinit = false) {
	if (lexer.has_compiled_dfa())
		return;     // nothing to do

	using std::ifstream;
	using std::ofstream;
	using std::ios;
	using std::cerr;
	using std::endl;

	ifstream dfa_in(SLEX_DFA_FILENAME, ios::in | ios::binary);

	lexer.init_dfa(language);
	if (force_reinit || !dfa_in.is_open() ||
	    !lexer.load(dfa_in, (long) lexer.get_compilation_time())) {
#if defined(BOOST_SPIRIT_DEBUG)
		cerr << "Compiling regular expressions for slex ...";
#endif // defined(BOOST_SPIRIT_DEBUG)

		dfa_in.close();
		lexer.create_dfa();

		ofstream dfa_out(SLEX_DFA_FILENAME, ios::out | ios::binary | ios::trunc);

		if (dfa_out.is_open())
			lexer.save(dfa_out, (long) lexer.get_compilation_time());

#if defined(BOOST_SPIRIT_DEBUG)
		cerr << " Done." << endl;
#endif // defined(BOOST_SPIRIT_DEBUG)
	}
}

///////////////////////////////////////////////////////////////////////////////
//  
//  lex_functor
//
///////////////////////////////////////////////////////////////////////////////

template<typename IteratorT, typename PositionT = wave::util::file_position_type>
class slex_functor
		: public slex_input_interface<
				typename lexer::lexer<IteratorT, PositionT>::token_type
		> {
public:

	typedef boost::wave::util::position_iterator<IteratorT, PositionT>
			iterator_type;
	typedef typename std::iterator_traits<IteratorT>::value_type char_type;
	typedef BOOST_WAVE_STRINGTYPE string_type;
	typedef typename lexer::lexer<IteratorT, PositionT>::token_type token_type;

	slex_functor(IteratorT const &first_, IteratorT const &last_,
	             PositionT const &pos_, boost::wave::language_support language_)
			: first(first_, last_, pos_), language(language_), at_eof(false) {
		// initialize lexer dfa tables
		init_lexer(lexer, language_);
	}

	virtual ~slex_functor() {}

// get the next token from the input stream
	token_type &get(token_type &result) {
		if (!at_eof) {
			do {
				// generate and return the next token
				std::string value;
				PositionT pos = first.get_position();   // begin of token position
				token2_id id = token2_id(lexer.next_token(first, last, &value));

				if ((token2_id)(-1) == id)
					id = T2_EOF;     // end of input reached

				string_type token_val(value.c_str());

				if (boost::wave::need_emit_contnewlines(language) ||
				    T2_CONTLINE != id) {
					//  The cast should avoid spurious warnings about missing case labels
					//  for the other token ids's.
					switch (static_cast<unsigned int>(id)) {
						case T2_IDENTIFIER:
							// test identifier characters for validity (throws if
							// invalid chars found)
							if (!boost::wave::need_no_character_validation(language)) {
								using boost::wave::cpplexer::impl::validate_identifier_name;
								validate_identifier_name(token_val,
								                         pos.get_line(), pos.get_column(),
								                         pos.get_file());
							}
							break;

						case T2_CHARLIT:
						case T2_STRINGLIT:
							// test literal characters for validity (throws if invalid
							// chars found)
							if (boost::wave::need_convert_trigraphs(language)) {
								using boost::wave::cpplexer::impl::convert_trigraphs;
								token_val = convert_trigraphs(token_val);
							}
							if (!boost::wave::need_no_character_validation(language)) {
								using boost::wave::cpplexer::impl::validate_literal;
								validate_literal(token_val, pos.get_line(), pos.get_column(), pos.get_file());
							}
							break;

						case T2_EOF:
							// T2_EOF is returned as a valid token, the next call will
							// return T2_EOI, i.e. the actual end of input
							at_eof = true;
							token_val.clear();
							break;
						default:break;
					}

					result = token_type(id, token_val, pos);
#if BOOST_WAVE_SUPPORT_PRAGMA_ONCE != 0
					return guards.detect_guard(result);
#else
					return result;
#endif
				}

				// skip the T2_CONTLINE token
			} while (true);
		}
		return result = token_type();   // return T2_EOI
	}

	void set_position(PositionT const &pos) {
		// set position has to change the file name and line number only
		first.get_position().set_file(pos.get_file());
		first.get_position().set_line(pos.get_line());
	}

#if BOOST_WAVE_SUPPORT_PRAGMA_ONCE != 0

	bool has_include_guards(std::string &guard_name) const { return guards.detected(guard_name); }

#endif

private:
	iterator_type first;
	iterator_type last;
	boost::wave::language_support language;
	static lexer::lexer<IteratorT, PositionT> lexer;   // needed only once

	bool at_eof;

#if BOOST_WAVE_SUPPORT_PRAGMA_ONCE != 0
	include_guards <token_type> guards;
#endif
};

template<typename IteratorT, typename PositionT>
lexer::lexer<IteratorT, PositionT> slex_functor<IteratorT, PositionT>::lexer;

#undef T2_EXTCHARLIT
#undef T2_EXTSTRINGLIT
#undef T2_EXTRAWSTRINGLIT

///////////////////////////////////////////////////////////////////////////////
//
//  The 'new_lexer' function allows the opaque generation of a new lexer object.
//  It is coupled to the iterator type to allow to decouple the lexer/iterator 
//  configurations at compile time.
//
//  This function is declared inside the cpp_slex_token.hpp file, which is 
//  referenced by the source file calling the lexer and the source file, which
//  instantiates the lex_functor. But it is defined here, so it will be 
//  instantiated only while compiling the source file, which instantiates the 
//  lex_functor. While the cpp_slex_token.hpp file may be included everywhere,
//  this file (cpp_slex_lexer.hpp) should be included only once. This allows
//  to decouple the lexer interface from the lexer implementation and reduces 
//  compilation time.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//  
//  The new_lexer_gen<>::new_lexer function (declared in cpp_slex_token.hpp)
//  should be defined inline, if the lex_functor shouldn't be instantiated 
//  separately from the lex_iterator.
//
//  Separate (explicit) instantiation helps to reduce compilation time.
//
///////////////////////////////////////////////////////////////////////////////

#if BOOST_WAVE_SEPARATE_LEXER_INSTANTIATION != 0
#define BOOST_WAVE_SLEX_NEW_LEXER_INLINE
#else
#define BOOST_WAVE_SLEX_NEW_LEXER_INLINE inline
#endif

template<typename IteratorT, typename PositionT>
BOOST_WAVE_SLEX_NEW_LEXER_INLINE
lex_input_interface <slex_token<PositionT>> *
new_lexer_gen<IteratorT, PositionT>::new_lexer(IteratorT const &first,
                                               IteratorT const &last, PositionT const &pos,
                                               boost::wave::language_support language) {
	return new slex_functor<IteratorT, PositionT>(first, last, pos,
	                                              language);
}

#undef BOOST_WAVE_SLEX_NEW_LEXER_INLINE

///////////////////////////////////////////////////////////////////////////////
}   // namespace slex
}   // namespace cpplexer
}   // namespace wave
}   // namespace boost

#endif // !defined(SLEX_LEXER_HPP_5E8E1DF0_BB41_4938_B7E5_A4BB68222FF6_INCLUDED)
