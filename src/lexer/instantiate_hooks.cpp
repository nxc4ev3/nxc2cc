/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Explicit instantiation of NXC hooks.
 */

#include <main.hpp>

#if BOOST_WAVE_SEPARATE_HOOK_INSTANTIATION != 0

#include "hooks.impl.hpp"
#include "slex_token.hpp"
#include "slex_iterator.hpp"
#include <boost/wave/cpp_context.hpp>


typedef boost::wave::util::file_position_type position_type;
typedef boost::wave::cpplexer::slex_token<position_type> token_type;
typedef boost::wave::cpplexer::slex::slex_iterator<token_type> lexer_type;
typedef boost::wave::context<std::string::iterator,
		lexer_type,
		boost::wave::iteration_context_policies::load_file_to_string,
		boost::wave::nxc_hooks<token_type>> context_type;
typedef context_type::token_sequence_type container_type;


template
class boost::wave::nxc_hooks<token_type>;
template
bool boost::wave::nxc_hooks<token_type>::found_unknown_directive(
		context_type const &ctx, container_type const &line, container_type &pending);

#endif
