/**
 * NXC2CC - the NXC to C transpiler
 * 
 * Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
 * Author: Jakub Vanek <nxc4ev3@vankovi.net>
 * 
 * 
 * Custom preprocessing directives - download and import.
 */


#include "hooks.hpp"

using namespace boost::wave;

template<typename TokenT,
		typename ContainerT,
		typename PositionT = typename TokenT::position_type>
inline void insert_token(ContainerT &container,
                         token2_id id,
                         std::string const &data,
                         PositionT pos,
                         bool space = true) {
	container.push_back(TokenT(id, (typename TokenT::string_type) (data.c_str()), pos));
	if (space)
		container.push_back(TokenT(T2_SPACE, (typename TokenT::string_type) (" "), pos));
}


template<typename Iterator>
void eat(Iterator &it, Iterator &end) {
	token2_category cat;
	do {
		if (it == end)
			break;
		++it;
		cat = static_cast<token2_category>(it->get_token2_id() & T2_TokenTypeMask);
	} while (cat == T2_WhiteSpaceTokenType || cat == T2_EOLTokenType);
}


template<typename TokenT>
nxc_hooks<TokenT>::nxc_hooks(std::string const &bindir_, std::vector<std::string> &downloads)
		: bindir(bindir_), download(downloads) {
	if (!fs::exists(bindir)) {
		throw std::runtime_error(std::string("binary directory doesn't exist").append(bindir_));
	}
}


template<typename TokenT>
template<typename ContextT, typename ContainerT>
bool nxc_hooks<TokenT>::found_unknown_directive(
		ContextT const &ctx, ContainerT const &line, ContainerT &pending) {
	// <#> < > <import>   < > <"file"> < > <identifier>?
	// <#> < > <download> < > <"file">
	typedef typename TokenT::string_type TokStrT;
	typedef typename ContainerT::const_iterator iter_t;
	typename TokenT::position_type base;
	TokStrT op;
	TokStrT filename;
	fs::path file;
	iter_t in = line.begin();
	iter_t end = line.end();
	if (in == end || in->get_token2_id() != T2_POUND) {
		return false;
	}
	base = in->get_position();
	eat(in, end);
	if (in == end || in->get_token2_id() != T2_IDENTIFIER) {
		return false;
	}
	op = in->get_value();
	eat(in, end);
	if (in == end || in->get_token2_id() != T2_STRINGLIT) {
		return false;
	}
	filename = in->get_value();
	BOOST_ASSERT(filename.size() >= 2);
	filename.erase(0, 1);
	filename.erase(filename.size() - 1, 1);
	file = (bindir / (fs::path(std::string(filename.c_str()))));
	file.make_preferred();
	if (!fs::exists(file)) {
		except_str = std::string("import/download file doesn't exist: ").append(file.string());
		throw boost::wave::preprocess_exception(except_str.c_str(),
		                                        boost::wave::preprocess_exception::bad_include_file,
		                                        base.get_line(),
		                                        base.get_column(),
		                                        base.get_file().c_str());
	}
	if (op == "import") {
		std::string identifier;
		eat(in, end);
		if (in != end) {
			if (in->get_token2_id() != T2_IDENTIFIER)
				return false;
			identifier = std::string(in->get_value().c_str());
		} else {
			size_t dot = filename.find('.');
			size_t nodot = (dot == TokStrT::npos) ? filename.size() : dot;
			identifier = std::string((nodot > 0) ? filename.substr(0, nodot).c_str() : "data");
		}
		eat(in, end);
		if (in != end) {
			return false;
		}

		insert_file(file, identifier, pending, base);
		return true;
	} else if (in != end && op == "download") {
		eat(in, end);
		if (in != end) {
			return false;
		}
		insert_download(file.string());
		return true;
	} else {
		return false;
	}
}


template<typename TokenT>
template<typename ContainerT, typename PositionT>
void nxc_hooks<TokenT>::insert_file(fs::path const &file,
                                    std::string const &identifier,
                                    ContainerT &output,
                                    PositionT base) {
	typedef std::vector<char> buf_t;
	typedef buf_t::iterator buf_iter_t;
	fs::ifstream input(file, std::ios::binary);
	if (!input.is_open()) {
		throw std::runtime_error(std::string("cannot open import file: ").append(file.string()));
	}
	input.unsetf(std::ios::skipws);
	// copies all data into buffer
	buf_t buf((std::istreambuf_iterator<char>(input.rdbuf())), std::istreambuf_iterator<char>());
	// iterate
	buf_iter_t it = buf.begin();
	buf_iter_t end = buf.end();

	insert_token<TokenT>(output, T2_BYTE, "byte", base);
	insert_token<TokenT>(output, T2_IDENTIFIER, identifier, base);
	insert_token<TokenT>(output, T2_LEFTBRACKET, "[", base, false);
	insert_token<TokenT>(output, T2_RIGHTBRACKET, "]", base);
	insert_token<TokenT>(output, T2_ASSIGN, "=", base);
	insert_token<TokenT>(output, T2_LEFTBRACE, "{", base);

	for (; it != end;) {
		insert_token<TokenT>(output, T2_INTLIT, std::to_string((int) *it), base, false);
		if (++it != end)
			insert_token<TokenT>(output, T2_COMMA, ",", base);
	}

	insert_token<TokenT>(output, T2_RIGHTBRACE, "}", base);
	insert_token<TokenT>(output, T2_SEMICOLON, ";", base, false);
	insert_token<TokenT>(output, T2_NEWLINE, "\\n", base, false);
}
