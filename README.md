# NXC4EV3 - NXC to C transpiler
This software allows you to run mostly unmodified NXC programs on the EV3 P-Brick.

## Build requirements
* C++11 compiler for target platform
* CMake 3.5 (although older might work by modifying `CMakeLists.txt`)
* Boost::Wave, Boost::Variant and dependencies

## Tested build configuration
* Build system: Ubuntu 16.04.1
* CMake version: 3.5.1
* Boost version: 1.58.0
* Make: GNU Make 4.1
* Target system: Linux x86_64
  * Target compiler: gcc (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
* Target system: Win32 i686
  * Target compiler: i686-w64-mingw32-gcc-{posix,win32} (GCC) 5.3.1 20160211

## Building

* Install Boost (primarily Boost::Wave and its dependencies).
* Build the project using provided CMake files.

## Running

```
Usage: ./nxc2cc <sysdir> <bindir> <input nxc> <output c> <output dl>
<sysdir>    ... Path to the directory with appropriate NXC headers.
<bindir>    ... Path to the directory from which should resources be pulled.
<input nxc> ... Path to the file to be transpiled to C.
<output c>  ... Path where the C file should be saved.
<output dl> ... Path where the file download list should be saved.
```

## Transpiler Copyright

```
NXC2CC - the NXC to C transpiler

Copyright (C) 2016-2017  Faculty of Electrical Engineering, CTU in Prague
```
